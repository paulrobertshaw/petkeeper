package com.paulro.petkeeper.database;

import com.paulro.petkeeper.provider.PetsContract.VetVisits;

public class TblVisits {

	public static final String TBL_NAME = "vet_visits";

	// move strings to content provider/contract
	public static final String COL_ID = VetVisits._ID;
	public static final String COL_DOCTOR = VetVisits.DOCTOR;
	public static final String COL_DATE = VetVisits.DATE;
	public static final String COL_NEXT_APPOINTMENT_DATE = VetVisits.NEXT_APPOINTMENT_DATE;
	public static final String COL_SUMMARY = VetVisits.SUMMARY;
	public static final String COL_NOTIF_DATE_TIME = VetVisits.NOTIF_DATE_TIME;
	public static final String COL_NOTIF_ID = VetVisits.NOTIF_ID;
	public static final String COL_NOTIF_SET = VetVisits.NOTIF_IS_SET;
	public static final String COL_PET_ID = VetVisits.PET_ID;

	protected static final String DDL_CREATE_TBL_VET_VISITS = 
			"CREATE TABLE " + TBL_NAME + " (" +
					COL_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, \n" +
					COL_DOCTOR + " TEXT NOT NULL, \n" +
					COL_DATE + " DATETIME NOT NULL, \n" +
					COL_NEXT_APPOINTMENT_DATE + " DATETIME, \n" +
					COL_SUMMARY + " TEXT, \n" +
					COL_NOTIF_DATE_TIME + " DATETIME, \n" +
					COL_NOTIF_ID + " INTEGER NOT NULL, \n" +
					COL_NOTIF_SET + " BOOLEAN DEFAULT 0, " +
					COL_PET_ID + " INTEGER, \n" + 
					" FOREIGN KEY (" + COL_PET_ID + ") REFERENCES " + TblPets.TBL_NAME + " (" + TblPets.COL_ID + ") ON DELETE CASCADE \n" +
					")";

}
