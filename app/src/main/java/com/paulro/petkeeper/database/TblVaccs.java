package com.paulro.petkeeper.database;

import com.paulro.petkeeper.provider.PetsContract.Vaccinations;

public class TblVaccs {

	public static final String TBL_NAME = "vaccinations";

	// move strings to content provider/contract
	public static final String COL_ID = Vaccinations._ID;
	public static final String COL_NAME = Vaccinations.NAME;
	public static final String COL_DATE = Vaccinations.DATE;
	public static final String COL_EXPIRATION = Vaccinations.EXPIRATION;
	public static final String COL_NUMBER = Vaccinations.NUMBER;
	public static final String COL_DOCTOR = Vaccinations.DOCTOR;
	public static final String COL_NOTIF_DATE_TIME = Vaccinations.NOTIF_DATE_TIME;
	public static final String COL_NOTIF_ID = Vaccinations.NOTIF_ID;
	public static final String COL_NOTIF_SET = Vaccinations.NOTIF_IS_SET;
	public static final String COL_PET_ID = Vaccinations.PET_ID;

	protected static final String DDL_CREATE_TBL_VACCINATIONS = 
			"CREATE TABLE " + TBL_NAME + " (" +
					COL_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, \n" +
					COL_NAME + " TEXT NOT NULL, \n" +
					COL_DATE + " DATETIME NOT NULL, \n" +
					COL_EXPIRATION + " DATETIME, \n" +
					COL_NUMBER + " TEXT, \n" +
					COL_DOCTOR + " TEXT, \n" +
					COL_NOTIF_DATE_TIME + " DATETIME, \n" +
					COL_NOTIF_ID + " INTEGER NOT NULL, \n" +
					COL_NOTIF_SET + " BOOLEAN DEFAULT 0, " +
					COL_PET_ID + " INTEGER, \n" + 
					" FOREIGN KEY (" + COL_PET_ID + ") REFERENCES " + TblPets.TBL_NAME + " (" + TblPets.COL_ID + ") ON DELETE CASCADE \n" +
					")";
}
