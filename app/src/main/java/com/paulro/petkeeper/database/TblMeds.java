package com.paulro.petkeeper.database;

import com.paulro.petkeeper.provider.PetsContract.Meds;

public class TblMeds {

	public static final String TBL_NAME = "meds";

	// move strings to content provider/contract
	public static final String COL_ID = Meds._ID;
	public static final String COL_NAME = Meds.NAME;
	public static final String COL_FREQUENCY = Meds.FREQUENCY;
	public static final String COL_DATE_BEGUN = Meds.DATE_BEGUN;
	public static final String COL_DATE_MOST_RECENT = Meds.DATE_MOST_RECENT;
	public static final String COL_END_DATE = Meds.END_DATE;
	public static final String COL_DOSE = Meds.DOSE;
	public static final String COL_NOTIF_DATE_TIME = Meds.NOTIF_DATE_TIME;
	public static final String COL_NOTIF_ID = Meds.NOTIF_ID;
	public static final String COL_NOTIF_SET = Meds.NOTIF_IS_SET;
	public static final String COL_PET_ID = Meds.PET_ID;

	protected static final String DDL_CREATE_TBL_MEDS = 
			"CREATE TABLE " + TBL_NAME + " (" +
					COL_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, \n" +
					COL_NAME + " TEXT NOT NULL, \n" +
					COL_FREQUENCY + " INTEGER, \n" + // enum/int
					COL_DATE_BEGUN + " DATETIME, \n" +
					COL_DATE_MOST_RECENT + " DATETIME, \n" +
					COL_END_DATE + " DATETIME, \n" +
					COL_DOSE + " TEXT, \n" +
					COL_NOTIF_DATE_TIME + " DATETIME, \n" +
					COL_NOTIF_ID + " INTEGER NOT NULL, \n" +
					COL_NOTIF_SET + " BOOLEAN DEFAULT 0 \n, " +
					COL_PET_ID + " INTEGER, \n" + 
					" FOREIGN KEY (" + COL_PET_ID + ") REFERENCES " + TblPets.TBL_NAME + " (" + TblPets.COL_ID + ") ON DELETE CASCADE \n" +
					")";
}
