package com.paulro.petkeeper;

import java.io.File;
import java.util.Calendar;

import android.app.AlarmManager;
import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.IntentCompat;
import android.util.Log;

import com.paulro.petkeeper.utilities.AppUtils;
import com.paulro.petkeeper.provider.PetsContract;
import com.paulro.petkeeper.provider.PetsContract.Meds;
import com.paulro.petkeeper.provider.PetsContract.NotificationFrequency;
import com.paulro.petkeeper.provider.PetsContract.Pets;
import com.paulro.petkeeper.provider.PetsContract.Vaccinations;
import com.paulro.petkeeper.provider.PetsContract.VetVisits;

public class NotifAndNextAlarmService extends IntentService {

	public static final String TAG = NotifAndNextAlarmService.class.getSimpleName();

	private NotificationCompat.Builder mBuilder;
	private boolean mValidNotification;

	public NotifAndNextAlarmService() {
		super(TAG);
	}
	public NotifAndNextAlarmService(String name) {
		super(name);
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
		mBuilder = new NotificationCompat.Builder(this);

		mValidNotification = true; // initialise to true

		Intent resultIntent = new Intent(this, NotificationClickActivity.class);

		int notifID = 0;
		Uri dataUri = intent.getData();
		String intentAction = intent.getAction();
		if (dataUri != null) {
			resultIntent.setData(dataUri);
			String uriType = this.getContentResolver().getType(dataUri);

			switch (uriType) {
			case PetsContract.Meds.CONTENT_ITEM_TYPE:
				notifID = handleMeds(dataUri, intentAction);
				break;
			case PetsContract.Vaccinations.CONTENT_ITEM_TYPE:
				notifID = handleVaccs(dataUri, intentAction);
				break;
			case PetsContract.VetVisits.CONTENT_ITEM_TYPE:
				notifID = handleVisits(dataUri);
				break;
			default:
				Log.e(AlarmReceiver.class.getName(), "something has gone terribly wrong!");
			}
		}

		SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(this);
		boolean allowNotifs = sharedPrefs.getBoolean(getString(R.string.key_notifications_enabled), true);

		if (mValidNotification && allowNotifs) { // if the end date is past, don't post notification
			Intent snoozeIntent = new Intent(this, AlarmSnoozeService.class);
			snoozeIntent.setAction(intentAction).setData(dataUri); // maybe I could just use the 'intent' that was passed in?
			snoozeIntent.putExtra(PetsContract.NotificationId.TBL_NAME, notifID);
			PendingIntent snoozePendIntent = PendingIntent.getService(this, notifID, snoozeIntent, 
					PendingIntent.FLAG_UPDATE_CURRENT);
			mBuilder.addAction(R.drawable.ic_action_alarms, getString(R.string.btn_snooze), 
					snoozePendIntent); // snooze button for 4.1+

//			mBuilder.setDefaults(Notification.DEFAULT_ALL);
			// read preferences
			boolean vibrate = sharedPrefs.getBoolean(getString(R.string.key_notifications_vibrate), true);
			String lightColor = sharedPrefs.getString(getString(R.string.key_notifications_lights), "0");
			Uri ringTone = Uri.parse(sharedPrefs.getString(getString(R.string.key_notifications_ringtone), ""));

			int lightCode = 0;
			if (!lightColor.startsWith("None")) {
				if (lightColor.equalsIgnoreCase("Default")) {
					mBuilder.setDefaults(Notification.DEFAULT_LIGHTS);
				}
				else if (lightColor.equalsIgnoreCase("White")) {
					lightCode = 0xFFFFFFFF;
				}
				else if (lightColor.equalsIgnoreCase("Red")) {
					lightCode = 0xFFFF0000;
				}
				else if (lightColor.equalsIgnoreCase("Green")) {
					lightCode = 0xFF00FF00;
				}
				else if (lightColor.equalsIgnoreCase("Blue")) {
					lightCode = 0xFF0000FF;
				}

				mBuilder.setLights(lightCode, 800, 200);
			}

			if (vibrate) { // could also use the default vibrate
				mBuilder.setVibrate(new long[] { 100, 100, 100, 300, 300, 500, 500, 500 });
			}

			mBuilder.setSound(ringTone); // what if there is no ringtone set?

			// no need to add back stack for a special activity intent

			// starts activity in a new, empty task
			resultIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | IntentCompat.FLAG_ACTIVITY_CLEAR_TASK); // API 11+?

			// not sure what the requestcode is good for here
			PendingIntent resultPendIntent = PendingIntent.getActivity(this, notifID, resultIntent, 
					PendingIntent.FLAG_UPDATE_CURRENT);

			mBuilder.setContentIntent(resultPendIntent);
			manager.notify(notifID, mBuilder.build());
		}

		AlarmReceiver.completeWakefulIntent(intent); // release wake lock
	}

	private int handleMeds(Uri medUri, String notifType) {
		Cursor med = this.getContentResolver().query(medUri, PetsContract.Meds.PROJECTION_ALL, null, null, null);

		if (!med.moveToFirst()) { // record has been deleted from db, but alarm has gone off
			mValidNotification = false;
			med.close();
			return -1;
		}

		int notifID = med.getInt(med.getColumnIndexOrThrow(Meds.NOTIF_ID));
		long petID = med.getLong(med.getColumnIndexOrThrow(Meds.PET_ID));

		Cursor pet = this.getContentResolver().query(Pets.CONTENT_URI, Pets.PROJECTION_ALL, 
				PetsContract.SELECTION_BY_ID, new String[] { String.valueOf(petID) }, null);
		pet.moveToFirst();
		String petName = pet.getString(pet.getColumnIndexOrThrow(Pets.NAME));
		pet.close();

		addImage(petID);
		mBuilder.setSmallIcon(R.drawable.ic_action_alarms);
		mBuilder.setContentTitle(med.getString(med.getColumnIndexOrThrow(Meds.NAME))); // app name? or other message?
		mBuilder.setContentText("Time to give " + petName + " medicine");
		mBuilder.setOngoing(true); // ongoing alerts can't be deleted by pressing clear all

		// use setWhen() for notifications where the alarm time is past
		if (notifType != null) {
			if (notifType.equals(AppUtils.NOTIFICATION_PAST_DUE)) {
				mBuilder.setWhen(med.getLong(med.getColumnIndexOrThrow(Meds.NOTIF_DATE_TIME)));
				mBuilder.setUsesChronometer(true);
			}
//			else if (notifType.equals(AppUtils.NOTIFICATION_FUTURE)) {} // special options here, if there are any
		}

		// set the next alarm
		int setNotification = med.getInt(med.getColumnIndexOrThrow(Meds.NOTIF_IS_SET));
		if (setNotification != 0) {
			long dateMostRecentUnixTime = med.getLong(med.getColumnIndexOrThrow(Meds.DATE_MOST_RECENT));
			long endDateUnixTime = med.getLong(med.getColumnIndexOrThrow(Meds.END_DATE));
			long frequencyIndex = med.getLong(med.getColumnIndexOrThrow(Meds.FREQUENCY));

			Calendar cal = Calendar.getInstance();
			long currentTime = cal.getTimeInMillis();

			if (endDateUnixTime > 0 && currentTime > endDateUnixTime) { // end date is past
				med.close();
				ContentValues values = new ContentValues();
				values.put(Meds.NOTIF_IS_SET, false); // deactivate alarm
				getContentResolver().update(medUri, values, null, null);
				mValidNotification = false;
				return notifID;
			}

			// look up the time interval in the frequencies table
			Cursor freq = getContentResolver().query(Uri.withAppendedPath(NotificationFrequency.CONTENT_URI, 
					String.valueOf(frequencyIndex)), NotificationFrequency.PROJECTION_ALL, null, null, null);

			freq.moveToFirst();
			int timeValue = freq.getInt(freq.getColumnIndexOrThrow(NotificationFrequency.TIME_VALUE));
			NotificationFrequency.TimeUnits timeUnit = NotificationFrequency.TimeUnits.
					valueOf(freq.getString(freq.getColumnIndexOrThrow(NotificationFrequency.TIME_UNIT)));
			freq.close();

			cal.setTimeInMillis(dateMostRecentUnixTime);
			int notificationsMissed = 0;
			long alarmTime = 0L;
            int calendarConstant = timeUnit.getCalendarConstant();
			while (alarmTime < currentTime) {
				// move calendar to new time
                if (!AppUtils.updateCalendar(cal, calendarConstant, timeValue, TAG)) {
                    break; // avoid infinite loops if the update fails
                }
				alarmTime = cal.getTimeInMillis();
				notificationsMissed++;
			}

			if (notificationsMissed > 2) { // more than one time interval has passed since the med was last given
				mBuilder.setNumber(notificationsMissed - 1); // the while loop goes one interval past the current time
			}

			// set next alarm
			AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
			Intent nextAlarmIntent = new Intent(this, AlarmReceiver.class);
			nextAlarmIntent.setAction(AppUtils.NOTIFICATION_FUTURE).setData(medUri);
			
			PendingIntent nextAlarmPendIntent = PendingIntent.getBroadcast(getApplicationContext(), 
					notifID, nextAlarmIntent, PendingIntent.FLAG_UPDATE_CURRENT);
			alarmManager.set(AlarmManager.RTC_WAKEUP, alarmTime, nextAlarmPendIntent);
			
			ContentValues values = new ContentValues();
			values.put(Meds.NOTIF_DATE_TIME, alarmTime);
			getContentResolver().update(medUri, values, null, null); // update database
		}
		else { // notification box has been unchecked for this med record
			mValidNotification = false;
		}

		med.close();
		return notifID;
	}

	private void addImage(long petID) {
		File imageFile = getFileStreamPath(AppUtils.imageFileName(String.valueOf(petID)));

		if (imageFile.exists()) {
			// 128sp maz dimen
			mBuilder.setLargeIcon(AppUtils.decodeSampledBitmapFromFile(imageFile.getPath(), 128));
		}
	}

	private int handleVaccs(Uri vacUri, String notifType) {
		Cursor vacc = getContentResolver().query(vacUri, Vaccinations.PROJECTION_ALL, null, null, null);
		if (!vacc.moveToFirst()) { // row not found in db
			mValidNotification = false;
			vacc.close();
			return -1;
		}
		if (vacc.getInt(vacc.getColumnIndexOrThrow(Vaccinations.NOTIF_IS_SET)) == 0) {
			// the set notification checkbox has been unchecked since the alarm was scheduled
			mValidNotification = false;
			vacc.close();
			return -1;
		}

		int notifID = vacc.getInt(vacc.getColumnIndexOrThrow(Vaccinations.NOTIF_ID));
		long petID = vacc.getInt(vacc.getColumnIndexOrThrow(Vaccinations.PET_ID));

		addImage(petID);
		if (notifType != null) {
			if (notifType.equals(AppUtils.NOTIFICATION_PAST_DUE)) {
				// show the time of expiration, or when the notification is posted?
				mBuilder.setWhen(vacc.getLong(vacc.getColumnIndexOrThrow(Vaccinations.EXPIRATION)));
				mBuilder.setUsesChronometer(true); // does this actually do anything?!
			}
		}

		String vaccName = vacc.getString(vacc.getColumnIndexOrThrow(Vaccinations.NAME));
		vacc.close();

		Cursor pet = getContentResolver().query(Uri.withAppendedPath(Pets.CONTENT_URI, String.valueOf(petID)), 
				new String[] { Pets.NAME, Pets.PHOTO }, null, null, null); // use pet photo for notif icon?
		pet.moveToFirst();
		String petName = pet.getString(pet.getColumnIndexOrThrow(Pets.NAME));
		pet.close();

		mBuilder.setSmallIcon(R.drawable.ic_action_alarms);
		mBuilder.setContentTitle("Vaccination Expiring"); // TODO localise? improve?
		mBuilder.setContentText(vaccName + " is expiring for " + petName);
		mBuilder.setOngoing(true);

		return notifID;
	}

	private int handleVisits(Uri vetUri) {
		Cursor visit = getContentResolver().query(vetUri, VetVisits.PROJECTION_ALL, null, null, null);
		if (!visit.moveToFirst()) {
			mValidNotification = false;
			Log.e(TAG, "moveToFirst() failed for URI: " + vetUri.toString());
			visit.close();
			return -1;
		}
		if (visit.getInt(visit.getColumnIndexOrThrow(VetVisits.NOTIF_IS_SET)) == 0) {
			// the set notification checkbox has been unchecked since the alarm was scheduled
			mValidNotification = false;
			visit.close();
			return -1;
		}

		int notifID = visit.getInt(visit.getColumnIndexOrThrow(VetVisits.NOTIF_ID));
		long petID = visit.getLong(visit.getColumnIndexOrThrow(VetVisits.PET_ID));
		long nextApptTimeMillis = visit.getLong(visit.getColumnIndexOrThrow(VetVisits.NEXT_APPOINTMENT_DATE));

		addImage(petID);
		mBuilder.setSmallIcon(R.drawable.ic_action_alarms);
		mBuilder.setContentTitle("Vet Appointment"); // TODO localise
		mBuilder.setContentText("On " + AppUtils.formatDate(nextApptTimeMillis, this) + 
				" at " + AppUtils.formatTime(nextApptTimeMillis, this));
		mBuilder.setOngoing(true);

		visit.close();
		return notifID;
	}
}
