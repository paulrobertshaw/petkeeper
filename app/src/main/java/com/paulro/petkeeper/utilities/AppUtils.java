package com.paulro.petkeeper.utilities;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.text.format.DateFormat;
import android.util.Log;
import android.widget.DatePicker;
import android.widget.TimePicker;
import android.widget.Toast;

import com.paulro.petkeeper.R;

import java.io.File;
import java.io.FileDescriptor;
import java.util.Calendar;
import java.util.Date;

public class AppUtils {

	public static final String NOTIFICATION_FUTURE = "NOTIFICATION_FUTURE";
	public static final String NOTIFICATION_PAST_DUE = "NOTIFICATION_PAST_DUE";

	// does passing in a Context reference cause memory leaks?
	// from what I've read these static methods don't...

	public static String formatDate(long startTime, final Context context) {
		return DateFormat.getDateFormat(context).format(new Date(startTime));
	}

	public static String formatDateTime(long startTime, final Context context) {
		return DateFormat.getDateFormat(context).format(new Date(startTime)) + " - " +
				DateFormat.getTimeFormat(context).format(new Date(startTime));
	}

	public static String formatTime(long startTime, final Context context) {
		return DateFormat.getTimeFormat(context).format(new Date(startTime));
	}

	public static long getLastViewedPetID(final Context context) {
		SharedPreferences sharedPrefs = context.getSharedPreferences(context.getString(R.string.shared_prefs_file),
				Context.MODE_PRIVATE);
		return sharedPrefs.getLong(context.getString(R.string.last_viewed_pet_key), 0); // if record cannot be found, return 0
	}

	public static void setLastViewedPetID(final Context context, long id) {
		SharedPreferences sharedPrefs = context.getSharedPreferences(context.getString(R.string.shared_prefs_file), 
				Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = sharedPrefs.edit();
		// apply() doesn't block the main thread like commit() does
		editor.putLong(context.getString(R.string.last_viewed_pet_key), id).apply();
	}

	public static long getUnixTime(DatePicker dateInput) {
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.YEAR, dateInput.getYear());
		cal.set(Calendar.MONTH, dateInput.getMonth());
		cal.set(Calendar.DAY_OF_MONTH, dateInput.getDayOfMonth());
		return cal.getTimeInMillis();
	}

	public static long getUnixTime(DatePicker dateInput, long startDate) {
		int[] startTime = getFriendlyTime(startDate); // used to preserve user's already chosen time
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.YEAR, dateInput.getYear());
		cal.set(Calendar.MONTH, dateInput.getMonth());
		cal.set(Calendar.DAY_OF_MONTH, dateInput.getDayOfMonth());
		cal.set(Calendar.HOUR_OF_DAY, startTime[0]);
		cal.set(Calendar.MINUTE, startTime[1]);
		return cal.getTimeInMillis();
	}

	public static long getUnixTimeOfDay(TimePicker timeInput, long startDate) {
		Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis(startDate);
		cal.set(Calendar.HOUR_OF_DAY, timeInput.getCurrentHour());
		cal.set(Calendar.MINUTE, timeInput.getCurrentMinute());
		return cal.getTimeInMillis();
	}

	public static int[] getFriendlyDate(long unixTimestamp) {
		Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis(unixTimestamp);
		int year = cal.get(Calendar.YEAR);
		int month = cal.get(Calendar.MONTH); 
		int day = cal.get(Calendar.DAY_OF_MONTH);
		return new int[] { year, month, day };
	}

	public static int[] getFriendlyTime(long unixTimeStamp) {
		Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis(unixTimeStamp);
		int hour = cal.get(Calendar.HOUR_OF_DAY);
		int minute = cal.get(Calendar.MINUTE);
		return new int[] { hour, minute };
	}

    public static boolean updateCalendar(Calendar cal, int calendarConstant, int timeValue, String callingClassTag){
        // eclipse could handle putting in the integer value of the calendar field to update
        // but android studio shows an error if it's not explicit
        if (calendarConstant == Calendar.HOUR_OF_DAY){
            cal.add(Calendar.HOUR_OF_DAY, timeValue);
            return true;
        }
        else if (calendarConstant == Calendar.DAY_OF_MONTH)  {
            cal.add(Calendar.DAY_OF_MONTH, timeValue);
            return true;
        }
        else if (calendarConstant == Calendar.WEEK_OF_MONTH) {
            cal.add(Calendar.WEEK_OF_MONTH, timeValue);
            return true;
        }
        else if (calendarConstant == Calendar.MONTH) {
            cal.add(Calendar.MONTH, timeValue);
            return true;
        }
        else {
            Log.wtf(callingClassTag, "Unexpected Calendar field value: " + calendarConstant);
            return false;
        }
    }

	public static void makeToast(final Context context, String message, int toastLength) {
		Toast.makeText(context, message, toastLength).show();
	}
	
	public static Bitmap decodeSampledBitmapFromFile(String filePath, int maxDimen) {
		return decodeSampledBitmapFromFile(filePath, maxDimen, false);
	}

	public static Bitmap decodeSampledBitmapFromFile(String filePath, int maxDimen, boolean heightOnly) {

		BitmapFactory.Options options = new BitmapFactory.Options();
		options.inJustDecodeBounds = true; // only get the info, not the image
		BitmapFactory.decodeFile(filePath, options); // returns a null bmp

		options.inSampleSize = calculateInSampleSize(options, maxDimen, heightOnly);
		options.inJustDecodeBounds = false; // set to false to now get the photo for real

		return BitmapFactory.decodeFile(filePath, options);
	}
	
	public static Bitmap decodeSampledBitmapFromFileDescriptor(FileDescriptor fd, int maxDimen) {
		BitmapFactory.Options options = new BitmapFactory.Options();
		options.inJustDecodeBounds = true; // only get the info, not the image
		BitmapFactory.decodeFileDescriptor(fd, null, options);
		
		options.inSampleSize = calculateInSampleSize(options, maxDimen, false); // only need boolean when displaying image
		options.inJustDecodeBounds = false; // set to false to now get the photo for real
		
		return BitmapFactory.decodeFileDescriptor(fd, null, options);
	}

	public static int calculateInSampleSize(BitmapFactory.Options options, int maxDimen, boolean heightOnly) {
		final int height = options.outHeight;
		final int width = options.outWidth;
		int inSampleSize = 1;

		if (height > maxDimen || width > maxDimen) {
			final int halfHeight = height / 2;
			final int halfWidth = width / 2;

			// calculate largest inSampleSize that leaves the larger dimension of the image just larger than 
			// the larger screen dimension, or just the height, depending on the need
			while ((halfHeight / inSampleSize) > maxDimen || (!heightOnly && (halfWidth / inSampleSize) > maxDimen)) {
				inSampleSize *= 2; // must be a power of 2
			}
		}

		return inSampleSize;
	}

	// this is the example from the android.com guide
//	public static int calculateInSampleSize(BitmapFactory.Options options, int reqHeight, int reqWidth) {
//		final int height = options.outHeight;
//		final int width = options.outWidth;
//		int inSampleSize = 1;
//
//		if (height > reqHeight || width > reqWidth) {
//			final int halfHeight = height / 2;
//			final int halfWidth = width / 2;
//
//			// calculate largest inSampleSize that leaves the dimensions of the image just larger than 
//			// the requested height and/or width, whichever is closer
//			while ((halfHeight / inSampleSize) > reqHeight && (halfWidth / inSampleSize) > reqWidth) {
//				inSampleSize *= 2; // must be a power of 2
//			}
//		}
//
//		return inSampleSize;
//	}

	public static Bitmap rotateBitmap(String filePath, Bitmap src) {
		try {
			ExifInterface exif = new ExifInterface(filePath);
			int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 1);
			Matrix matrix = new Matrix();

			if (orientation != ExifInterface.ORIENTATION_NORMAL) {
				switch (orientation) {
				case ExifInterface.ORIENTATION_ROTATE_90:
					matrix.setRotate(90);
					break;
				case ExifInterface.ORIENTATION_ROTATE_180:
					matrix.setRotate(180);
					break;
				case ExifInterface.ORIENTATION_ROTATE_270:
					matrix.setRotate(270);
					break;
				case ExifInterface.ORIENTATION_TRANSPOSE:
					matrix.setRotate(90);
					matrix.postScale(-1, 1);
					break;
				case ExifInterface.ORIENTATION_TRANSVERSE:
					matrix.setRotate(270);
					matrix.postScale(-1, 1);
					break;
				case ExifInterface.ORIENTATION_FLIP_HORIZONTAL:
					matrix.setScale(-1, 1);
					break;
				case ExifInterface.ORIENTATION_FLIP_VERTICAL:
					matrix.setRotate(180);
					matrix.postScale(-1, 1);
					break;
				default:
					return src;
				}
				// can cause OutOfMemoryException
				Bitmap newBitmap = Bitmap.createBitmap(src, 0, 0, src.getWidth(), src.getHeight(), matrix, true);
				src.recycle();
				return newBitmap;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return src; // image did not need to be rotated, or an exception was caught
	}

	public static String imageFileName(String petId) {
		return "photo_" + petId + ".jpg";
	}
	
	public static boolean deleteImageFile(final Context context, String petId) {
		File photoFile = context.getFileStreamPath(AppUtils.imageFileName(petId));
		if (photoFile.exists()) {
			return photoFile.delete();
		}
		return false;
	}

}
