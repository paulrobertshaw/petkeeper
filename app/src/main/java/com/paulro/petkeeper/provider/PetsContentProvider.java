package com.paulro.petkeeper.provider;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.provider.BaseColumns;
import android.text.TextUtils;

import com.paulro.petkeeper.database.PetDatabaseHelper;
import com.paulro.petkeeper.database.TblMeds;
import com.paulro.petkeeper.database.TblNotifFrequency;
import com.paulro.petkeeper.database.TblPets;
import com.paulro.petkeeper.database.TblVaccs;
import com.paulro.petkeeper.database.TblVisits;
import com.paulro.petkeeper.provider.PetsContract.Meds;
import com.paulro.petkeeper.provider.PetsContract.NotificationFrequency;
import com.paulro.petkeeper.provider.PetsContract.Pets;
import com.paulro.petkeeper.provider.PetsContract.Vaccinations;
import com.paulro.petkeeper.provider.PetsContract.VetVisits;

public class PetsContentProvider extends ContentProvider {

	private static final int PET_LIST = 1;
	private static final int PET_ID = 2;
	private static final int MED_LIST = 10;
	private static final int MED_ID = 11;
	private static final int VISIT_LIST = 20;
	private static final int VISIT_ID = 21;
	private static final int VACC_LIST = 30;
	private static final int VACC_ID = 31;
	private static final int NOTIF_FREQ_LIST = 40;
	private static final int NOTIF_FREQ_ID = 41;

	private static final UriMatcher URI_MATCHER;

	private PetDatabaseHelper mDbHelper;

	static {
		URI_MATCHER = new UriMatcher(UriMatcher.NO_MATCH);
		URI_MATCHER.addURI(PetsContract.AUTHORITY, "pets", PET_LIST);
		URI_MATCHER.addURI(PetsContract.AUTHORITY, "pets/#", PET_ID);
		URI_MATCHER.addURI(PetsContract.AUTHORITY, "meds", MED_LIST);
		URI_MATCHER.addURI(PetsContract.AUTHORITY, "meds/#", MED_ID);
		URI_MATCHER.addURI(PetsContract.AUTHORITY, "vet_visits", VISIT_LIST);
		URI_MATCHER.addURI(PetsContract.AUTHORITY, "vet_visits/#", VISIT_ID);
		URI_MATCHER.addURI(PetsContract.AUTHORITY, "vaccinations", VACC_LIST);
		URI_MATCHER.addURI(PetsContract.AUTHORITY, "vaccinations/#", VACC_ID);
		URI_MATCHER.addURI(PetsContract.AUTHORITY, "notif_freq", NOTIF_FREQ_LIST);
		URI_MATCHER.addURI(PetsContract.AUTHORITY, "notif_freq/#", NOTIF_FREQ_ID);
	}

	@Override
	public int delete(Uri uri, String selection, String[] selectionArgs) {
		SQLiteDatabase db = mDbHelper.getWritableDatabase();
		int rowsDeleted;
		String id = uri.getLastPathSegment();
		switch (URI_MATCHER.match(uri)) {
		case PET_LIST:
			rowsDeleted = db.delete(TblPets.TBL_NAME, selection, selectionArgs);
			break;
		case PET_ID:
			if (TextUtils.isEmpty(selection)) {
				rowsDeleted = db.delete(TblPets.TBL_NAME, PetsContract.SELECTION_BY_ID, new String[] { id });
			}
			else {
				rowsDeleted = db.delete(TblPets.TBL_NAME, selection + " AND " + Pets._ID + " = " + id, selectionArgs);
			}
			break;

		case MED_LIST:
			rowsDeleted = db.delete(TblMeds.TBL_NAME, selection, selectionArgs);
			break;
		case MED_ID:
			if (TextUtils.isEmpty(selection)) {
				rowsDeleted = db.delete(TblMeds.TBL_NAME, PetsContract.SELECTION_BY_ID, new String[] { id });
			}
			else {
				rowsDeleted = db.delete(TblMeds.TBL_NAME, selection + " AND " + Meds._ID + " = " + id, selectionArgs);
			}
			break;

		case VISIT_LIST:
			rowsDeleted = db.delete(TblVisits.TBL_NAME, selection, selectionArgs);
			break;
		case VISIT_ID:
			if (TextUtils.isEmpty(selection)) {
				rowsDeleted = db.delete(TblVisits.TBL_NAME, PetsContract.SELECTION_BY_ID, new String[] { id });
			}
			else {
				rowsDeleted = db.delete(TblVisits.TBL_NAME, selection + " AND " + VetVisits._ID + " = " + id, selectionArgs);
			}
			break;

		case VACC_LIST:
			rowsDeleted = db.delete(TblVaccs.TBL_NAME, selection, selectionArgs);
			break;
		case VACC_ID:
			if (TextUtils.isEmpty(selection)) {
				rowsDeleted = db.delete(TblVaccs.TBL_NAME, PetsContract.SELECTION_BY_ID, new String[] { id });
			}
			else {
				rowsDeleted = db.delete(TblVaccs.TBL_NAME, selection + " AND " + Vaccinations._ID + " = " + id, selectionArgs);
			}
			break;

		case NOTIF_FREQ_LIST:
			rowsDeleted = db.delete(TblNotifFrequency.TBL_NAME, selection, selectionArgs);
            break;
		case NOTIF_FREQ_ID:
			if (TextUtils.isEmpty(selection)) {
				rowsDeleted = db.delete(TblNotifFrequency.TBL_NAME, PetsContract.SELECTION_BY_ID, new String[] { id });
			}
			else {
				rowsDeleted = db.delete(TblNotifFrequency.TBL_NAME, selection + " AND " + NotificationFrequency._ID + " = " + id, selectionArgs);
			}
			break;

		default:
			throw new IllegalArgumentException("Unknown URI: " + uri);
		}
		getContext().getContentResolver().notifyChange(uri, null);
		return rowsDeleted;
	}

	@Override
	public String getType(Uri uri) {
		switch (URI_MATCHER.match(uri)) {
		case PET_LIST:
			return Pets.CONTENT_TYPE;
		case PET_ID:
			return Pets.CONTENT_ITEM_TYPE;
		case MED_LIST:
			return Meds.CONTENT_TYPE;
		case MED_ID:
			return Meds.CONTENT_ITEM_TYPE;
		case VISIT_LIST:
			return VetVisits.CONTENT_TYPE;
		case VISIT_ID:
			return VetVisits.CONTENT_ITEM_TYPE;
		case VACC_LIST:
			return Vaccinations.CONTENT_TYPE;
		case VACC_ID:
			return Vaccinations.CONTENT_ITEM_TYPE;
		case NOTIF_FREQ_LIST:
			return NotificationFrequency.CONTENT_TYPE;
		case NOTIF_FREQ_ID:
			return NotificationFrequency.CONTENT_ITEM_TYPE;
		default:
			throw new IllegalArgumentException("Unknown URI: " + uri);
		}
	}

	@Override
	public Uri insert(Uri uri, ContentValues values) {
		// TODO checking foreign keys perhaps
		SQLiteDatabase db = mDbHelper.getWritableDatabase();
		long insertID = 0;
		long notifID;

		switch (URI_MATCHER.match(uri)) {
		case PET_LIST:
			insertID = db.insert(TblPets.TBL_NAME, null, values);
			return getUriAndNotify(insertID, uri);
		case MED_LIST:
			db.beginTransaction();
			try {
				notifID = db.insert(PetsContract.NotificationId.TBL_NAME, PetsContract.NotificationId._ID, null);
				values.put(Meds.NOTIF_ID, notifID);
				insertID = db.insert(TblMeds.TBL_NAME, null, values);
				db.setTransactionSuccessful();
			}
			finally {
				db.endTransaction();
			}
			return getUriAndNotify(insertID, uri);
		case VISIT_LIST:
			db.beginTransaction();
			try {
				notifID = db.insert(PetsContract.NotificationId.TBL_NAME, PetsContract.NotificationId._ID, null);
				values.put(VetVisits.NOTIF_ID, notifID);
				insertID = db.insert(TblVisits.TBL_NAME, null, values);
				db.setTransactionSuccessful();
			}
			finally {
				db.endTransaction();
			}
			return getUriAndNotify(insertID, uri);
		case VACC_LIST:
			db.beginTransaction();
			try {
				notifID = db.insert(PetsContract.NotificationId.TBL_NAME, PetsContract.NotificationId._ID, null);
				values.put(Vaccinations.NOTIF_ID, notifID);
				insertID = db.insert(TblVaccs.TBL_NAME, null, values);
				db.setTransactionSuccessful();
			}
			finally {
				db.endTransaction();
			}
			return getUriAndNotify(insertID, uri);
		case NOTIF_FREQ_LIST:
			insertID = db.insert(TblNotifFrequency.TBL_NAME, null, values);
			return getUriAndNotify(insertID, uri);
		default:
			throw new IllegalArgumentException("Unsupported URI for insertion: " + uri);
		}
	}

	private Uri getUriAndNotify(long id, Uri uri) {
		if (id > 0) {
			Uri insertedUri = ContentUris.withAppendedId(uri, id);
			//notify listeners of changes
			getContext().getContentResolver().notifyChange(insertedUri, null);
			return insertedUri;
		}
		throw new SQLException("URI not inserted: " + uri);
	}

	@Override
	public boolean onCreate() {
		mDbHelper = new PetDatabaseHelper(getContext());
		return true;
	}

	@Override
	public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs,
			String sortOrder) {
		// TODO possible joins later
		SQLiteDatabase db = mDbHelper.getWritableDatabase(); // can't call onUpgrade on a read-only db
		SQLiteQueryBuilder builder = new SQLiteQueryBuilder();
		
		switch (URI_MATCHER.match(uri)) {
		case PET_LIST:
			builder.setTables(TblPets.TBL_NAME);
			if (TextUtils.isEmpty(sortOrder)) {
				sortOrder = Pets.SORT_ORDER_DEFAULT;
			}
			break;
		case PET_ID:
			builder.setTables(TblPets.TBL_NAME);
			// only return one row
			builder.appendWhere(Pets._ID + " = " + uri.getLastPathSegment());
			break;
		case MED_LIST:
			builder.setTables(TblMeds.TBL_NAME);
			if (TextUtils.isEmpty(sortOrder)) {
				sortOrder = Meds.SORT_ORDER_DEFAULT;
			}
			break;
		case MED_ID:
			builder.setTables(TblMeds.TBL_NAME);
			builder.appendWhere(Meds._ID + " = " + uri.getLastPathSegment());
			break;
		case VISIT_LIST:
			builder.setTables(TblVisits.TBL_NAME);
			if (TextUtils.isEmpty(sortOrder)) {
				sortOrder = VetVisits.SORT_ORDER_DEFAULT;
			}
			break;
		case VISIT_ID:
			builder.setTables(TblVisits.TBL_NAME);
			builder.appendWhere(VetVisits._ID + " = " + uri.getLastPathSegment());
			break;
		case VACC_LIST:
			builder.setTables(TblVaccs.TBL_NAME);
			if (TextUtils.isEmpty(sortOrder)) {
				sortOrder = Vaccinations.SORT_ORDER_DEFAULT;
			}
			break;
		case VACC_ID:
			builder.setTables(TblVaccs.TBL_NAME);
			builder.appendWhere(Vaccinations._ID + " = " + uri.getLastPathSegment());
			break;
		case NOTIF_FREQ_LIST:
			builder.setTables(TblNotifFrequency.TBL_NAME);
			if (TextUtils.isEmpty(sortOrder)) {
				sortOrder = NotificationFrequency.SORT_ORDER_DEFAULT;
			}
			break;
		case NOTIF_FREQ_ID:
			builder.setTables(TblNotifFrequency.TBL_NAME);
			builder.appendWhere(NotificationFrequency._ID + " = " + uri.getLastPathSegment());
			break;
		default:
			throw new IllegalArgumentException("Unknown URI: " + uri);
		}
		
		Cursor cursor = builder.query(db, projection, selection, selectionArgs, null, null, sortOrder);
		
		// to be notified of any changes
		cursor.setNotificationUri(getContext().getContentResolver(), uri);
		
		return cursor;
	}

	@Override
	public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
		// TODO updates to notifications?
		SQLiteDatabase db = mDbHelper.getWritableDatabase();
		int rowsUpdated;
		String where;
		
		switch (URI_MATCHER.match(uri)) {
		case PET_LIST:
			rowsUpdated = db.update(TblPets.TBL_NAME, values, selection, selectionArgs);
			break;
		case PET_ID:
			where = BaseColumns._ID + " = " + uri.getLastPathSegment();
			if (!TextUtils.isEmpty(selection)) {
				where += " AND " + selection;
			}
			rowsUpdated = db.update(TblPets.TBL_NAME, values, where, selectionArgs);
			break;
			
		case MED_LIST:
			rowsUpdated = db.update(TblMeds.TBL_NAME, values, selection, selectionArgs);
			break;
		case MED_ID:
			where = BaseColumns._ID + " = " + uri.getLastPathSegment();
			if (!TextUtils.isEmpty(selection)) {
				where += " AND " + selection;
			}
			rowsUpdated = db.update(TblMeds.TBL_NAME, values, where, selectionArgs);
			break;
			
		case VISIT_LIST:
			rowsUpdated = db.update(TblVisits.TBL_NAME, values, selection, selectionArgs);
			break;
		case VISIT_ID:
			where = BaseColumns._ID + " = " + uri.getLastPathSegment();
			if (!TextUtils.isEmpty(selection)) {
				where += " AND " + selection;
			}
			rowsUpdated = db.update(TblVisits.TBL_NAME, values, where, selectionArgs);
			break;
			
		case VACC_LIST:
			rowsUpdated = db.update(TblVaccs.TBL_NAME, values, selection, selectionArgs);
			break;
		case VACC_ID:
			where = BaseColumns._ID + " = " + uri.getLastPathSegment();
			if (!TextUtils.isEmpty(selection)) {
				where += " AND " + selection;
			}
			rowsUpdated = db.update(TblVaccs.TBL_NAME, values, where, selectionArgs);
			break;
			
		case NOTIF_FREQ_LIST:
			rowsUpdated = db.update(TblNotifFrequency.TBL_NAME, values, selection, selectionArgs);
			break;
		case NOTIF_FREQ_ID:
			where = BaseColumns._ID + " = " + uri.getLastPathSegment();
			if (!TextUtils.isEmpty(selection)) {
				where += " AND " + selection;
			}
			rowsUpdated = db.update(TblNotifFrequency.TBL_NAME, values, where, selectionArgs);
			break;
		default:
			throw new IllegalArgumentException("Unknown URI: " + uri);
		}
		
		if (rowsUpdated > 0) { // only notify when a change has been made
			getContext().getContentResolver().notifyChange(uri, null);
		}
		return rowsUpdated;
	}

}
