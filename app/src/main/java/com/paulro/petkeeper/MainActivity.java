package com.paulro.petkeeper;

import java.util.Calendar;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.TaskStackBuilder;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.paulro.petkeeper.utilities.AppUtils;
import com.paulro.petkeeper.utilities.ImageSaverFragment;
import com.paulro.petkeeper.provider.PetsContract;
import com.paulro.petkeeper.utilities.ConfirmationFragment;
import com.paulro.petkeeper.utilities.NewPetSelected;

public class MainActivity extends ActionBarActivity implements MainFragment.ButtonListeners, MainFragment.MissingPetsListener,
NavigationDrawerFragment.NavigationDrawerCallbacks {

	private static final String TAG = MainActivity.class.getSimpleName();

	private static final String DEFAULT_FRAGMENT_TAG = MainFragment.TAG;
	private static final int SELECT_PICTURE = 101;
	private static final int OPEN_DOCUMENT = 110;

	/**
	 * Fragment managing the behaviors, interactions and presentation of the
	 * navigation drawer.
	 */
	private NavigationDrawerFragment mNavigationDrawerFragment;

	/**
	 * Used to store the last screen title. For use in
	 * {@link #restoreActionBar()}.
	 */
	private CharSequence mTitle;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		PreferenceManager.setDefaultValues(this, R.xml.pref_notification, false);

		mNavigationDrawerFragment = (NavigationDrawerFragment) getSupportFragmentManager()
				.findFragmentById(R.id.navigation_drawer);
		mTitle = getTitle();

		if (savedInstanceState == null) {
			getSupportFragmentManager().beginTransaction()
			.add(R.id.container, new MainFragment(), DEFAULT_FRAGMENT_TAG).commit();
		}
		// Set up the drawer.
		mNavigationDrawerFragment.setUp(R.id.navigation_drawer,
				(DrawerLayout) findViewById(R.id.drawer_layout));
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		switch (item.getItemId()) {
		case R.id.action_settings:
			Intent settingsIntent = new Intent(this, SettingsActivity.class);
			startActivity(settingsIntent);
			return true;
		case R.id.action_new:
			addNewPet();
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	protected void onResume() {
		// TODO maybe polish this a bit

		long start = Calendar.getInstance().getTimeInMillis();
		// if the last fragment was a no-pets fragment, load the normal one and query db again.
		if (getSupportFragmentManager().findFragmentByTag(MainFragmentNoPets.TAG) != null) {
			getSupportFragmentManager().beginTransaction().replace(R.id.container, 
					new MainFragment(), DEFAULT_FRAGMENT_TAG).commit();
		}
		Log.v("MainActivity", "onResume() took " + String.valueOf(Calendar.getInstance().getTimeInMillis() - start) + "ms");
		super.onResume();
	}

	private void addNewPet() {
		Intent intent = new Intent(this, PetEditActivity.class);
		// the lack of intent extras tells it it's a new entry
		startActivity(intent);
	}

	@Override
	public void onInfoButtonClick(Uri petUri) {
		Intent intent = new Intent(this, PetInfoActivity.class);
		intent.putExtra(PetsContract.Pets.CONTENT_ITEM_TYPE, petUri);
		startActivity(intent);
	}

	@Override
	public void onMedsButtonClick(Uri petUri) {
		Intent intent = new Intent(this, MedsListActivity.class);
		intent.putExtra(PetsContract.Pets.CONTENT_ITEM_TYPE, petUri);
		startActivity(intent);
	}

	@Override
	public void defaultPetChosen() {
		mNavigationDrawerFragment.startFillDrawerTask(); // make the right pet be selected
	}

	@Override
	public void noPetsFound() {
		// create new fragment with a tip to add a new pet
		Fragment newFragment = new MainFragmentNoPets();
		FragmentTransaction ft = getSupportFragmentManager().beginTransaction();

		ft.replace(R.id.container, newFragment, MainFragmentNoPets.TAG); 
//		ft.addToBackStack(null);

		ft.commitAllowingStateLoss(); // the regular commit() isn't allowed inside onLoadFinished()
	}

	@Override
	public void onNotifsButtonClick(Uri petUri) {
		// TODO unimplemented
	}

	@Override
	public void onVaccsButtonClick(Uri petUri) {
		Intent intent = new Intent(this, VaccListActivity.class);
		intent.putExtra(PetsContract.Pets.CONTENT_ITEM_TYPE, petUri);
		startActivity(intent);
	}

	@Override
	public void onVisitsButtonClick(Uri petUri) {
		Intent intent = new Intent(this, VetVisitsListActivity.class);
		intent.putExtra(PetsContract.Pets.CONTENT_ITEM_TYPE, petUri);
		startActivity(intent);
	}

	@SuppressLint("InlinedApi")
	@Override
	public void onFindPicureButtonPressed() {
		if (Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB) {
			Intent imgIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
			startActivityForResult(Intent.createChooser(imgIntent, "Select picture"), SELECT_PICTURE);
		}
		else if (Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT) {
			Intent imgIntent = new Intent();
			imgIntent.setType("image/*").setAction(Intent.ACTION_GET_CONTENT);
			imgIntent.putExtra(Intent.EXTRA_LOCAL_ONLY, true); // only allow local files, no network
			startActivityForResult(Intent.createChooser(imgIntent, "Select picture"), SELECT_PICTURE);
		}
		else {
			Intent imgIntent = new Intent(Intent.ACTION_GET_CONTENT); // has more sources than ACTION_OPEN_DOCUMENT
			imgIntent.addCategory(Intent.CATEGORY_OPENABLE);
			imgIntent.setType("image/*");
			startActivityForResult(imgIntent, OPEN_DOCUMENT);
		}
	}

	@Override
	public void onNavigationDrawerItemSelected(int position, long id) {
		// need to cross-navigate without adding to back stack
		if (id < 0) { // petId's will be positive
			if (id == NavigationDrawerFragment.ID_SETTINGS) {
				Intent settingsIntent = new Intent(this, SettingsActivity.class);
				startActivity(settingsIntent);
			}
			else if (id == NavigationDrawerFragment.ID_MEDICATIONS) {
				Intent intent = new Intent(this, MedsListActivity.class);
				TaskStackBuilder.create(this)
					// Add all of this activity's parents to the back stack
					.addNextIntentWithParentStack(intent)
					// Navigate up to the closest parent
					.startActivities();
			}
			else if (id == NavigationDrawerFragment.ID_VACCINATIONS) {
				Intent intent = new Intent(this, VaccListActivity.class);
				TaskStackBuilder.create(this)
					// Add all of this activity's parents to the back stack
					.addNextIntentWithParentStack(intent)
					// Navigate up to the closest parent
					.startActivities();
			}
			else if (id == NavigationDrawerFragment.ID_VET_VISITS) {
				Intent intent = new Intent(this, VetVisitsListActivity.class);
				TaskStackBuilder.create(this)
					// Add all of this activity's parents to the back stack
					.addNextIntentWithParentStack(intent)
					// Navigate up to the closest parent
					.startActivities();
			}
			else if (id == NavigationDrawerFragment.ID_ADD_PET) {
				Intent intent = new Intent(this, PetEditActivity.class);
				// the lack of intent extras tells it it's a new entry
				startActivity(intent);
			}
			else if (id == NavigationDrawerFragment.ID_DELETE_PET) {
				// pop up a confirmation dialog
				new ConfirmationFragment().show(getSupportFragmentManager(), ConfirmationFragment.TAG);
			}
		}
		else {
			// set new petId, then tell fragment to reload info
			AppUtils.setLastViewedPetID(this, id);
			Fragment fragment = getSupportFragmentManager().findFragmentByTag(DEFAULT_FRAGMENT_TAG);
			if (fragment instanceof NewPetSelected) {
				((NewPetSelected) fragment).onNewPetSelected();
			}
		}
	}

	@Override
	public void onDeletePetSelected(boolean confirmed) {
		if (confirmed) {
			long currentId = AppUtils.getLastViewedPetID(this);
			AppUtils.setLastViewedPetID(this, 0L); // set it to 0 to make the loader choose the first one in the list
			getContentResolver().delete(Uri.withAppendedPath(PetsContract.Pets.CONTENT_URI, String.valueOf(currentId)), null, null);
			AppUtils.deleteImageFile(this, String.valueOf(currentId));
			// not needed in when already in main activity
//			Intent intent = new Intent(this, MainActivity.class);
//			TaskStackBuilder.create(this)
//				.addNextIntentWithParentStack(intent) // Add all of this activity's parents to the back stack
//				.startActivities(); // Navigate up to the closest parent
		}
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (resultCode == Activity.RESULT_OK) {
			if (requestCode == SELECT_PICTURE || requestCode == OPEN_DOCUMENT) {
				Uri imgUri = data.getData();
				if (imgUri == null) {
					Log.e(TAG, "null URI");
					AppUtils.makeToast(this, "Picture could not be loaded!", Toast.LENGTH_LONG);
					return;
				}
				
				if (requestCode == SELECT_PICTURE) {
					ImageSaverFragment fragment = (ImageSaverFragment) getSupportFragmentManager().
							findFragmentByTag(DEFAULT_FRAGMENT_TAG);
					if (fragment != null) {
						fragment.newImageSelected(imgUri);
					}
				}
				else { //(requestCode == OPEN_DOCUMENT)
					ImageSaverFragment fragment = (ImageSaverFragment) getSupportFragmentManager().
							findFragmentByTag(DEFAULT_FRAGMENT_TAG);
					if (fragment != null) {
						fragment.newImageSelectedKitKat(imgUri);
					}
				}
			}
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		if (!mNavigationDrawerFragment.isDrawerOpen()) {
			// Only show items in the action bar relevant to this screen
			// if the drawer is not showing. Otherwise, let the drawer
			// decide what to show in the action bar.
			getMenuInflater().inflate(R.menu.main, menu);
			if (getSupportFragmentManager().findFragmentByTag(MainFragmentNoPets.TAG) != null) {
				MenuItem newPicture = menu.findItem(R.id.action_new_picture);
				if (newPicture != null) {
					newPicture.setVisible(false);
				}
			}
			restoreActionBar();
			return true;
		}
		return super.onCreateOptionsMenu(menu);
	}

	private void restoreActionBar() {
		ActionBar actionBar = getSupportActionBar();
		actionBar.setDisplayShowTitleEnabled(true);
		actionBar.setTitle(mTitle);
	}
}
