package com.paulro.petkeeper;

import java.io.File;
import java.util.Calendar;

import android.app.Activity;
import android.app.NotificationManager;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.app.ActionBarActivity;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.paulro.petkeeper.utilities.AppUtils;
import com.paulro.petkeeper.provider.PetsContract;
import com.paulro.petkeeper.provider.PetsContract.Meds;
import com.paulro.petkeeper.provider.PetsContract.Vaccinations;
import com.paulro.petkeeper.provider.PetsContract.VetVisits;
import com.paulro.petkeeper.utilities.LoadBitmapTask;

public class NotificationClickFragment extends Fragment implements LoaderManager.LoaderCallbacks<Cursor> {

	public static final String TAG = NotificationClickFragment.class.getSimpleName();

	private static final int LOADER_ID_MEDS = 101;
	private static final int LOADER_ID_VACCS = 102;
	private static final int LOADER_ID_VISITS = 103;

	private NotificationManager mManager;

	private String actionString;
	private Uri dataUri;
	private int notifID = 0;
	private int mNotifType = LOADER_ID_MEDS;
	private TextView mDescription;
	private TextView mPetName;
	private ImageView mPicture;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		mManager = (NotificationManager) getActivity().getSystemService(Context.NOTIFICATION_SERVICE);
		actionString = getActivity().getIntent().getAction();
		dataUri = getActivity().getIntent().getData();

		String uriType = getActivity().getContentResolver().getType(dataUri);
		switch (uriType) { // start the right kind of loader
		case Meds.CONTENT_ITEM_TYPE:
			getLoaderManager().initLoader(LOADER_ID_MEDS, null, this);
			break;
		case Vaccinations.CONTENT_ITEM_TYPE:
			mNotifType = LOADER_ID_VACCS;
			getLoaderManager().initLoader(LOADER_ID_VACCS, null, this);
			break;
		case VetVisits.CONTENT_ITEM_TYPE:
			mNotifType = LOADER_ID_VISITS;
			getLoaderManager().initLoader(LOADER_ID_VISITS, null, this);
			break;
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View view = inflater.inflate(R.layout.fragment_notification_click, container, false);
		Button snoozeBtn = (Button) view.findViewById(R.id.btn_snooze);
		Button medGivenBtn = (Button) view.findViewById(R.id.btn_med_given);
		mDescription = (TextView) view.findViewById(R.id.notification_description);
		mPetName = (TextView) view.findViewById(R.id.pet_name);
		mPicture = (ImageView) view.findViewById(R.id.pet_info_picture);

		if (mNotifType == LOADER_ID_MEDS) { // set button action and label based on notification type
			medGivenBtn.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					medGiven();
				}
			});
		}
		else {
			if (mNotifType == LOADER_ID_VACCS) { // change title based on notification type
				if (getActivity() instanceof ActionBarActivity) {
					((ActionBarActivity) getActivity()).getSupportActionBar()
					.setTitle(getString(R.string.title_activity_notification_click_vacc));
				}
			}
			else {
				if (getActivity() instanceof ActionBarActivity) {
					((ActionBarActivity) getActivity()).getSupportActionBar()
					.setTitle(getString(R.string.title_activity_notification_click_visit));
				}
			}
			medGivenBtn.setText(getString(R.string.btn_dismiss));
			medGivenBtn.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					dismissButton();
				}
			});
		}

		snoozeBtn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				snoozePressed();
			}
		});

		return view;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
	}

	protected void dismissButton() {
		mManager.cancel(notifID);
		
		ContentValues values = new ContentValues();
		values.put(Vaccinations.NOTIF_IS_SET, false); // row name is the same for all tables
		getActivity().getContentResolver().update(dataUri, values, null, null);
		
		getActivity().setResult(Activity.RESULT_OK);
		getActivity().finish();
	}

	protected void medGiven() {
		mManager.cancel(notifID);

		ContentValues values = new ContentValues();
		values.put(PetsContract.Meds.DATE_MOST_RECENT, Calendar.getInstance().getTimeInMillis());
		getActivity().getContentResolver().update(dataUri, values, null, null); // notification is not deactivated here

		Intent serviceIntent = new Intent(getActivity(), AlarmSetService.class);
		serviceIntent.setData(dataUri).setAction(TAG);
		getActivity().startService(serviceIntent); // update the time of the alarm

		getActivity().setResult(Activity.RESULT_OK);
		getActivity().finish(); // end activity
	}

	protected void snoozePressed() {
		// set alarm in 10 minutes
		Intent snoozeIntent = new Intent(getActivity(), AlarmSnoozeService.class);
		snoozeIntent.setAction(actionString).setData(dataUri);
		snoozeIntent.putExtra(PetsContract.NotificationId.TBL_NAME, notifID);
		getActivity().startService(snoozeIntent);

		getActivity().setResult(Activity.RESULT_OK);
		getActivity().finish(); // end activity
	}

	@Override
	public Loader<Cursor> onCreateLoader(int id, Bundle args) {
		switch (id) {
		case LOADER_ID_MEDS:
			return new CursorLoader(getActivity(), dataUri, Meds.PROJECTION_ALL, null, null, null);
		case LOADER_ID_VACCS:
			return new CursorLoader(getActivity(), dataUri, Vaccinations.PROJECTION_ALL, null, null, null);
		case LOADER_ID_VISITS:
		default: // add default to make error go away
			return new CursorLoader(getActivity(), dataUri, VetVisits.PROJECTION_ALL, null, null, null);
		}
	}

	@Override
	public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
		int loaderID = loader.getId();
		if (data != null && data.getCount() > 0) {
			data.moveToFirst();
			long petID;

			if (loaderID == LOADER_ID_MEDS) {
				notifID = data.getInt(data.getColumnIndexOrThrow(Meds.NOTIF_ID));
				petID = data.getLong(data.getColumnIndexOrThrow((Meds.PET_ID)));
				setImage(petID);
				String petName = getPetName(petID);

				mPetName.setText(petName);
				long notificationFrequency = data.getLong(data.getColumnIndexOrThrow(Meds.FREQUENCY));
//				long nextNotifTime = data.getLong(data.getColumnIndexOrThrow(Meds.NOTIF_DATE_TIME));
				long dateMostRecent = data.getLong(data.getColumnIndexOrThrow(Meds.DATE_MOST_RECENT));
				long notificationTime = getLeastRecentMedTime(dateMostRecent, notificationFrequency);
				mDescription.setText(AppUtils.formatDateTime(notificationTime, getActivity()) +": It's time to give " +
						data.getString(data.getColumnIndexOrThrow(Meds.NAME)) + " to " + petName);
			}
			else if (loaderID == LOADER_ID_VACCS) {
				notifID = data.getInt(data.getColumnIndexOrThrow(Vaccinations.NOTIF_ID));
				petID = data.getLong(data.getColumnIndexOrThrow(Vaccinations.PET_ID));
				setImage(petID);
				String petName = getPetName(petID);

				mPetName.setText(petName); // TODO change the text & use resources for localisation 
				long expirationTime = data.getLong(data.getColumnIndexOrThrow(Vaccinations.EXPIRATION));
				mDescription.setText(data.getString(data.getColumnIndexOrThrow(Vaccinations.NAME)) + " will expire on " + 
						AppUtils.formatDate(expirationTime, getActivity()));
			}
			else if (loaderID == LOADER_ID_VISITS) {
				notifID = data.getInt(data.getColumnIndexOrThrow(VetVisits.NOTIF_ID));
				petID = data.getLong(data.getColumnIndexOrThrow(VetVisits.PET_ID));
				setImage(petID);
				String petName = getPetName(petID);

				mPetName.setText(petName);
				long nextVisitTime = data.getLong(data.getColumnIndexOrThrow(VetVisits.NEXT_APPOINTMENT_DATE));
				mDescription.setText(petName + " has an appointment with " + 
						data.getString(data.getColumnIndexOrThrow(VetVisits.DOCTOR)) + " on " + 
						AppUtils.formatDateTime(nextVisitTime, getActivity()));
			}
			else {
				Log.e(TAG, "an invalid loaderID was found: " + loaderID);
			}
		}
	}

	@Override
	public void onLoaderReset(Loader<Cursor> loader) {
	}

	private String getPetName(long petID) { // helper method for onLoadFinished
		Cursor cursor = getActivity().getContentResolver().query(PetsContract.Pets.CONTENT_URI, new String[]
				{ PetsContract.Pets.NAME }, PetsContract.SELECTION_BY_ID, new String[] { String.valueOf(petID) }, null);
		cursor.moveToFirst();
		String petName = cursor.getString(cursor.getColumnIndexOrThrow(PetsContract.Pets.NAME));
		cursor.close();

		return petName;
	}

	private long getLeastRecentMedTime(long dateMostRecent, long frequencyIndex) {
		// show the time that the med is due to be given, when the next one is what is stored
		Cursor cursor = getActivity().getContentResolver().query(PetsContract.NotificationFrequency.CONTENT_URI,
				new String[] { PetsContract.NotificationFrequency.TIME_VALUE, PetsContract.NotificationFrequency.TIME_UNIT },
				PetsContract.SELECTION_BY_ID, new String[] { String.valueOf(frequencyIndex) }, null);
		cursor.moveToFirst();
		int timeValue = cursor.getInt(cursor.getColumnIndexOrThrow(PetsContract.NotificationFrequency.TIME_VALUE));
		PetsContract.NotificationFrequency.TimeUnits timeUnit =
				PetsContract.NotificationFrequency.TimeUnits.valueOf(cursor.getString(
						cursor.getColumnIndexOrThrow(PetsContract.NotificationFrequency.TIME_UNIT)));
		cursor.close();

		Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis(dateMostRecent);

		// add 1 interval to find out when medication was first due (in case of multiple missed doses)
		AppUtils.updateCalendar(cal, timeUnit.getCalendarConstant(), timeValue, TAG);
		return cal.getTimeInMillis();
	}

	private void setImage(long petID) {
		File imageFile = getActivity().getFileStreamPath(AppUtils.imageFileName(String.valueOf(petID)));

		if (imageFile.exists()) {
			DisplayMetrics dMetrics = getDisplayMetrics();

			LoadBitmapTask loadTask = new LoadBitmapTask(mPicture, dMetrics.heightPixels / 3) {
				@Override
				public void onImageLoadError() {
					setDefaultImage();
				}
			};
			loadTask.execute(imageFile.getPath());
		}
		else {
			setDefaultImage();
		}
	}

	private void setDefaultImage() {
		Bitmap defaultBmp = BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher);
		ViewGroup.LayoutParams lParams = mPicture.getLayoutParams();

		DisplayMetrics dMetrics = getDisplayMetrics();

		// these numbers are just to get an estimate of how it should look
		lParams.height = Math.max(Math.min(dMetrics.heightPixels / 3, defaultBmp.getHeight()), dMetrics.heightPixels / 5);

		mPicture.setImageBitmap(defaultBmp);
	}

	private DisplayMetrics getDisplayMetrics() {
		DisplayMetrics dMetrics = new DisplayMetrics();
		getActivity().getWindowManager().getDefaultDisplay().getMetrics(dMetrics);
		return dMetrics;
	}
}
