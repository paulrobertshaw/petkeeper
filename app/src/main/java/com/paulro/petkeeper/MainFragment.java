package com.paulro.petkeeper;

import java.io.File;

import android.app.Activity;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.paulro.petkeeper.utilities.AppUtils;
import com.paulro.petkeeper.provider.PetsContract;
import com.paulro.petkeeper.provider.PetsContract.Pets;
import com.paulro.petkeeper.utilities.ImageSaverFragment;
import com.paulro.petkeeper.utilities.LoadBitmapTask;
import com.paulro.petkeeper.utilities.NewPetSelected;
import com.paulro.petkeeper.utilities.SaveImageFromFileTask;

public class MainFragment extends Fragment implements LoaderManager.LoaderCallbacks<Cursor>, ImageSaverFragment,
		NewPetSelected {

	public interface ButtonListeners {
		public void onInfoButtonClick(Uri petUri);
		public void onMedsButtonClick(Uri petUri);
		public void onVaccsButtonClick(Uri petUri);
		public void onVisitsButtonClick(Uri petUri);
		public void onNotifsButtonClick(Uri petUri);
		public void onFindPicureButtonPressed();
	}
	public interface MissingPetsListener {
		public void defaultPetChosen();
		public void noPetsFound();
	}

	public static String TAG = MainFragment.class.getSimpleName();
	private static int LOADER_ID = 5;

	private ButtonListeners mButtonListeners; // info button pressed
	private MissingPetsListener mMissingPetsListener;

	private Uri petUri;
	private long mDefaultPetID = 0;

	private Button infoButton;
	private Button medsButton;
	private Button vaccsButton;
	private Button visitsButton;
	private Button notifsButton;
	private TextView mTvName;
	private ImageView mPicture;

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		try {
			mButtonListeners = (ButtonListeners) activity;
			mMissingPetsListener = (MissingPetsListener) activity;
		}
		catch (Exception e) {
			throw new ClassCastException(e.getLocalizedMessage());
		}
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setHasOptionsMenu(true);
		getLoaderManager().initLoader(LOADER_ID, null, this);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_main, container, false);
		infoButton = (Button) view.findViewById(R.id.btn_info);
		medsButton = (Button) view.findViewById(R.id.btn_medications);
		vaccsButton = (Button) view.findViewById(R.id.btn_vaccinations);
		visitsButton = (Button) view.findViewById(R.id.btn_visits);
		notifsButton = (Button) view.findViewById(R.id.btn_notifications);
		setButtonsVisibility(false); // don't show buttons until load finishes

		mTvName = (TextView) view.findViewById(R.id.pet_name);
		mPicture = (ImageView) view.findViewById(R.id.pet_info_picture);

		infoButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				mButtonListeners.onInfoButtonClick(petUri);
			}
		});
		medsButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				mButtonListeners.onMedsButtonClick(petUri);
			}
		});
		vaccsButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				mButtonListeners.onVaccsButtonClick(petUri);
			}
		});
		visitsButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				mButtonListeners.onVisitsButtonClick(petUri);
			}
		});
		notifsButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				mButtonListeners.onNotifsButtonClick(petUri);
			}
		});

        mPicture.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                mButtonListeners.onFindPicureButtonPressed();
            }
        });

		return view;
	}

	@Override
	public void onDetach() {
		super.onDetach();
		mButtonListeners = null;
		mMissingPetsListener = null;
	}

	@Override
	public void onResume() {
		super.onResume();
		reloadPet(); // in case the pet changes while in another activity, and back button is pressed
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.action_new_picture:
			mButtonListeners.onFindPicureButtonPressed();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	private void lookupDefaultPetId() {
		mDefaultPetID = AppUtils.getLastViewedPetID(getActivity()); // if record cannot be found, return 0
		if (mDefaultPetID > 0) {
			petUri = Uri.withAppendedPath(PetsContract.Pets.CONTENT_URI, String.valueOf(mDefaultPetID));
		}
	}
	
	private void setButtonsVisibility(boolean isVisible) {
		int visibility = android.view.View.VISIBLE;
		if (!isVisible) {
			visibility = android.view.View.INVISIBLE;
		}
		infoButton.setVisibility(visibility);
		medsButton.setVisibility(visibility);
		vaccsButton.setVisibility(visibility);
		visitsButton.setVisibility(visibility);
		notifsButton.setVisibility(android.view.View.INVISIBLE); // to be implemented in a later update
	}

	@Override
	public void newImageSelected(Uri imgUri) {

		DisplayMetrics dMetrics = new DisplayMetrics();
		getActivity().getWindowManager().getDefaultDisplay().getMetrics(dMetrics);
		int maxDimen = Math.max(dMetrics.heightPixels, dMetrics.widthPixels);

		SaveImageFromFileTask task = new SaveImageFromFileTask(this,
				maxDimen, petUri.getLastPathSegment(), false);
		task.execute(imgUri);
	}

	@Override
	public void newImageSelectedKitKat(Uri imgUri) {
		DisplayMetrics dMetrics = new DisplayMetrics();
		getActivity().getWindowManager().getDefaultDisplay().getMetrics(dMetrics);
		int maxDimen = Math.max(dMetrics.heightPixels, dMetrics.widthPixels);

		SaveImageFromFileTask task = new SaveImageFromFileTask(this,
				maxDimen, petUri.getLastPathSegment(), true);
		task.execute(imgUri);
	}

	@Override
	public void setNewImage() { // load new picture
		setImage();
	}

	private void setImage() { // runs from AsyncTask to not block UI thread
		File imageFile = getActivity().getFileStreamPath(AppUtils.imageFileName(petUri.getLastPathSegment()));

		if (imageFile.exists()) {
			DisplayMetrics dMetrics = new DisplayMetrics();
			getActivity().getWindowManager().getDefaultDisplay().getMetrics(dMetrics);

			// scale to height of screen, could be different depending on landscape/portrait mode
			// for now just set the max picture size to a fraction of the screen
			LoadBitmapTask task = new LoadBitmapTask(mPicture, dMetrics.heightPixels / 4) {
				@Override
				public void onImageLoadError() {
					setDefaultImage();
				}
			};
			task.execute(imageFile.getPath());
		}
		else { // no image has been defined for this pet, use a default icon
			setDefaultImage();
		}
	}

	protected void setDefaultImage() {
		Log.v(TAG, "using default image");
		Bitmap defaultBitmap = BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher);
		// resize image in case there was a failed image load
		LayoutParams lParams = mPicture.getLayoutParams();
		DisplayMetrics dMetrics = new DisplayMetrics();
		getActivity().getWindowManager().getDefaultDisplay().getMetrics(dMetrics);
		lParams.height = Math.max(Math.min(dMetrics.heightPixels / 3, defaultBitmap.getHeight()), dMetrics.heightPixels / 6);

		mPicture.setImageBitmap(defaultBitmap);
	}

	private void reloadPet() {
		getLoaderManager().restartLoader(LOADER_ID, null, this); // load new pet's data
	}

	@Override
	public void onNewPetSelected() {
		reloadPet();
	}

	@Override
	public Loader<Cursor> onCreateLoader(int id, Bundle args) {
		return new CursorLoader(getActivity(), PetsContract.Pets.CONTENT_URI, PetsContract.Pets.PROJECTION_ALL, null, null, null);
	}

	@Override
	public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
//		long start = Calendar.getInstance().getTimeInMillis();

		if (data != null && data.getCount() > 0) {
			data.moveToFirst();

			int colIdIndex = data.getColumnIndexOrThrow(Pets._ID);
			boolean defaultRecordFound = true;
			lookupDefaultPetId(); // check for changes since last load
			while (data.getLong(colIdIndex) != mDefaultPetID) {
				data.moveToNext();
				if (data.isAfterLast()) {
					defaultRecordFound = false; 
					break;
				}
			}
			// the record in the prefs file was not found
			if (!defaultRecordFound) {
				data.moveToFirst();
				long firstPetID = data.getLong(colIdIndex);
				petUri = Uri.withAppendedPath(PetsContract.Pets.CONTENT_URI, String.valueOf(firstPetID));
				Log.v(TAG, "failover petUri = " + petUri);
				// update sharedprefs
				AppUtils.setLastViewedPetID(getActivity(),  firstPetID);
				mMissingPetsListener.defaultPetChosen();
			}
			mTvName.setText(data.getString(data.getColumnIndexOrThrow(Pets.NAME)));
			setButtonsVisibility(true);
			// get picture, update other fields if I add any later
			setImage();

//			Log.v(TAG, "onLoadFinished() took " + String.valueOf(Calendar.getInstance().getTimeInMillis() - start) + "ms");
		}
		else {
			Log.v(TAG, "onLoadFinished(): No cursor rows were returned!");
			mMissingPetsListener.noPetsFound();
		}
	}

	@Override
	public void onLoaderReset(Loader<Cursor> loader) {
	}
}
