package com.paulro.petkeeper;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.database.Cursor;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.paulro.petkeeper.utilities.AppUtils;
import com.paulro.petkeeper.provider.PetsContract;
import com.paulro.petkeeper.provider.PetsContract.Pets;

/**
 * Fragment used for managing interactions for and presentation of a navigation drawer.
 * See the <a href="https://developer.android.com/design/patterns/navigation-drawer.html#Interaction">
 * design guidelines</a> for a complete explanation of the behaviors implemented here.
 */
public class NavigationDrawerFragment extends Fragment {

	public static final long ID_SETTINGS = -1L;
	public static final long ID_MEDICATIONS =-2L;
	public static final long ID_VACCINATIONS = -3L;
	public static final long ID_VET_VISITS = -4L;
	public static final long ID_ADD_PET = -99L;
	public static final long ID_DELETE_PET = -100L;

	private static final String TAG = NavigationDrawerFragment.class.getSimpleName();
	private static final long ID_DO_NOTHING = -101L;

	/**
	 * Remember the position of the selected item.
	 */
	private static final String STATE_SELECTED_POSITION = "selected_position";

	/**
	 * Per the design guidelines, you should show the drawer on launch until the user manually
	 * expands it. This shared preference tracks this.
	 */
	private static final String PREF_USER_LEARNED_DRAWER = "navigation_drawer_learned";

	/**
	 * A pointer to the current callbacks instance (the Activity).
	 */
	private NavigationDrawerCallbacks mCallbacks;

	/**
	 * Helper component that ties the action bar to the navigation drawer.
	 */
	private ActionBarDrawerToggle mDrawerToggle;

	private DrawerLayout mDrawerLayout;
	private ListView mDrawerListView;
	private View mFragmentContainerView;

	private int mCurrentSelectedPosition = 0;
	private boolean mFromSavedInstanceState;
	private boolean mUserLearnedDrawer;

	private FillDrawerTask mTask = null;

	public NavigationDrawerFragment() {
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// Read in the flag indicating whether or not the user has demonstrated awareness of the
		// drawer. See PREF_USER_LEARNED_DRAWER for details.
		SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(getActivity());
		mUserLearnedDrawer = sp.getBoolean(PREF_USER_LEARNED_DRAWER, false);

		if (savedInstanceState != null) {
			mCurrentSelectedPosition = savedInstanceState.getInt(STATE_SELECTED_POSITION);
			mFromSavedInstanceState = true;
		}

	}

	@Override
	public void onActivityCreated (Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		// Indicate that this fragment would like to influence the set of actions in the action bar.
		setHasOptionsMenu(true);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		Log.v(TAG, "onCreateView called");
		mDrawerListView = (ListView) inflater.inflate(R.layout.fragment_navigation_drawer, container, false);

		mDrawerListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				selectItem(position, id);
			}
		});
		return mDrawerListView;
	}

	public boolean isDrawerOpen() {
		return mDrawerLayout != null && mDrawerLayout.isDrawerOpen(mFragmentContainerView);
	}

	/**
	 * Users of this fragment must call this method to set up the navigation drawer interactions.
	 *
	 * @param fragmentId   The android:id of this fragment in its activity's layout.
	 * @param drawerLayout The DrawerLayout containing this fragment's UI.
	 */
	public void setUp(int fragmentId, DrawerLayout drawerLayout) {
		Log.v(TAG, "setUp called");
		mFragmentContainerView = getActivity().findViewById(fragmentId);
		mDrawerLayout = drawerLayout;

		// set a custom shadow that overlays the main content when the drawer opens
		mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);
		// set up the drawer's list view with items and click listener

		ActionBar actionBar = getActionBar();
		actionBar.setDisplayHomeAsUpEnabled(true);
		actionBar.setHomeButtonEnabled(true);

		// ActionBarDrawerToggle ties together the the proper interactions
		// between the navigation drawer and the action bar app icon.
		mDrawerToggle = new ActionBarDrawerToggle(
				getActivity(),                    /* host Activity */
				mDrawerLayout,                    /* DrawerLayout object */
//				R.drawable.ic_drawer,             /* nav drawer image to replace 'Up' caret */
				R.string.navigation_drawer_open,  /* "open drawer" description for accessibility */
				R.string.navigation_drawer_close  /* "close drawer" description for accessibility */
				) {
			@Override
			public void onDrawerClosed(View drawerView) {
				super.onDrawerClosed(drawerView);
				if (!isAdded()) {
					return;
				}

				getActivity().supportInvalidateOptionsMenu(); // calls onPrepareOptionsMenu()
			}

			@Override
			public void onDrawerOpened(View drawerView) {
				super.onDrawerOpened(drawerView);
				if (!isAdded()) {
					return;
				}

				if (!mUserLearnedDrawer) {
					// The user manually opened the drawer; store this flag to prevent auto-showing
					// the navigation drawer automatically in the future.
					mUserLearnedDrawer = true;
					SharedPreferences sp = PreferenceManager
							.getDefaultSharedPreferences(getActivity());
					sp.edit().putBoolean(PREF_USER_LEARNED_DRAWER, true).apply();
				}

				getActivity().supportInvalidateOptionsMenu(); // calls onPrepareOptionsMenu()
			}
		};

		// If the user hasn't 'learned' about the drawer, open it to introduce them to the drawer,
		// per the navigation drawer design guidelines.
		if (!mUserLearnedDrawer && !mFromSavedInstanceState) {
			mDrawerLayout.openDrawer(mFragmentContainerView);
		}

		// Defer code dependent on restoration of previous instance state.
		mDrawerLayout.post(new Runnable() {
			@Override
			public void run() {
				mDrawerToggle.syncState();
			}
		});

		mDrawerLayout.setDrawerListener(mDrawerToggle);
	}

	public void startFillDrawerTask() {
		stopFillDrawerTask(); // stop it if it's currently running

		if (mDrawerListView != null) {

			Log.v(TAG, "startFillDrawerTask called");
			long petId = AppUtils.getLastViewedPetID(getActivity());
			mTask = new FillDrawerTask();
			mTask.execute(petId);
		}
	}

	private void stopFillDrawerTask() {
		if (mTask != null) {
			mTask.cancel(true);
			mTask = null; // remove reference?
		}
	}

	protected void setListViewAdapter(ArrayList<NavListItem> list, int currentPetIndex) {
		if (mDrawerListView != null) {
//    		mDrawerListView.setAdapter(new ArrayAdapter<NavListItem>(
//    				getActionBar().getThemedContext(),
//    				R.layout.list_item_activated_1,
//    				R.id.list_item,
//    				list));
			mDrawerListView.setAdapter(new DrawerListAdapter(getActivity(), // getThemedContext()?
					R.layout.list_item_activated_1, R.id.list_item, list));

			mCurrentSelectedPosition = currentPetIndex;
			// Select the current pet
			mDrawerListView.setItemChecked(mCurrentSelectedPosition, true);
		}
	}

	private void selectItem(int position, long id) {
		if (id < 0) { // don't highlight things like 'delete pet' and lose focus of which pet is active
			mDrawerListView.setItemChecked(mCurrentSelectedPosition, true);
		}
		else { // positive id's are cursor row id's
			mCurrentSelectedPosition = position;
		}

		if (id != ID_DO_NOTHING) { // don't want clicking on headers or inactive buttons to close drawer
			if (mDrawerLayout != null) {
				mDrawerLayout.closeDrawer(mFragmentContainerView);
			}
			if (mCallbacks != null) {
				mCallbacks.onNavigationDrawerItemSelected(position, id);
			}
		}
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		try {
			mCallbacks = (NavigationDrawerCallbacks) activity;
		} catch (ClassCastException e) {
			throw new ClassCastException("Activity must implement NavigationDrawerCallbacks.");
		}
	}

	@Override
	public void onDetach() {
		super.onDetach();
		mCallbacks = null;
	}

	@Override
	public void onStop() {
		super.onStop();
		stopFillDrawerTask();
	}

	@Override
	public void onResume() {
		super.onResume();
		startFillDrawerTask(); // call here to make it reload in case there is an update to the pet list
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putInt(STATE_SELECTED_POSITION, mCurrentSelectedPosition);
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		// Forward the new configuration the drawer toggle component.
		mDrawerToggle.onConfigurationChanged(newConfig);
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		// If the drawer is open, show the global app actions in the action bar. See also
		// showGlobalContextActionBar, which controls the top-left area of the action bar.
		if (mDrawerLayout != null && isDrawerOpen()) {
//            inflater.inflate(R.menu.global, menu);
			showGlobalContextActionBar();
		}
		super.onCreateOptionsMenu(menu, inflater);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (mDrawerToggle.onOptionsItemSelected(item)) {
			return true;
		}

		return super.onOptionsItemSelected(item);
	}

	/**
	 * Per the navigation drawer design guidelines, updates the action bar to show the global app
	 * 'context', rather than just what's in the current screen.
	 */
	private void showGlobalContextActionBar() {
		ActionBar actionBar = getActionBar();
		actionBar.setDisplayShowTitleEnabled(true);
		actionBar.setTitle(R.string.app_name);
	}

	private ActionBar getActionBar() {
		return ((ActionBarActivity) getActivity()).getSupportActionBar();
	}

	/**
	 * Callbacks interface that all activities using this fragment must implement.
	 */
	public static interface NavigationDrawerCallbacks {
		/**
		 * Called when an item in the navigation drawer is selected.
		 */
		void onNavigationDrawerItemSelected(int position, long id);

		/**
		 * Called when the 'Yes' button is selected in DialogFragment
		 */
		void onDeletePetSelected(boolean confirmed);
	}

	private class NavListItem {
		private final String name;
		private final long id;
		private boolean isHeader;

		public NavListItem(String name, long rowId, boolean isHeader) {
			this.name = name;
			this.id = rowId;
			this.isHeader = isHeader;
		}

		public String getName() {
			return name;
		}

		public long getId() {
			return id;
		}

		public boolean isHeader() {
			return isHeader;
		}

		@Override
		public String toString() {
			// toString() is what default ArrayAdapter uses; I ended up writing my own though
			return getName();
		}
	}

	private class FillDrawerTask extends AsyncTask<Long, Void, ArrayList<NavListItem>> {

		final int staticItems = 8; // number of items without pets
		int currentPetIndex = 0;

		@Override
		protected ArrayList<NavListItem> doInBackground(Long... params) {
			long currentPetId = params[0];
			ArrayList<NavListItem> items;
			Cursor cursor = getActivity().getContentResolver().query(PetsContract.Pets.CONTENT_URI, 
					new String[] { Pets._ID, Pets.NAME }, null, null, null);
			// query db and load all pets into ArrayList
			if (cursor != null && cursor.moveToFirst()) {
				int numPets = cursor.getCount();
				items = new ArrayList<>(staticItems + numPets);

				addStaticItems(items);
				// only need these if a pet is entered already
				items.add(items.size() - 1, new NavListItem("Medications", ID_MEDICATIONS, false));
				items.add(items.size() - 1, new NavListItem("Vaccinations", ID_VACCINATIONS, false));
				items.add(items.size() - 1, new NavListItem("Vet Visits", ID_VET_VISITS, false));

				while (!cursor.isAfterLast()) {
					long id = cursor.getLong(cursor.getColumnIndexOrThrow(Pets._ID));
					items.add(new NavListItem(cursor.getString(cursor.getColumnIndexOrThrow(Pets.NAME)), id, false));
					if (id == currentPetId) {
						currentPetIndex = items.size() - 1; // the index of the newest addition to the list
					}
					cursor.moveToNext();
				}
				Log.v(TAG, "pet index: " + currentPetIndex);
				// option to delete pet at the end of list, different id number from other static items
				items.add(new NavListItem("ACTIONS", ID_DO_NOTHING, true));
				items.add(new NavListItem("Add Pet", ID_ADD_PET, false));
				items.add(new NavListItem("Delete Current Pet", ID_DELETE_PET, false));
			}
			else {
				items = new ArrayList<>(staticItems - 1); // don't need the 'delete pet' option if there are no pets
				addStaticItems(items);
				items.add(new NavListItem("none", ID_DO_NOTHING, false)); // shows that there are no pets

				items.add(new NavListItem("ACTIONS", ID_DO_NOTHING, true));
				items.add(new NavListItem("Add Pet", ID_ADD_PET, false));
				currentPetIndex = items.size() - 1; // set highlighted field to 'Add Pet'
			}

			if (cursor != null) {
				cursor.close();
			}
			return items;
		}

		private void addStaticItems(ArrayList<NavListItem> items) { // no need to duplicate code
			items.add(new NavListItem("Settings", ID_SETTINGS, false)); // TODO what else here? update staticItems count above
			items.add(new NavListItem("YOUR PETS", ID_DO_NOTHING, true));
		}

		@Override
		protected void onPostExecute(ArrayList<NavListItem> result) { // will not run if task is cancelled
			setListViewAdapter(result, currentPetIndex);
		}
	}

	private class DrawerListAdapter extends ArrayAdapter<NavListItem> {
		private Context context;
		private int resourceId;
		private int textViewResourceId;
		private List<NavListItem> itemList;

		public DrawerListAdapter(Context context, int resource, int textViewResourceId, List<NavListItem> listItems) {
			super(context, resource, listItems);
			this.context = context;
			this.resourceId = resource;
			this.textViewResourceId = textViewResourceId;
			this.itemList = listItems;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			View view = convertView;
			TextView tv;

			NavListItem item = getItem(position);

			// rebuild view object, or the special formatting will be put on the wrong rows after scrolling
			// this should be easier than trying to set styling based on SDK level
			// from what I could tell, the old views are being gc'd well
			LayoutInflater inflater = LayoutInflater.from(context);
			view = inflater.inflate(resourceId, parent, false);

			try {
				tv = (TextView) view.findViewById(textViewResourceId);
				if (item.isHeader()) {
					tv.setBackgroundResource(R.drawable.list_section_divider_holo_dark);
					tv.setTextAppearance(getActivity(), R.style.holoTextViewListSeparator);
					tv.setGravity(android.view.Gravity.BOTTOM);
				}
			}
			catch (ClassCastException e) {
				String TAG = DrawerListAdapter.class.getSimpleName();
				Log.e(TAG, "textViewResourceId must be a TextView!");
				throw new IllegalStateException(TAG + " requires that textViewResourceId is a TextView", e);
			}

			// set background color on gingerbread, since it doesn't natively support 'activated' views
			if (Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB) {
				if (position == mCurrentSelectedPosition) {
					// a close approximation of the holo theme highlight color
					view.setBackgroundColor(Color.argb(192, 51, 173, 214));
				}
			}

			tv.setText(item.getName());
			return view;
		}

		@Override
		public NavListItem getItem(int position) {
			return itemList.get(position);
		}

		@Override
		public long getItemId(int position) {
			return getItem(position).getId(); // a petId will be positive, other menu items negative
		}
	}
}
