package com.paulro.petkeeper;

import android.app.AlarmManager;
import android.app.IntentService;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.SystemClock;
import android.preference.PreferenceManager;
import android.util.Log;

import com.paulro.petkeeper.provider.PetsContract;

public class AlarmSnoozeService extends IntentService {

	private static final String TAG = AlarmSnoozeService.class.getSimpleName();

	public AlarmSnoozeService() { // default constructor required
		super(TAG);
	}
	public AlarmSnoozeService(String name) {
		super(name);
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		Bundle extras = intent.getExtras();
		int notifID = 0;
		if (extras != null) { // get bundled unique ID
			notifID = extras.getInt(PetsContract.NotificationId.TBL_NAME);
			NotificationManager nManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
			nManager.cancel(notifID);
		}
		else {
			Log.e(TAG, "extras were null!");
		}

		// read snooze preference
		SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(this);
		int snoozeMinutes = Integer.parseInt(sharedPrefs.getString(getString(R.string.key_snooze_interval), "10"));

		AlarmManager alarmMgr = (AlarmManager) this.getSystemService(Context.ALARM_SERVICE);
		Intent alarmIntent = new Intent(this, AlarmReceiver.class);
		alarmIntent.setAction(intent.getAction()).setData(intent.getData());

		PendingIntent alarmPendIntent = PendingIntent.getBroadcast(getApplicationContext(), 
				notifID, alarmIntent, PendingIntent.FLAG_UPDATE_CURRENT);
		// set alarm for ten minutes (five to test)
		alarmMgr.set(AlarmManager.ELAPSED_REALTIME_WAKEUP,
				SystemClock.elapsedRealtime() + snoozeMinutes * 60 * 1000, alarmPendIntent);
	}
}
