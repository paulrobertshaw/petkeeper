package com.paulro.petkeeper;

import java.util.ArrayList;

import android.app.Activity;
import android.app.NotificationManager;
import android.content.ContentProviderOperation;
import android.content.Context;
import android.content.OperationApplicationException;
import android.database.Cursor;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.RemoteException;
import android.support.v4.app.ListFragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.widget.CursorAdapter;
import android.support.v4.widget.SimpleCursorAdapter;
import android.support.v4.widget.SimpleCursorAdapter.ViewBinder;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.view.ActionMode;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.paulro.petkeeper.utilities.AppUtils;
import com.paulro.petkeeper.provider.PetsContract;
import com.paulro.petkeeper.provider.PetsContract.VetVisits;
import com.paulro.petkeeper.utilities.NewPetSelected;

public class VetVisitsListFragment extends ListFragment implements LoaderManager.LoaderCallbacks<Cursor>, NewPetSelected {

	public interface ButtonListener {
		public void onAddNewButtonPressed(Uri petUri);
		public void onVetVisitItemSelected(Uri vetUri, Uri petUri);
	}

	public static final String TAG = VetVisitsListFragment.class.getSimpleName();

	private static final int LOADER_ID = 67;

	protected ActionMode mActionMode;
	protected ListView mListView;

	private ActivatedCursorAdapter mAdapter;
	private ButtonListener mButtonListener;

	private Uri petUri;

	private ActionMode.Callback mActionModeCallback = new ActionMode.Callback() {

		@Override
		public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
			// called each time action mode is shown
			return false; // return false if nothing is done
		}

		@Override
		public void onDestroyActionMode(ActionMode mode) {
			// clear the highlighted rows in case the back or done buttons are pressed
			mListView.clearChoices();
			mListView.invalidateViews();
			mListView.requestLayout();
			mListView.post(new Runnable() {
				@Override
				public void run() {
					// change choice mode in a runnable to give the adapter time to redraw backgrounds.
					// when in CHOICE_MODE_NONE, the de-select methods don't run
					mListView.setChoiceMode(ListView.CHOICE_MODE_NONE);
				}
			});
			mActionMode = null;
		}

		@Override
		public boolean onCreateActionMode(ActionMode mode, Menu menu) {
			// inflate the contextual menu resource
			MenuInflater inflater = mode.getMenuInflater();
			inflater.inflate(R.menu.contextual, menu);
			return true;
		}

		@Override
		public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
			switch (item.getItemId()) {
			case R.id.action_delete:
				final long[] ids = mListView.getCheckedItemIds();

				if (ids.length > 0) {
					// do deletes in separate thread
					new Thread(new Runnable() {
						@Override
						public void run() {
							ArrayList<ContentProviderOperation> operations = new ArrayList<>();
							ArrayList<Integer> notifIds = new ArrayList<>();
							String[] projection = new String[] { VetVisits.NOTIF_ID };
							for (long id : ids) {
								Uri vetUri = Uri.withAppendedPath(VetVisits.CONTENT_URI, String.valueOf(id));
								operations.add(ContentProviderOperation.newDelete(vetUri).build());
								// assemble a list of the notification ids to remove notifications
								Cursor vet = getActivity().getContentResolver().query(vetUri, projection, null, null, null);
								if (vet.moveToFirst()) {
									notifIds.add(vet.getInt(vet.getColumnIndexOrThrow(VetVisits.NOTIF_ID)));
								}
								vet.close();
							}

							try {
								// cancel notifications, if any, for these elements
								// otherwise, the notification will only go away with a force close
								NotificationManager nManager = (NotificationManager) getActivity()
										.getSystemService(Context.NOTIFICATION_SERVICE);
								for (int notifId : notifIds) {
									nManager.cancel(notifId);
								}

								// apply all items as a batch, uses a single transaction
								getActivity().getContentResolver().applyBatch(PetsContract.AUTHORITY, operations);
							} catch (RemoteException | OperationApplicationException e) {
								Log.wtf(TAG, e);
							}
						}
					}).start();
				}

				mode.finish(); // end action mode
				return true;
			default:
				return false;
			}
		}
	};

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		if (savedInstanceState != null) {
			petUri = savedInstanceState.getParcelable(PetsContract.Pets.CONTENT_ITEM_TYPE);
		}

		registerForContextMenu(getListView());
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		try {
			mButtonListener = (ButtonListener) activity;
		}
		catch (ClassCastException e) {
			throw new ClassCastException(activity.toString() + " must implement VetVisitsListFragment.ButtonListener");
		}
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		petUri = (savedInstanceState == null) ? null : (Uri) savedInstanceState.getParcelable(PetsContract.Pets.CONTENT_ITEM_TYPE);

		Bundle extras = getActivity().getIntent().getExtras();
		if (extras != null) {
			petUri = extras.getParcelable(PetsContract.Pets.CONTENT_ITEM_TYPE);
		}
		// look up the shared prefs file if necessary
		if (petUri == null) {
			long defaultPetID = AppUtils.getLastViewedPetID(getActivity());
			if (defaultPetID > 0) {
				petUri = Uri.withAppendedPath(PetsContract.Pets.CONTENT_URI, String.valueOf(defaultPetID));
			}
		}

		String[] from = new String[] { PetsContract.VetVisits.DATE, PetsContract.VetVisits.DOCTOR };
		int[] to = new int[] { R.id.text1, R.id.text2 };
		mAdapter = new ActivatedCursorAdapter(getActivity(), R.layout.list_item_2_lines, 
				null, from, to, CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER);

		mAdapter.setViewBinder(new DateFormatViewBinder());
		setListAdapter(mAdapter);

		setHasOptionsMenu(true);
		getLoaderManager().initLoader(LOADER_ID, null, this);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_vet_visits_list, container, false);

		mListView = (ListView) view.findViewById(android.R.id.list);

		mListView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
			@Override
			public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
				if (mActionMode != null) {
					return false;
				}

				mListView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
				mListView.setItemChecked(position, true);

				mActionMode = ((ActionBarActivity) getActivity()).startSupportActionMode(mActionModeCallback);
				return true;
			}
		});

		return view;
	}

	@Override
	public void onListItemClick(ListView l, View v, int position, long id) {
		super.onListItemClick(l, v, position, id);
		if (mActionMode == null) { // don't want to launch new activity when in action mode
			mButtonListener.onVetVisitItemSelected(
					Uri.withAppendedPath(PetsContract.VetVisits.CONTENT_URI, String.valueOf(id)), petUri);
		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch(item.getItemId()) {
		case R.id.action_new:
			mButtonListener.onAddNewButtonPressed(petUri);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putParcelable(PetsContract.Pets.CONTENT_ITEM_TYPE, petUri);
	}

	@Override
	public void onDetach() {
		super.onDetach();
		mButtonListener = null;
	}

	@Override
	public void onNewPetSelected() {
		long id = AppUtils.getLastViewedPetID(getActivity());
		petUri = Uri.withAppendedPath(PetsContract.Pets.CONTENT_URI, String.valueOf(id));
		getLoaderManager().restartLoader(LOADER_ID, null, this);
	}

	@Override
	public Loader<Cursor> onCreateLoader(int id, Bundle args) {
		String[] projection = { PetsContract.VetVisits._ID, PetsContract.VetVisits.DATE, PetsContract.VetVisits.DOCTOR };
		return new CursorLoader(getActivity(), PetsContract.VetVisits.CONTENT_URI, projection, 
				PetsContract.VetVisits.SELECTION_BY_FOREIGN_KEY, new String[] { petUri.getLastPathSegment() }, null);
	}

	@Override
	public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
		mAdapter.swapCursor(data);
	}

	@Override
	public void onLoaderReset(Loader<Cursor> loader) {
		mAdapter.swapCursor(null);
	}

	private class ActivatedCursorAdapter extends SimpleCursorAdapter {

		public ActivatedCursorAdapter(Context context, int layout, Cursor c,
				String[] from, int[] to, int flags) {
			super(context, layout, c, from, to, flags);
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			// set background for activated item in gingerbread
			if (Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB) {
				View view = super.getView(position, convertView, parent);

				if (mListView.isItemChecked(position)) {
					// a close approximation of the holo theme highlight color
					view.setBackgroundColor(Color.argb(192, 51, 173, 214));
				}
				else {
					view.setBackgroundColor(Color.TRANSPARENT);
				}
				return view;
			}
			else { // regular behaviour for API 11+
				return super.getView(position, convertView, parent);
			}
		}
	}

	private class DateFormatViewBinder implements ViewBinder {

		@Override
		public boolean setViewValue(View view, Cursor cursor, int columnIndex) {
			int dateIndex = cursor.getColumnIndexOrThrow(PetsContract.VetVisits.DATE);
			if (columnIndex == dateIndex) {
				long time = cursor.getLong(dateIndex);
				if (time > 0) {
					TextView tv = (TextView) view;
					tv.setText(AppUtils.formatDate(time, getActivity()));
				}
				else {
					view.setVisibility(View.INVISIBLE); // should never come up because date is required
														// but don't want to show 1970-01-01 just in case
				}
				// return true either way
				return true;
			}
			
			// not the right column, return false to use default viewbinder
			return false;
		}
	}
}
