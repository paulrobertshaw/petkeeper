package com.paulro.petkeeper;

import android.content.Context;
import android.content.Intent;
import android.os.SystemClock;
import android.support.v4.content.WakefulBroadcastReceiver;
import android.util.Log;

public class AlarmReceiver extends WakefulBroadcastReceiver {

	private static final String TAG = AlarmReceiver.class.getSimpleName();

	@Override
	public void onReceive(Context context, Intent intent) { // onReceive() must complete within 10 seconds
		// wake lock to prevent device from sleeping before the service completes
		Intent serviceIntent = new Intent(context, NotifAndNextAlarmService.class);
		
		serviceIntent.setAction(intent.getAction()).setData(intent.getData()); // pass the URI and action string to the service
		
		Log.i(TAG, "Starting service @ " + SystemClock.elapsedRealtime());
		startWakefulService(context, serviceIntent);
	}
	
	
}
