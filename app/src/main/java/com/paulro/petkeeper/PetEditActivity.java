package com.paulro.petkeeper;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;

public class PetEditActivity extends ActionBarActivity {
// using an activity for now
// maybe I'll load in the edit pet fragment dynamically later

	private PetEditFragment mEditPetFragment;
//	private MenuItem mSaveButton;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_pet_edit);

		if (savedInstanceState == null) {
			mEditPetFragment = new PetEditFragment();
			getSupportFragmentManager().beginTransaction()
			.add(R.id.container, mEditPetFragment, PetEditFragment.TAG).commit();
		}
		else {
			// check this for correctness
			// restore fragment instance
			mEditPetFragment = (PetEditFragment) getSupportFragmentManager().getFragment(savedInstanceState, PetEditFragment.TAG);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
//		getMenuInflater().inflate(R.menu.edit_pet, menu);
//		
//		mSaveButton = menu.findItem(R.id.action_save);
//		mSaveButton.setVisible(true);

		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.

		// moved to fragment, where it makes more sense
//		switch(item.getItemId()) {
//		case R.id.action_save:
//			saveButtonPressed();
//			return true;
//		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		// save the fragment instance
		// maybe this is only for the back stack?
		getSupportFragmentManager().putFragment(outState, PetEditFragment.TAG, mEditPetFragment);
	}
}
