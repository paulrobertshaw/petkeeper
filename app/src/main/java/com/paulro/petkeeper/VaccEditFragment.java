package com.paulro.petkeeper;

import java.util.Calendar;

import com.paulro.petkeeper.utilities.AppUtils;
import com.paulro.petkeeper.provider.PetsContract;
import com.paulro.petkeeper.provider.PetsContract.Vaccinations;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.app.ActionBarActivity;
import android.text.TextUtils;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TimePicker;
import android.widget.Toast;

public class VaccEditFragment extends Fragment implements LoaderManager.LoaderCallbacks<Cursor> {

	private static final int LOADER_ID = 45;
	
	private EditText mName;
	
	private EditText mDate;
	private long mDateUnixTime = 0;
	private boolean mDateHasFocus = false;
	private EditText mExpirationDate;
	private long mExpirationDateUnixTime = 0;
	private boolean mExpirationDateHasFocus = false;
	
	private EditText mVaccNumber;
	private EditText mDoctor;
	private CheckBox mSetNotification;
	private boolean mUserClearNotification = false;
	
	private Uri petUri;
	private Uri vacUri;
	
	private int mDatePickerBugfix = 0;
	
	private DatePickerDialog.OnDateSetListener mDatePickerListner = new DatePickerDialog.OnDateSetListener() {

		@Override
		public void onDateSet(DatePicker view, int year, int monthOfYear,
				int dayOfMonth) {
			if (mDatePickerBugfix > 0) { // annoyingly, onDateSet is called twice in JellyBean and higher
				return;
			}
			mDatePickerBugfix++;

			if (mDateHasFocus) {
				if (mDateUnixTime > 0) {
					// preserves the user's input time
					mDateUnixTime = AppUtils.getUnixTime(view, mDateUnixTime);
				}
				else {
					// no time has already been entered, start with current time
					mDateUnixTime = AppUtils.getUnixTime(view);
				}
				showTimePickerDialog(mDateUnixTime);
			}
			else if (mExpirationDateHasFocus) {
				// set the alarm for the same time of day that this fragment is run
				mExpirationDateUnixTime = AppUtils.getUnixTime(view, Calendar.getInstance().getTimeInMillis());
				mExpirationDate.setText(AppUtils.formatDate(mExpirationDateUnixTime, getActivity()));
				// if user has not already unchecked the box, automatically set a notification if expiration date provided
				if (!mUserClearNotification) {
					mSetNotification.setChecked(true);
				}
			}
		}
	};
	
	private TimePickerDialog.OnTimeSetListener mTimePickerListener = new TimePickerDialog.OnTimeSetListener() {
		
		@Override
		public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
			if (mDateHasFocus) {
				mDateUnixTime = AppUtils.getUnixTimeOfDay(view, mDateUnixTime);
				mDate.setText(AppUtils.formatDateTime(mDateUnixTime, getActivity()));
			}
			// no need to have a time for the expiration, I think
//			else if (mExpirationDateHasFocus) {
//				mExpirationDateUnixTime = AppUtils.getUnixTimeOfDay(view, mExpirationDateUnixTime);
//				mExpirationDate.setText(AppUtils.formatDateTime(mExpirationDateUnixTime, getActivity()));
//			}
		}
	};
	
	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		petUri = (savedInstanceState == null) ? null : (Uri) savedInstanceState.getParcelable(PetsContract.Pets.CONTENT_ITEM_TYPE);
		vacUri = (savedInstanceState == null) ? null : 
			(Uri) savedInstanceState.getParcelable(PetsContract.Vaccinations.CONTENT_ITEM_TYPE);

		mDateUnixTime = (savedInstanceState == null) ? 0 : savedInstanceState.getLong(Vaccinations.DATE);
		mExpirationDateUnixTime = (savedInstanceState == null) ? 0 : savedInstanceState.getLong(Vaccinations.EXPIRATION);

		Bundle extras = getActivity().getIntent().getExtras();
		if (extras != null) {
			petUri = extras.getParcelable(PetsContract.Pets.CONTENT_ITEM_TYPE);
			vacUri = extras.getParcelable(PetsContract.Vaccinations.CONTENT_ITEM_TYPE);
		}
		
		if (vacUri != null) {
			if (getLoaderManager().getLoader(LOADER_ID) == null) {
				// if a loader exists already, the screen was likely rotated
				// starting it again will cause any previous inputs to be lost
				getLoaderManager().initLoader(LOADER_ID, null, this);
			}
		}
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_vacc_edit, container, false);
		mName = (EditText) view.findViewById(R.id.field_name);
		mDate = (EditText) view.findViewById(R.id.field_date);
		mExpirationDate = (EditText) view.findViewById(R.id.field_expiration_date);
		mVaccNumber = (EditText) view.findViewById(R.id.field_vacc_number);
		mDoctor = (EditText) view.findViewById(R.id.field_doctor);
		mSetNotification = (CheckBox) view.findViewById(R.id.check_set_notification);
		mSetNotification.setChecked(false); // initialize to false
		// if user has not already unchecked the box, automatically set a notification if expiration date provided
		mSetNotification.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				mUserClearNotification = true;
			}
		});
		
		if (vacUri == null) { // Uri is not passed in, therefore is for a new record
			// title should be "add", not "edit
			if (getActivity() instanceof ActionBarActivity) {
				((ActionBarActivity) getActivity()).getSupportActionBar()
					.setTitle(getString(R.string.title_activity_vacc_edit_add));
			}
		}
		mDate.setOnFocusChangeListener(new View.OnFocusChangeListener() {
			
			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				if (hasFocus) {
					mDatePickerBugfix = 0;
					mDateHasFocus = true;
					showDatePickerDialog(mDateUnixTime);
				}
				else {
					mDateHasFocus = false;
				}
			}
		});
		mExpirationDate.setOnFocusChangeListener(new View.OnFocusChangeListener() {
			
			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				if (hasFocus) {
					mDatePickerBugfix = 0;
					mExpirationDateHasFocus = true;
					showDatePickerDialog(mExpirationDateUnixTime);
				}
				else {
					mExpirationDateHasFocus = false;
				}
			}
		});
		
		setHasOptionsMenu(true);
		return view;
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		
		outState.putParcelable(PetsContract.Pets.CONTENT_ITEM_TYPE, petUri);
		outState.putParcelable(PetsContract.Vaccinations.CONTENT_ITEM_TYPE, vacUri);
		
		outState.putString(Vaccinations.NAME, mName.getText().toString());
		outState.putLong(Vaccinations.DATE, mDateUnixTime);
		outState.putLong(Vaccinations.EXPIRATION, mExpirationDateUnixTime);
		outState.putString(Vaccinations.NUMBER, mVaccNumber.getText().toString());
		outState.putString(Vaccinations.DOCTOR, mDoctor.getText().toString());
		outState.putBoolean(Vaccinations.NOTIF_IS_SET, mSetNotification.isChecked());
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.action_save:
			if (saveData()) {
				getActivity().setResult(Activity.RESULT_OK);
				getActivity().finish();
			}
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onDetach() {
		super.onDetach();
		mDatePickerListner = null;
		mTimePickerListener = null;
	}
	
	protected void showDatePickerDialog(long startTime) {
		int[] startDate = (startTime > 0) ? AppUtils.getFriendlyDate(startTime) : 
			AppUtils.getFriendlyDate(Calendar.getInstance().getTimeInMillis());
		DatePickerDialog dpd = new DatePickerDialog(getActivity(), mDatePickerListner, startDate[0], startDate[1], startDate[2]);
		dpd.show();
	}
	
	protected void showTimePickerDialog(long startTime) {
		int[] initialTime = (startTime > 0) ? AppUtils.getFriendlyTime(startTime): 
			AppUtils.getFriendlyTime(Calendar.getInstance().getTimeInMillis());
		TimePickerDialog tpd = new TimePickerDialog(getActivity(), mTimePickerListener, 
				initialTime[0], initialTime[1], DateFormat.is24HourFormat(getActivity()));
		tpd.show();
	}

	private boolean saveData() {
		String name = mName.getText().toString();
		boolean setNotification = mSetNotification.isChecked();
		if (TextUtils.isEmpty(name)) {
			AppUtils.makeToast(getActivity(), "Name can not be empty", Toast.LENGTH_LONG);
			return false;
		}
		if (mDateUnixTime == 0) {
			AppUtils.makeToast(getActivity(), "Please enter date of vaccination", Toast.LENGTH_LONG);
			return false;
		}
		if (mExpirationDateUnixTime > 0 && mDateUnixTime > mExpirationDateUnixTime) {
			AppUtils.makeToast(getActivity(), "Expiration date is before vaccination date", Toast.LENGTH_LONG);
			return false;
		}
		if (mExpirationDateUnixTime == 0 && setNotification) {
			AppUtils.makeToast(getActivity(), "Cannot set notification without expiration date!", Toast.LENGTH_LONG);
			return false;
		}
		
		ContentValues values = new ContentValues();
		values.put(Vaccinations.NAME, name);
		values.put(Vaccinations.DATE, mDateUnixTime);
		values.put(Vaccinations.EXPIRATION, mExpirationDateUnixTime);
		values.put(Vaccinations.NUMBER, mVaccNumber.getText().toString());
		values.put(Vaccinations.DOCTOR, mDoctor.getText().toString());
		values.put(Vaccinations.NOTIF_IS_SET, setNotification);
		values.put(Vaccinations.PET_ID, petUri.getLastPathSegment()); // foreign key
		
		if (vacUri == null) {
			vacUri = getActivity().getContentResolver().insert(PetsContract.Vaccinations.CONTENT_URI, values);
		}
		else {
			getActivity().getContentResolver().update(vacUri, values, null, null);
		}
		
		if (setNotification) {
			Intent serviceIntent = new Intent(getActivity(), AlarmSetService.class);
			serviceIntent.setData(vacUri);
			
			getActivity().startService(serviceIntent);
		}
		
		return true;
	}
	
	@Override
	public Loader<Cursor> onCreateLoader(int id, Bundle args) {
		return new CursorLoader(getActivity(), vacUri, PetsContract.Vaccinations.PROJECTION_ALL, null, null, null);
	}
	
	@Override
	public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
		if (data != null && data.getCount() > 0) {
			data.moveToFirst();
			
			mName.setText(data.getString(data.getColumnIndexOrThrow(Vaccinations.NAME)));
			mDateUnixTime = data.getLong(data.getColumnIndexOrThrow(Vaccinations.DATE));
			mExpirationDateUnixTime = data.getLong(data.getColumnIndexOrThrow(Vaccinations.EXPIRATION));
			mVaccNumber.setText(data.getString(data.getColumnIndexOrThrow(Vaccinations.NUMBER)));
			mDoctor.setText(data.getString(data.getColumnIndexOrThrow(Vaccinations.DOCTOR)));
			mSetNotification.setChecked(data.getInt(data.getColumnIndexOrThrow(Vaccinations.NOTIF_IS_SET)) != 0);
			
			if (mDateUnixTime > 0) {
				mDate.setText(AppUtils.formatDateTime(mDateUnixTime, getActivity()));
			}
			if (mExpirationDateUnixTime > 0) {
				mExpirationDate.setText(AppUtils.formatDate(mExpirationDateUnixTime, getActivity()));
			}
		}
	}
	
	@Override
	public void onLoaderReset(Loader<Cursor> loader) {
	}
}
