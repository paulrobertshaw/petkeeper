package com.paulro.petkeeper.database;

import com.paulro.petkeeper.provider.PetsContract.NotificationFrequency;

public class TblNotifFrequency {

	public static final String TBL_NAME = "notif_freq";
	
	public static final String COL_ID = NotificationFrequency._ID;
	public static final String COL_DESCRIPTION = NotificationFrequency.DESCRIPTION;
	public static final String COL_TIME_VALUE = NotificationFrequency.TIME_VALUE;
	public static final String COL_TIME_UNIT = NotificationFrequency.TIME_UNIT;
	
	protected static final String DDL_CREATE_TABLE_NOTIFICATIONS = 
			"CREATE TABLE " + TBL_NAME + " (" +
					COL_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, \n" +
					COL_DESCRIPTION + " TEXT NOT NULL, \n" +
					COL_TIME_VALUE + " INTEGER NOT NULL, \n" +
					COL_TIME_UNIT + " TEXT NOT NULL \n" +
					")";
}
