package com.paulro.petkeeper.database;

import com.paulro.petkeeper.provider.PetsContract.Pets;

public class TblPets {

	public static final String TBL_NAME = "pets";

	// columns of pets table
	public static final String COL_ID = Pets._ID;
	public static final String COL_NAME = Pets.NAME;
	public static final String COL_HEIGHT = Pets.HEIGHT;
	public static final String COL_WEIGHT = Pets.WEIGHT;
	public static final String COL_DOB = Pets.DOB;
	public static final String COL_BREED = Pets.BREED;
	public static final String COL_MICROCHIP = Pets.MICROCHIP;
	public static final String COL_GENDER = Pets.GENDER;
	public static final String COL_PHOTO = Pets.PHOTO;

	protected static final String DDL_CREATE_TBL_PETS = 
			"CREATE TABLE " + TBL_NAME + " (" +
					COL_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, \n" +
					COL_NAME + " TEXT NOT NULL, \n" +
					COL_HEIGHT + " INTEGER, \n" +
					COL_WEIGHT + " FLOAT, \n" +
					COL_DOB + " DATETIME, \n" +
					COL_BREED + " TEXT, \n" +
					COL_MICROCHIP + " TEXT, \n" +
					COL_GENDER + " TEXT, \n" +
					COL_PHOTO + " BLOB \n" +
					")";
}
