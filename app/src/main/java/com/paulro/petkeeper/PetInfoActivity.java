package com.paulro.petkeeper;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.TaskStackBuilder;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.paulro.petkeeper.utilities.AppUtils;
import com.paulro.petkeeper.utilities.ImageSaverFragment;
import com.paulro.petkeeper.provider.PetsContract;
import com.paulro.petkeeper.utilities.ConfirmationFragment;
import com.paulro.petkeeper.utilities.NewPetSelected;

public class PetInfoActivity extends ActionBarActivity implements PetInfoFragment.PetInfoButtonListeners, 
NavigationDrawerFragment.NavigationDrawerCallbacks {

	public static final String TAG = PetInfoActivity.class.getSimpleName();

	private static final String DEFAULT_FRAGMENT_TAG = PetInfoFragment.TAG;
	private static final int NEW_PET_CODE = 39;
	private static final int SELECT_PICTURE = 101;
	private static final int OPEN_DOCUMENT = 110;

	private NavigationDrawerFragment mNavigationDrawerFragment;
	private CharSequence mTitle;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_pet_info);

		if (savedInstanceState == null) {
			getSupportFragmentManager().beginTransaction()
					.add(R.id.container, new PetInfoFragment(), DEFAULT_FRAGMENT_TAG).commit();
		}

		mNavigationDrawerFragment = (NavigationDrawerFragment) getSupportFragmentManager()
				.findFragmentById(R.id.navigation_drawer);
		mTitle = getTitle();
		mNavigationDrawerFragment.setUp(R.id.navigation_drawer,
				(DrawerLayout) findViewById(R.id.drawer_layout));
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		
		// items will be handled by PetInfoFragment
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onEditButtonPressed(Uri uri) {
		Intent intent = new Intent(this, PetEditActivity.class);
		intent.putExtra(PetsContract.Pets.CONTENT_ITEM_TYPE, uri);
		startActivity(intent); // update new information in display?
	}

	@SuppressLint("InlinedApi")
	@Override
	public void onFindPicureButtonPressed() {
		if (Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB) {
			// TODO pick from external storage only?
			Intent imgIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
//			imgIntent.setType("image/*").setAction(Intent.ACTION_GET_CONTENT);
			startActivityForResult(Intent.createChooser(imgIntent, "Select picture"), SELECT_PICTURE);
		}
		else if (Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT) {
			Intent imgIntent = new Intent();
			imgIntent.setType("image/*").setAction(Intent.ACTION_GET_CONTENT);
			imgIntent.putExtra(Intent.EXTRA_LOCAL_ONLY, true); // only allow local files, no network
			startActivityForResult(Intent.createChooser(imgIntent, "Select picture"), SELECT_PICTURE);
		}
		else {
			Intent imgIntent = new Intent(Intent.ACTION_GET_CONTENT); // has more sources than ACTION_OPEN_DOCUMENT
			imgIntent.addCategory(Intent.CATEGORY_OPENABLE);
			imgIntent.setType("image/*");
			startActivityForResult(imgIntent, OPEN_DOCUMENT);
		}
	}

	@Override
	public void onNavigationDrawerItemSelected(int position, long id) {
		// cross-navigate without adding to back stack
		if (id < 0) { // petId's will be positive
			if (id == NavigationDrawerFragment.ID_SETTINGS) {
				Intent settingsIntent = new Intent(this, SettingsActivity.class);
				startActivity(settingsIntent);
			}
			else if (id == NavigationDrawerFragment.ID_MEDICATIONS) {
				Intent intent = new Intent(this, MedsListActivity.class);
				TaskStackBuilder.create(this)
					// Add all of this activity's parents to the back stack
					.addNextIntentWithParentStack(intent)
					// Navigate up to the closest parent
					.startActivities();
			}
			else if (id == NavigationDrawerFragment.ID_VACCINATIONS) {
				Intent intent = new Intent(this, VaccListActivity.class);
				TaskStackBuilder.create(this)
					// Add all of this activity's parents to the back stack
					.addNextIntentWithParentStack(intent)
					// Navigate up to the closest parent
					.startActivities();
			}
			else if (id == NavigationDrawerFragment.ID_VET_VISITS) {
				Intent intent = new Intent(this, VetVisitsListActivity.class);
				TaskStackBuilder.create(this)
					// Add all of this activity's parents to the back stack
					.addNextIntentWithParentStack(intent)
					// Navigate up to the closest parent
					.startActivities();
			}
			else if (id == NavigationDrawerFragment.ID_ADD_PET) {
				Intent intent = new Intent(this, PetEditActivity.class);
				// the lack of intent extras tells it it's a new entry
				startActivityForResult(intent, NEW_PET_CODE);
			}
			else if (id == NavigationDrawerFragment.ID_DELETE_PET) {
				// pop up a confirmation dialog
				new ConfirmationFragment().show(getSupportFragmentManager(), ConfirmationFragment.TAG);
			}
		}
		else {
			// set new petId, then tell fragment to reload info
			AppUtils.setLastViewedPetID(this, id);
			syncFragmentWithNewPet();
		}
	}

	@Override
	public void onDeletePetSelected(boolean confirmed) {
		if (confirmed) {
			long currentId = AppUtils.getLastViewedPetID(this);
			AppUtils.setLastViewedPetID(this, 0L); // set it to 0 to make the loader choose the first one in the list
			getContentResolver().delete(Uri.withAppendedPath(PetsContract.Pets.CONTENT_URI, String.valueOf(currentId)), null, null);
			AppUtils.deleteImageFile(this, String.valueOf(currentId));
			// navigate up to main activity upon delete
			Intent intent = new Intent(this, MainActivity.class);
			TaskStackBuilder.create(this)
				.addNextIntentWithParentStack(intent) // Add all of this activity's parents to the back stack
				.startActivities(); // Navigate up to the closest parent
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		if (!mNavigationDrawerFragment.isDrawerOpen()) {
			// Only show items in the action bar relevant to this screen
			// if the drawer is not showing. Otherwise, let the drawer
			// decide what to show in the action bar.
			getMenuInflater().inflate(R.menu.pet_info, menu);
			restoreActionBar();
			return true;
		}
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (resultCode == Activity.RESULT_OK) {
			if (requestCode == SELECT_PICTURE || requestCode == OPEN_DOCUMENT) {
				Uri imgUri = data.getData();
				if (imgUri == null) {
					Log.e(TAG, "null URI");
					AppUtils.makeToast(this, "Picture could not be loaded!", Toast.LENGTH_LONG);
					return;
				}
				
				if (requestCode == SELECT_PICTURE) {
					ImageSaverFragment fragment = (ImageSaverFragment) getSupportFragmentManager().
							findFragmentByTag(DEFAULT_FRAGMENT_TAG);
					if (fragment != null) {
						fragment.newImageSelected(imgUri);
					}
				}
				else { //if (requestCode == OPEN_DOCUMENT)
					ImageSaverFragment fragment = (ImageSaverFragment) getSupportFragmentManager().
							findFragmentByTag(DEFAULT_FRAGMENT_TAG);
					if (fragment != null) {
						fragment.newImageSelectedKitKat(imgUri);
					}
				}
			}
			else if (requestCode == NEW_PET_CODE) {
				syncFragmentWithNewPet();
			}
		}
	}

	private void syncFragmentWithNewPet() {
		Fragment fragment = getSupportFragmentManager().findFragmentByTag(DEFAULT_FRAGMENT_TAG);
		if (fragment instanceof NewPetSelected) {
			((NewPetSelected) fragment).onNewPetSelected();
		}
	}

	private void restoreActionBar() {
		ActionBar actionBar = getSupportActionBar();
		actionBar.setDisplayShowTitleEnabled(true);
		actionBar.setTitle(mTitle);
	}
}
