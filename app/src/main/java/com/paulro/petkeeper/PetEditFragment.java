package com.paulro.petkeeper;

import java.util.Calendar;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.ContentValues;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.app.ActionBarActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.paulro.petkeeper.utilities.AppUtils;
import com.paulro.petkeeper.database.TblPets;
import com.paulro.petkeeper.provider.PetsContract;
import com.paulro.petkeeper.provider.PetsContract.Pets;

public class PetEditFragment extends Fragment implements LoaderManager.LoaderCallbacks<Cursor> {

	public static final String TAG = PetEditFragment.class.getName();
	private static final int LOADER_ID = 201;

	private EditText mName;
	private EditText mHeight;
	private EditText mWeight;
	private EditText mDateOfBirth;
	private EditText mBreed;
	private EditText mMicrochip;
	private Spinner mGender;
	private Uri petUri;

	private long dobUnixTime = 0;

	private DatePickerDialog.OnDateSetListener mDatePickerListner = new DatePickerDialog.OnDateSetListener() {

		@Override
		public void onDateSet(DatePicker view, int year, int monthOfYear,
				int dayOfMonth) {
			dobUnixTime = AppUtils.getUnixTime(view);
			mDateOfBirth.setText(AppUtils.formatDate(dobUnixTime, getActivity()));
		}
	};

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		if (savedInstanceState != null) {
			petUri = savedInstanceState.getParcelable(PetsContract.Pets.CONTENT_ITEM_TYPE);

			if (petUri == null) { // if not, data can be retrieved from fillData()
				mName.setText(savedInstanceState.getString(TblPets.COL_NAME));
				mHeight.setText(savedInstanceState.getString(TblPets.COL_HEIGHT));
				mWeight.setText(savedInstanceState.getString(TblPets.COL_WEIGHT));
				// set DOB
				dobUnixTime = savedInstanceState.getLong(TblPets.COL_DOB);
				if (dobUnixTime > 0) {
					mDateOfBirth.setText(AppUtils.formatDate(dobUnixTime, getActivity()));
				}

				mBreed.setText(savedInstanceState.getString(TblPets.COL_BREED));
				mMicrochip.setText(savedInstanceState.getString(TblPets.COL_MICROCHIP));

				String gender = savedInstanceState.getString(TblPets.COL_GENDER);
				for (int i = 0; i < mGender.getCount(); i++) {
					if (((String) mGender.getItemAtPosition(i))
							.equalsIgnoreCase(gender)) {
						mGender.setSelection(i);
						break;
					}
				}
			}
		}
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		petUri = (savedInstanceState == null) ? null : (Uri) savedInstanceState.getParcelable(PetsContract.Pets.CONTENT_ITEM_TYPE);

		Bundle extras = getActivity().getIntent().getExtras();
		if (extras != null) {
			petUri = extras.getParcelable(PetsContract.Pets.CONTENT_ITEM_TYPE);
		}

		// only needed when petUri exists
		if (petUri != null) {
			getLoaderManager().initLoader(LOADER_ID, null, this);
		}
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		// TODO check if I need to call super() 
		//		super.onCreateOptionsMenu(menu, inflater);
		inflater.inflate(R.menu.pet_edit, menu);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
		int unitSystem = Integer.parseInt(sharedPrefs.getString(getString(R.string.key_choose_units), "0"));
		View view = inflater.inflate(R.layout.fragment_pet_edit, container, false);

		mName = (EditText) view.findViewById(R.id.field_name);
		mHeight = (EditText) view.findViewById(R.id.field_height);
		mWeight = (EditText) view.findViewById(R.id.field_weight);
		mDateOfBirth = (EditText) view.findViewById(R.id.field_dob);
		mBreed = (EditText) view.findViewById(R.id.field_breed);
		mMicrochip = (EditText) view.findViewById(R.id.field_microchip);
		mGender = (Spinner) view.findViewById(R.id.spinner_pet_gender);
		if (unitSystem != 0) {
			mHeight.setHint(R.string.field_height_hint_en);
			mWeight.setHint(R.string.field_weight_hint_en);
		}

		if (petUri == null) { // Uri is not passed in, therefore is for a new record
			// title should be "add", not "edit
			if (getActivity() instanceof ActionBarActivity) {
				((ActionBarActivity) getActivity()).getSupportActionBar().setTitle(getString(R.string.title_activity_edit_pet_add));
			}
		}
		mDateOfBirth.setOnFocusChangeListener(new View.OnFocusChangeListener() {

			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				if (hasFocus) {
					// show datepicker
					showDatePickerDialog();
				}
			}
		});

		setHasOptionsMenu(true);
		return view;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.action_save:
			if (saveData()) { // save was successful
				getActivity().setResult(Activity.RESULT_OK);
				getActivity().finish();
			}
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	@Override
	public void onSaveInstanceState(Bundle outState) { 
		super.onSaveInstanceState(outState);

		outState.putParcelable(PetsContract.Pets.CONTENT_ITEM_TYPE, petUri);
		outState.putString(TblPets.COL_NAME, mName.getText().toString());
		outState.putString(TblPets.COL_HEIGHT, mHeight.getText().toString());
		outState.putString(TblPets.COL_WEIGHT, mWeight.getText().toString());
		outState.putLong(TblPets.COL_DOB, dobUnixTime);
		outState.putString(TblPets.COL_BREED, mBreed.getText().toString());
		outState.putString(TblPets.COL_MICROCHIP, mMicrochip.getText().toString());
		outState.putString(TblPets.COL_GENDER, (String) mGender.getSelectedItem());
	}

	@Override
	public void onDetach() {
		super.onDetach();
		mDatePickerListner = null;
	}

	protected void showDatePickerDialog() {
		int[] startDate = (dobUnixTime > 0) ? AppUtils.getFriendlyDate(dobUnixTime) : 
			AppUtils.getFriendlyDate(Calendar.getInstance().getTimeInMillis());
		DatePickerDialog dpd = new DatePickerDialog(getActivity(), mDatePickerListner, startDate[0], startDate[1], startDate[2]);
		dpd.show();
	}

	private boolean saveData() { 
		// TODO error-checking
		String name = mName.getText().toString();
		if (TextUtils.isEmpty(name)) {
			AppUtils.makeToast(getActivity(), "Name can not be empty", Toast.LENGTH_LONG);
			return false;
		}

		long start = Calendar.getInstance().getTimeInMillis();

		String height = mHeight.getText().toString();
		String weight = mWeight.getText().toString();
		Long dateOfBirth = (dobUnixTime == 0) ? null : dobUnixTime;
		String breed = mBreed.getText().toString();
		String microchip = mMicrochip.getText().toString();
		String gender = (String) mGender.getSelectedItem();

		ContentValues values = new ContentValues();
		values.put(TblPets.COL_NAME, name);
		values.put(TblPets.COL_HEIGHT, height);
		values.put(TblPets.COL_WEIGHT, weight);
		values.put(TblPets.COL_DOB, dateOfBirth);
		values.put(TblPets.COL_BREED, breed);
		values.put(TblPets.COL_MICROCHIP, microchip);
		values.put(TblPets.COL_GENDER, gender);

		// insert into database
		if (petUri == null) {
			// if I assign it here, is there a potential for bugs?
			petUri = getActivity().getContentResolver().insert(PetsContract.Pets.CONTENT_URI, values);

			// put record into shared preferences to be loaded as the default pet to display
			AppUtils.setLastViewedPetID(getActivity(), Long.parseLong(petUri.getLastPathSegment()));
		}
		else {
			getActivity().getContentResolver().update(petUri, values, null, null);
		}
		Log.v(TAG, "saveData() took " + String.valueOf(Calendar.getInstance().getTimeInMillis() - start) + "ms");

		// return true for now
		return true;
	}

//	@Deprecated
//	private void fillData(Uri uri) { // what about null values?
//
//		Cursor cursor = getActivity().getContentResolver().query(uri, PetsContract.Pets.PROJECTION_ALL, null, null, null);
//		if (cursor != null) {
//			cursor.moveToFirst();
//
//			mName.setText(cursor.getString(cursor.getColumnIndexOrThrow(TblPets.COL_NAME)));
//			mHeight.setText(cursor.getString(cursor.getColumnIndexOrThrow(TblPets.COL_HEIGHT)));
//			mWeight.setText(cursor.getString(cursor.getColumnIndexOrThrow(TblPets.COL_WEIGHT)));
//			// set DOB
//			dobUnixTime = cursor.getLong(cursor.getColumnIndexOrThrow(TblPets.COL_DOB));
//			if (dobUnixTime > 0) {
//				mDateOfBirth.setText(AppUtils.formatDate(new Date(dobUnixTime), getActivity()));
//			}
//
//			mBreed.setText(cursor.getString(cursor.getColumnIndexOrThrow(TblPets.COL_BREED)));
//			mMicrochip.setText(cursor.getString(cursor.getColumnIndexOrThrow(TblPets.COL_MICROCHIP)));
//
//			String gender = cursor.getString(cursor.getColumnIndexOrThrow(TblPets.COL_GENDER));
//			for (int i = 0; i < mGender.getCount(); i++) {
//				if (((String) mGender.getItemAtPosition(i)).equalsIgnoreCase(gender)) {
//					mGender.setSelection(i);
//					break;
//				}
//			}
//			// close cursor!
//			cursor.close();
//		}
//	}

	@Override
	public Loader<Cursor> onCreateLoader(int id, Bundle args) {
		return new CursorLoader(getActivity(), petUri, Pets.PROJECTION_ALL, null, null, null);
	}

	@Override
	public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
		if (data != null && data.getCount() > 0) {
			data.moveToFirst();

			mName.setText(data.getString(data.getColumnIndexOrThrow(TblPets.COL_NAME)));
			mHeight.setText(data.getString(data.getColumnIndexOrThrow(TblPets.COL_HEIGHT)));
			mWeight.setText(data.getString(data.getColumnIndexOrThrow(TblPets.COL_WEIGHT)));
			// set DOB
			dobUnixTime = data.getLong(data.getColumnIndexOrThrow(TblPets.COL_DOB));
			if (dobUnixTime > 0) {
				mDateOfBirth.setText(AppUtils.formatDate(dobUnixTime, getActivity()));
			}

			mBreed.setText(data.getString(data.getColumnIndexOrThrow(TblPets.COL_BREED)));
			mMicrochip.setText(data.getString(data.getColumnIndexOrThrow(TblPets.COL_MICROCHIP)));

			String gender = data.getString(data.getColumnIndexOrThrow(TblPets.COL_GENDER));
			for (int i = 0; i < mGender.getCount(); i++) {
				if (((String) mGender.getItemAtPosition(i)).equalsIgnoreCase(gender)) {
					mGender.setSelection(i);
					break;
				}
			}
		}
	}

	@Override
	public void onLoaderReset(Loader<Cursor> loader) {
	}
}
