package com.paulro.petkeeper;

import java.util.Calendar;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.app.ActionBarActivity;
import android.text.TextUtils;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TimePicker;
import android.widget.Toast;

import com.paulro.petkeeper.utilities.AppUtils;
import com.paulro.petkeeper.provider.PetsContract;
import com.paulro.petkeeper.provider.PetsContract.Meds;

public class MedEditFragment extends Fragment implements LoaderManager.LoaderCallbacks<Cursor> {

	private static final int LOADER_ID = 5;
	
	private EditText mName;
	private Spinner mFrequency;
	
	private EditText mDateBegun;
	private long mDateBegunUnixTime = 0;
	private boolean mDateBegunHasFocus = false;
	private EditText mDateMostRecent;
	private long mDateMostRecentUnixTime = 0;
	private boolean mDateMostRecentHasFocus = false;
	private EditText mEndDate;
	private long mEndDateUnixTime = 0;
	private boolean mEndDateHasFocus = false;
	
	private EditText mDose;
	private CheckBox mSetNotification;
	private boolean mUserClearNotification = false;
	
	private Uri petUri;
	private Uri medUri;
	
	private int mDatePickerBugfix = 0;
	
	private DatePickerDialog.OnDateSetListener mDatePickerListner = new DatePickerDialog.OnDateSetListener() {

		@Override
		public void onDateSet(DatePicker view, int year, int monthOfYear,
				int dayOfMonth) {
			if (mDatePickerBugfix > 0) { // annoyingly, onDateSet is called twice in JellyBean and higher
				return;
			}
			mDatePickerBugfix++;

			if (mDateBegunHasFocus) {
				if (mDateBegunUnixTime > 0) {
					// preserves the user's input time
					mDateBegunUnixTime = AppUtils.getUnixTime(view, mDateBegunUnixTime);
				}
				else {
					// no time has already been entered, start with current time
					mDateBegunUnixTime = AppUtils.getUnixTime(view);
				}
				showTimePickerDialog(mDateBegunUnixTime);
			}
			else if (mDateMostRecentHasFocus) {
				if (mDateMostRecentUnixTime > 0) {
					mDateMostRecentUnixTime = AppUtils.getUnixTime(view, mDateMostRecentUnixTime);
				}
				else {
					mDateMostRecentUnixTime = AppUtils.getUnixTime(view);
				}
				showTimePickerDialog(mDateMostRecentUnixTime);
			}
			else if (mEndDateHasFocus) {
				if (mEndDateUnixTime > 0) {
					mEndDateUnixTime = AppUtils.getUnixTime(view, mEndDateUnixTime);
				}
				else {
					mEndDateUnixTime = AppUtils.getUnixTime(view);
				}
				showTimePickerDialog(mEndDateUnixTime);
			}
		}
	};
	private TimePickerDialog.OnTimeSetListener mTimePickerListener = new TimePickerDialog.OnTimeSetListener() {
		
		@Override
		public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
			if (mDateBegunHasFocus) {
				mDateBegunUnixTime = AppUtils.getUnixTimeOfDay(view, mDateBegunUnixTime);
				mDateBegun.setText(AppUtils.formatDateTime(mDateBegunUnixTime, getActivity()));
			}
			else if (mDateMostRecentHasFocus) {
				mDateMostRecentUnixTime = AppUtils.getUnixTimeOfDay(view, mDateMostRecentUnixTime);
				mDateMostRecent.setText(AppUtils.formatDateTime(mDateMostRecentUnixTime, getActivity()));
				// if user has not already unchecked the box, automatically set a notification if most recent date provided
				if (!mUserClearNotification) {
					mSetNotification.setChecked(true);
				}
			}
			else if (mEndDateHasFocus) {
				mEndDateUnixTime = AppUtils.getUnixTimeOfDay(view, mEndDateUnixTime);
				mEndDate.setText(AppUtils.formatDateTime(mEndDateUnixTime, getActivity()));
			}
		}
	};
	
	@Override
	public void onAttach(Activity activity) {
		// TODO Auto-generated method stub
		super.onAttach(activity);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		petUri = (savedInstanceState == null) ? null : (Uri) savedInstanceState.getParcelable(PetsContract.Pets.CONTENT_ITEM_TYPE);
		medUri = (savedInstanceState == null) ? null : (Uri) savedInstanceState.getParcelable(PetsContract.Meds.CONTENT_ITEM_TYPE);

		mDateBegunUnixTime = (savedInstanceState == null) ? 0 : savedInstanceState.getLong(Meds.DATE_BEGUN);
		mDateMostRecentUnixTime = (savedInstanceState == null) ? 0 : savedInstanceState.getLong(Meds.DATE_MOST_RECENT);
		mEndDateUnixTime = (savedInstanceState == null) ? 0 : savedInstanceState.getLong(Meds.END_DATE);

		Bundle extras = getActivity().getIntent().getExtras();
		if (extras != null) {
			petUri = extras.getParcelable(PetsContract.Pets.CONTENT_ITEM_TYPE);
			medUri = extras.getParcelable(PetsContract.Meds.CONTENT_ITEM_TYPE); // can this cause NPE?
		}
		
		if (medUri != null) { // only need loader if there is an existing record
			if (getLoaderManager().getLoader(LOADER_ID) == null) { // if loader exists, screen was probably rotated
				getLoaderManager().initLoader(LOADER_ID, null, this);
			}
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_med_edit, container, false);
		mName = (EditText) view.findViewById(R.id.field_name);
		mFrequency = (Spinner) view.findViewById(R.id.spinner_frequency);
		mDateBegun = (EditText) view.findViewById(R.id.field_date_begun);
		mDateMostRecent = (EditText) view.findViewById(R.id.field_date_most_recent);
		mEndDate = (EditText) view.findViewById(R.id.field_end_date);
		mDose = (EditText) view.findViewById(R.id.field_dose);
		mSetNotification = (CheckBox) view.findViewById(R.id.check_set_notification);
		mSetNotification.setChecked(false); // initialize to false
		// if user has not already unchecked the box, automatically set a notification if most recent date provided
		mSetNotification.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				mUserClearNotification = true;
			}
		});
		
		if (medUri == null) { // Uri is not passed in, therefore is for a new record
			// title should be "add", not "edit
			if (getActivity() instanceof ActionBarActivity) {
				((ActionBarActivity) getActivity()).getSupportActionBar().setTitle(getString(R.string.title_activity_edit_med_add));
			}
		}
		mDateBegun.setOnFocusChangeListener(new View.OnFocusChangeListener() {

			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				if (hasFocus) {
					mDatePickerBugfix = 0;
					mDateBegunHasFocus = true;
					// show datepicker
					showDatePickerDialog(mDateBegunUnixTime);
				}
				else {
					mDateBegunHasFocus = false;
				}
			}
		});
		mDateMostRecent.setOnFocusChangeListener(new View.OnFocusChangeListener() {

			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				if (hasFocus) {
					mDatePickerBugfix = 0;
					mDateMostRecentHasFocus = true;
					showDatePickerDialog(mDateMostRecentUnixTime);
				}
				else {
					mDateMostRecentHasFocus = false;
				}
			}
		});
		mEndDate.setOnFocusChangeListener(new View.OnFocusChangeListener() {

			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				if (hasFocus) {
					mDatePickerBugfix = 0;
					mEndDateHasFocus = true;
					showDatePickerDialog(mEndDateUnixTime);
				}
				else {
					mEndDateHasFocus = false;
				}
			}
		});

		setHasOptionsMenu(true);
		return view;
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		
		outState.putParcelable(PetsContract.Pets.CONTENT_ITEM_TYPE, petUri);
		outState.putParcelable(PetsContract.Meds.CONTENT_ITEM_TYPE, medUri);
		outState.putString(Meds.NAME, mName.getText().toString());
		
		outState.putString(Meds.FREQUENCY, (String) mFrequency.getSelectedItem());
		
		outState.putString(Meds.DOSE, mDose.getText().toString());
		outState.putBoolean(Meds.NOTIF_IS_SET, mSetNotification.isChecked());
		outState.putLong(Meds.DATE_BEGUN, mDateBegunUnixTime);
		outState.putLong(Meds.DATE_MOST_RECENT, mDateMostRecentUnixTime);
		outState.putLong(Meds.END_DATE, mEndDateUnixTime);
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		super.onCreateOptionsMenu(menu, inflater);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.action_save:
			if (saveData()) { // save was successful
				getActivity().setResult(Activity.RESULT_OK);
				getActivity().finish();
			}
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	@Override
	public void onDetach() {
		super.onDetach();
		mDatePickerListner = null;
		mTimePickerListener = null;
	}
	
	protected void showDatePickerDialog(long startTime) {
		int[] startDate = (startTime > 0) ? AppUtils.getFriendlyDate(startTime) : 
			AppUtils.getFriendlyDate(Calendar.getInstance().getTimeInMillis());
		DatePickerDialog dpd = new DatePickerDialog(getActivity(), mDatePickerListner, startDate[0], startDate[1], startDate[2]);
		dpd.show();
	}
	
	protected void showTimePickerDialog(long startTime) {
		int[] initialTime = (startTime > 0) ? AppUtils.getFriendlyTime(startTime): 
			AppUtils.getFriendlyTime(Calendar.getInstance().getTimeInMillis());
		TimePickerDialog tpd = new TimePickerDialog(getActivity(), mTimePickerListener, 
				initialTime[0], initialTime[1], DateFormat.is24HourFormat(getActivity()));
		tpd.show();
	}

	private boolean saveData() {
		String name = mName.getText().toString();
		boolean setNotification = mSetNotification.isChecked();
		if (TextUtils.isEmpty(name)) {
			AppUtils.makeToast(getActivity(), "Name can not be empty", Toast.LENGTH_LONG);
			return false;
		}
		if (mDateBegunUnixTime > mDateMostRecentUnixTime) {
			AppUtils.makeToast(getActivity(), "Date begun is after most recent date", Toast.LENGTH_LONG);
			return false;
		}
		if (mEndDateUnixTime > 0 && mEndDateUnixTime < mDateBegunUnixTime) {
			AppUtils.makeToast(getActivity(), "End date is before start date!", Toast.LENGTH_LONG);
			return false;
		}
		if (mDateMostRecentUnixTime == 0 && setNotification) {
			AppUtils.makeToast(getActivity(), "Cannot set notification without most recent date!", Toast.LENGTH_LONG);
			return false;
		}
		
		int frequency = mFrequency.getSelectedItemPosition();
		
		String dose = mDose.getText().toString();
		
		ContentValues values = new ContentValues();
		values.put(Meds.NAME, name);
		values.put(Meds.FREQUENCY, frequency); // just put in the index of frequency table
		values.put(Meds.DATE_BEGUN, (mDateBegunUnixTime == 0) ? null : mDateBegunUnixTime);
		values.put(Meds.DATE_MOST_RECENT, (mDateMostRecentUnixTime == 0) ? null : mDateMostRecentUnixTime);
		values.put(Meds.END_DATE, (mEndDateUnixTime == 0) ? null : mEndDateUnixTime);
		values.put(Meds.DOSE, dose);
		values.put(Meds.NOTIF_IS_SET, setNotification);
		values.put(Meds.PET_ID, petUri.getLastPathSegment());
		
		if (medUri == null) {
			medUri = getActivity().getContentResolver().insert(PetsContract.Meds.CONTENT_URI, values);
		}
		else {
			getActivity().getContentResolver().update(medUri, values, null, null);
		}
		
		if (setNotification) {
			// launch service to set alarm in background
			Intent serviceIntent = new Intent(getActivity(), AlarmSetService.class);
			serviceIntent.setData(medUri);

			getActivity().startService(serviceIntent);
		}
		
		return true;
	}

	@Override
	public Loader<Cursor> onCreateLoader(int id, Bundle args) {
		return new CursorLoader(getActivity(), medUri, PetsContract.Meds.PROJECTION_ALL, null, null, null);
	}

	@Override
	public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
		if (data != null && data.getCount() > 0) {
			data.moveToFirst();
			
			mName.setText(data.getString(data.getColumnIndexOrThrow(Meds.NAME)));
			mFrequency.setSelection(data.getInt(data.getColumnIndexOrThrow(Meds.FREQUENCY)));

			mDateBegunUnixTime = data.getLong(data.getColumnIndexOrThrow(Meds.DATE_BEGUN));
			mDateMostRecentUnixTime = data.getLong(data.getColumnIndexOrThrow(Meds.DATE_MOST_RECENT));
			mEndDateUnixTime = data.getLong(data.getColumnIndexOrThrow(Meds.END_DATE));
			mDose.setText(data.getString(data.getColumnIndexOrThrow(Meds.DOSE)));
			mSetNotification.setChecked(data.getInt(data.getColumnIndexOrThrow(Meds.NOTIF_IS_SET)) != 0); // sqlite 1==true
			
			if (mDateBegunUnixTime > 0) {
				mDateBegun.setText(AppUtils.formatDateTime(mDateBegunUnixTime, getActivity()));
			}
			if (mDateMostRecentUnixTime > 0) {
				mDateMostRecent.setText(AppUtils.formatDateTime(mDateMostRecentUnixTime, getActivity()));
			}
			if (mEndDateUnixTime > 0) {
				mEndDate.setText(AppUtils.formatDateTime(mEndDateUnixTime, getActivity()));
			}
		}
	}

	@Override
	public void onLoaderReset(Loader<Cursor> loader) {
	}
}
