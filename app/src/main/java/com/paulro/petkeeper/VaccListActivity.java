package com.paulro.petkeeper;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.TaskStackBuilder;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;

import com.paulro.petkeeper.utilities.AppUtils;
import com.paulro.petkeeper.provider.PetsContract;
import com.paulro.petkeeper.utilities.ConfirmationFragment;
import com.paulro.petkeeper.utilities.NewPetSelected;

public class VaccListActivity extends ActionBarActivity implements VaccListFragment.ButtonListener, 
NavigationDrawerFragment.NavigationDrawerCallbacks {

	private static final String DEFAULT_FRAGMENT_TAG = VaccListFragment.TAG;
	private static final int NEW_PET_CODE = 39;

	private NavigationDrawerFragment mNavigationDrawerFragment;
	private CharSequence mTitle;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_vacc_list);

		if (savedInstanceState == null) {
			getSupportFragmentManager().beginTransaction()
					.add(R.id.container, new VaccListFragment(), DEFAULT_FRAGMENT_TAG).commit();
		}

		mNavigationDrawerFragment = (NavigationDrawerFragment) getSupportFragmentManager()
				.findFragmentById(R.id.navigation_drawer);
		mTitle = getTitle();
		mNavigationDrawerFragment.setUp(R.id.navigation_drawer,
				(DrawerLayout) findViewById(R.id.drawer_layout));
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onVaccItemSelected(Uri vacUri, Uri petUri) {
		Intent intent = new Intent(this, VaccEditActivity.class);
		intent.putExtra(PetsContract.Pets.CONTENT_ITEM_TYPE, petUri);
		intent.putExtra(PetsContract.Vaccinations.CONTENT_ITEM_TYPE, vacUri);
		startActivity(intent);
	}

	@Override
	public void onAddNewButtonPressed(Uri petUri) {
		Intent intent = new Intent(this, VaccEditActivity.class);
		intent.putExtra(PetsContract.Pets.CONTENT_ITEM_TYPE, petUri);
		startActivity(intent);
	}

	@Override
	public void onNavigationDrawerItemSelected(int position, long id) {
		// cross-navigate without adding to back stack
		if (id < 0) { // petId's will be positive
			if (id == NavigationDrawerFragment.ID_SETTINGS) {
				Intent settingsIntent = new Intent(this, SettingsActivity.class);
				startActivity(settingsIntent);
			}
			else if (id == NavigationDrawerFragment.ID_MEDICATIONS) {
				Intent intent = new Intent(this, MedsListActivity.class);
				TaskStackBuilder.create(this)
					// Add all of this activity's parents to the back stack
					.addNextIntentWithParentStack(intent)
					// Navigate up to the closest parent
					.startActivities();
			}
			else if (id == NavigationDrawerFragment.ID_VACCINATIONS) {
				Intent intent = new Intent(this, VaccListActivity.class);
				TaskStackBuilder.create(this)
					// Add all of this activity's parents to the back stack
					.addNextIntentWithParentStack(intent)
					// Navigate up to the closest parent
					.startActivities();
			}
			else if (id == NavigationDrawerFragment.ID_VET_VISITS) {
				Intent intent = new Intent(this, VetVisitsListActivity.class);
				TaskStackBuilder.create(this)
					// Add all of this activity's parents to the back stack
					.addNextIntentWithParentStack(intent)
					// Navigate up to the closest parent
					.startActivities();
			}
			else if (id == NavigationDrawerFragment.ID_ADD_PET) {
				Intent intent = new Intent(this, PetEditActivity.class);
				// the lack of intent extras tells it it's a new entry
				startActivityForResult(intent, NEW_PET_CODE);
			}
			else if (id == NavigationDrawerFragment.ID_DELETE_PET) {
				// pop up a confirmation dialog
				new ConfirmationFragment().show(getSupportFragmentManager(), ConfirmationFragment.TAG);
			}
		}
		else {
			// set new petId, then tell fragment to reload info
			AppUtils.setLastViewedPetID(this, id);
			syncFragmentWithNewPet();
		}
	}

	@Override
	public void onDeletePetSelected(boolean confirmed) {
		if (confirmed) {
			long currentId = AppUtils.getLastViewedPetID(this);
			AppUtils.setLastViewedPetID(this, 0L); // set it to 0 to make the loader choose the first one in the list
			getContentResolver().delete(Uri.withAppendedPath(PetsContract.Pets.CONTENT_URI, String.valueOf(currentId)), null, null);
			AppUtils.deleteImageFile(this, String.valueOf(currentId));
			// navigate up to main activity upon delete
			Intent intent = new Intent(this, MainActivity.class);
			TaskStackBuilder.create(this)
				.addNextIntentWithParentStack(intent) // Add all of this activity's parents to the back stack
				.startActivities(); // Navigate up to the closest parent
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		if (!mNavigationDrawerFragment.isDrawerOpen()) {
			// Only show items in the action bar relevant to this screen
			// if the drawer is not showing. Otherwise, let the drawer
			// decide what to show in the action bar.
			getMenuInflater().inflate(R.menu.vacc_list, menu);
			restoreActionBar();
			return true;
		}
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == NEW_PET_CODE) {
			syncFragmentWithNewPet();
		}
	}

	private void syncFragmentWithNewPet() {
		Fragment fragment = getSupportFragmentManager().findFragmentByTag(DEFAULT_FRAGMENT_TAG);
		if (fragment instanceof NewPetSelected) {
			((NewPetSelected) fragment).onNewPetSelected();
		}
	}

	private void restoreActionBar() {
		ActionBar actionBar = getSupportActionBar();
		actionBar.setDisplayShowTitleEnabled(true);
		actionBar.setTitle(mTitle);
	}
}
