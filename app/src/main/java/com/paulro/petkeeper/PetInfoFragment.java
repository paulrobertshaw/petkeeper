package com.paulro.petkeeper;

import java.io.File;

import android.app.Activity;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.ImageView;
import android.widget.TextView;

import com.paulro.petkeeper.utilities.AppUtils;
import com.paulro.petkeeper.provider.PetsContract;
import com.paulro.petkeeper.provider.PetsContract.Pets;
import com.paulro.petkeeper.utilities.ImageSaverFragment;
import com.paulro.petkeeper.utilities.LoadBitmapTask;
import com.paulro.petkeeper.utilities.NewPetSelected;
import com.paulro.petkeeper.utilities.SaveImageFromFileTask;

public class PetInfoFragment extends Fragment implements LoaderManager.LoaderCallbacks<Cursor>, ImageSaverFragment,
		NewPetSelected {

	public interface PetInfoButtonListeners {
		public void onEditButtonPressed(Uri petUri);
		public void onFindPicureButtonPressed();
	}

	public static final String TAG = "PetInfoFragment";

	private static final int LOADER_ID = 1;

	private Uri petUri;
	private PetInfoButtonListeners mButtonListeners;

	private TextView mName;
	private TextView mHeight;
	private TextView mWeight;
	private TextView mBreed;
	private TextView mDateOfBirth;
	private TextView mMicrochip;
	private ImageView mPicture;

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		try {
			mButtonListeners = (PetInfoButtonListeners) activity;
		}
		catch (ClassCastException e) {
			throw new ClassCastException(activity.toString() + " must implement PetInfoFragment.PetInfoButtonListeners");
		}
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		petUri = (savedInstanceState == null) ? null : (Uri) savedInstanceState.getParcelable(PetsContract.Pets.CONTENT_ITEM_TYPE);

		Bundle extras = getActivity().getIntent().getExtras();
		if (extras != null) {
			petUri = extras.getParcelable(PetsContract.Pets.CONTENT_ITEM_TYPE);
		}

		// must be ready for onLoadFinished() to run at any time after this is called 
		getLoaderManager().initLoader(LOADER_ID, null, this);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_pet_info, container, false);
		mName = (TextView) view.findViewById(R.id.pet_info_name);
		mHeight = (TextView) view.findViewById(R.id.pet_info_height);
		mWeight = (TextView) view.findViewById(R.id.pet_info_weight);
		mBreed = (TextView) view.findViewById(R.id.pet_info_breed);
		mDateOfBirth = (TextView) view.findViewById(R.id.pet_info_dob);
		mMicrochip = (TextView) view.findViewById(R.id.pet_info_microchip);
		mPicture = (ImageView) view.findViewById(R.id.pet_info_picture);

		mPicture.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				mButtonListeners.onFindPicureButtonPressed();
			}
		});

		setHasOptionsMenu(true);
		return view;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch(item.getItemId()) {
		case R.id.action_new_picture: // same as clicking on the picture
			mButtonListeners.onFindPicureButtonPressed();
			return true;
		case R.id.action_edit:
			mButtonListeners.onEditButtonPressed(petUri);
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);

		outState.putParcelable(PetsContract.Pets.CONTENT_ITEM_TYPE, petUri);
	}

	@Override
	public void onDetach() {
		super.onDetach();
		mButtonListeners = null;
	}

	@Override
	public void newImageSelected(Uri imgUri) {
		Log.v(TAG, "returned media URI: " + imgUri);

		DisplayMetrics dMetrics = new DisplayMetrics();
		getActivity().getWindowManager().getDefaultDisplay().getMetrics(dMetrics);
		int maxDimen = Math.max(dMetrics.heightPixels, dMetrics.widthPixels);

		SaveImageFromFileTask task = new SaveImageFromFileTask(this,
				maxDimen, petUri.getLastPathSegment(), false);
		task.execute(imgUri);
	}

	@Override
	public void newImageSelectedKitKat(Uri imgUri) {
		Log.v(TAG, "returned media URI: " + imgUri);
		DisplayMetrics dMetrics = new DisplayMetrics();
		getActivity().getWindowManager().getDefaultDisplay().getMetrics(dMetrics);
		int maxDimen = Math.max(dMetrics.heightPixels, dMetrics.widthPixels);

		SaveImageFromFileTask task = new SaveImageFromFileTask(this,
				maxDimen, petUri.getLastPathSegment(), true);
		task.execute(imgUri);
	}

	@Override
	public void setNewImage() { // load new picture
		setImage();
	}

	private void setImage() { // runs from AsyncTask to not block UI thread
		File imageFile = getActivity().getFileStreamPath(AppUtils.imageFileName(petUri.getLastPathSegment()));

		if (imageFile.exists()) {
			DisplayMetrics dMetrics = new DisplayMetrics();
			getActivity().getWindowManager().getDefaultDisplay().getMetrics(dMetrics);

			// scale to height of screen, could be different depending on landscape/portrait mode
			// for now just set the max picture size to a fraction of the screen
			LoadBitmapTask task = new LoadBitmapTask(mPicture, dMetrics.heightPixels / 3) {
				@Override
				public void onImageLoadError() {
					setDefaultImage();
				}
			};
			task.execute(imageFile.getPath());
		}
		else { // no image has been defined for this pet, use a default icon
			setDefaultImage();
		}
	}

	protected void setDefaultImage() {
		Log.v(TAG, "using default image");
		Bitmap defaultBitmap = BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher);
		// resize image in case there was a failed image load
		LayoutParams lParams = mPicture.getLayoutParams();
		DisplayMetrics dMetrics = new DisplayMetrics();
		getActivity().getWindowManager().getDefaultDisplay().getMetrics(dMetrics);
		lParams.height = Math.max(Math.min(dMetrics.heightPixels / 3, defaultBitmap.getHeight()), dMetrics.heightPixels / 5);

		mPicture.setImageBitmap(defaultBitmap);
	}

	@Override
	public void onNewPetSelected() {
		long id = AppUtils.getLastViewedPetID(getActivity());
		petUri = Uri.withAppendedPath(Pets.CONTENT_URI, String.valueOf(id));
		getLoaderManager().restartLoader(LOADER_ID, null, this);
	}

	@Override
	public Loader<Cursor> onCreateLoader(int id, Bundle args) {
		// should limit to one result based on the URI id
		return new CursorLoader(getActivity(), petUri, 
				PetsContract.Pets.PROJECTION_ALL, null, null, null);
	}

	@Override
	public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
		// TODO picture size?
		if (data != null && data.getCount() > 0) {
			data.moveToFirst();
			mName.setText(data.getString(data.getColumnIndexOrThrow(Pets.NAME)));
			mName.setTextSize(28); // 28sp

			SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
			int unitSystem = Integer.parseInt(sharedPrefs.getString(getString(R.string.key_choose_units), "0"));
			String heightUnit;
			String weightUnit;
			if (unitSystem == 0) {
				heightUnit = getString(R.string.field_height_hint);
				weightUnit = getString(R.string.field_weight_hint);
			}
			else {
				heightUnit = getString(R.string.field_height_hint_en);
				weightUnit = getString(R.string.field_weight_hint_en);
			}

			long dateOfBirth = data.getLong(data.getColumnIndexOrThrow(Pets.DOB));

			mHeight.setText(getString(R.string.field_height) + " " + 
					data.getInt(data.getColumnIndexOrThrow(Pets.HEIGHT)) + " " + heightUnit);
			mWeight.setText(getString(R.string.field_weight) + " " + 
					data.getFloat(data.getColumnIndexOrThrow(Pets.WEIGHT)) + " " + weightUnit); 
			mBreed.setText(getString(R.string.field_breed) + " " + 
					data.getString(data.getColumnIndexOrThrow(Pets.BREED)));
			mDateOfBirth.setText(getString(R.string.field_dob) + " " +
					(dateOfBirth > 0 ? AppUtils.formatDate(dateOfBirth, getActivity()) : ""));
			mMicrochip.setText(getString(R.string.field_microchip) + " " + 
					data.getString(data.getColumnIndexOrThrow(Pets.MICROCHIP)));

			setImage();
		}
		else {
			Log.v(TAG, "onLoadFinished(): No cursor was returned!");
		}
	}

	@Override
	public void onLoaderReset(Loader<Cursor> loader) {
	}
}
