package com.paulro.petkeeper.utilities;

import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.widget.ImageView;

import java.lang.ref.WeakReference;

public abstract class LoadBitmapTask extends AsyncTask<String, Void, Bitmap> {

	private final WeakReference<ImageView> imageViewReference;
	private final int maxHeight;

	public LoadBitmapTask(ImageView imageView, int maxDimen) {
		imageViewReference = new WeakReference<>(imageView);
		this.maxHeight = maxDimen;
	}

	public abstract void onImageLoadError(); // implement method in whatever class creates task

	@Override
	protected Bitmap doInBackground(String... params) {
		String filePath = params[0];
		return AppUtils.decodeSampledBitmapFromFile(filePath, maxHeight);
	}

	@Override
	protected void onPostExecute(Bitmap result) {
		if (imageViewReference != null) {
			final ImageView imageView = imageViewReference.get();
			if (imageView != null) { // reference still valid
				if (result == null) {
					onImageLoadError();
				}
				else {
					android.view.ViewGroup.LayoutParams lParams = imageView.getLayoutParams();
					imageView.setImageBitmap(result);
					lParams.height = Math.min(maxHeight, result.getHeight());
				}
			}
		}
	}
}
