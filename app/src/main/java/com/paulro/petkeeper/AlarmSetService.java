package com.paulro.petkeeper;

import java.util.Calendar;

import android.app.AlarmManager;
import android.app.IntentService;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Handler;
import android.util.Log;
import android.widget.Toast;

import com.paulro.petkeeper.utilities.AppUtils;
import com.paulro.petkeeper.provider.PetsContract;
import com.paulro.petkeeper.provider.PetsContract.Meds;
import com.paulro.petkeeper.provider.PetsContract.NotificationFrequency;
import com.paulro.petkeeper.provider.PetsContract.Vaccinations;
import com.paulro.petkeeper.provider.PetsContract.VetVisits;

public class AlarmSetService extends IntentService {

	private static final String TAG = AlarmSetService.class.getSimpleName();

	private AlarmManager mAlarmMgr;
	private PendingIntent mPendIntent;
	private Handler mHandler;
	private boolean mRunningAtStartup;
	private boolean mShowToast;

	public AlarmSetService() { // default constructor required
		super(TAG);
	}

	public AlarmSetService(String name) {
		super(name);
	}

	@Override
	public void onCreate() {
		super.onCreate();
		mHandler = new Handler();
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		mRunningAtStartup = false;
		mShowToast = true;

		String intentAction = intent.getAction();
		if (intentAction != null) {
			if (intentAction.equals(Intent.ACTION_BOOT_COMPLETED)) {
				mRunningAtStartup = true;
				mShowToast = false;
				setAlarmsAtBoot();
				mRunningAtStartup = false;
				return; // don't want to continue as there is no dataUri from boot receiver
			}
			else if (intentAction.equals(NotificationClickFragment.TAG)) {
				mShowToast = false;
			}
		}

		Uri dataUri = intent.getData();
		if (dataUri != null) {
			String uriType = getContentResolver().getType(dataUri);
			switch (uriType) {
			case PetsContract.Meds.CONTENT_ITEM_TYPE:
				handleMeds(dataUri);
				break;
			case PetsContract.Vaccinations.CONTENT_ITEM_TYPE:
				handleVaccs(dataUri);
				break;
			case PetsContract.VetVisits.CONTENT_ITEM_TYPE:
				handleVisits(dataUri);
				break;
			default:
				Log.e(TAG, "something has gone terribly wrong!");
			}
		}

		// enable boot receiver if not enabled
		ComponentName bootReceiver = new ComponentName(getApplicationContext(), AlarmBootReceiver.class);
		PackageManager pm = this.getPackageManager();
//		Log.v(TAG, "Boot receiver component enabled setting: " + pm.getComponentEnabledSetting(bootReceiver));
		if (pm.getComponentEnabledSetting(bootReceiver) != PackageManager.COMPONENT_ENABLED_STATE_ENABLED){
			pm.setComponentEnabledSetting(bootReceiver, PackageManager.COMPONENT_ENABLED_STATE_ENABLED, 
					PackageManager.DONT_KILL_APP);
		}
	}

	private void handleMeds(Uri medUri) {
		Cursor med = getContentResolver().query(medUri, PetsContract.Meds.PROJECTION_ALL, null, null, null);
		if (med == null || !med.moveToFirst()) // record not found
			return;
		
		int setNotification = med.getInt(med.getColumnIndexOrThrow(Meds.NOTIF_IS_SET));
		if (setNotification != 0) {
			long dateMostRecentUnixTime = med.getLong(med.getColumnIndexOrThrow(Meds.DATE_MOST_RECENT));
			long endDateUnixTime = med.getLong(med.getColumnIndexOrThrow(Meds.END_DATE));
			long frequencyIndex = med.getLong(med.getColumnIndexOrThrow(Meds.FREQUENCY));
			
			Calendar cal = Calendar.getInstance();
			long currentTime = cal.getTimeInMillis();
			
			if (endDateUnixTime > 0 && currentTime > endDateUnixTime) {
				return;
			}
			cal.setTimeInMillis(dateMostRecentUnixTime);
			
			// look up the time interval in the frequencies table
			Cursor freq = getContentResolver().query(Uri.withAppendedPath(PetsContract.NotificationFrequency.CONTENT_URI, 
					String.valueOf(frequencyIndex)), PetsContract.NotificationFrequency.PROJECTION_ALL, null, null, null);
			
			freq.moveToFirst();
			int timeValue = freq.getInt(freq.getColumnIndexOrThrow(NotificationFrequency.TIME_VALUE));
			NotificationFrequency.TimeUnits timeUnit = NotificationFrequency.TimeUnits.valueOf(freq.getString(
					freq.getColumnIndexOrThrow(NotificationFrequency.TIME_UNIT)));
            int calendarConstant = timeUnit.getCalendarConstant();
			AppUtils.updateCalendar(cal, calendarConstant, timeValue, TAG); // move calendar to new time
			
			// TODO get rid of this
//			cal.add(Calendar.MINUTE, timeValue + 20); // just for testing
//			Log.v(TAG, "actual calendar constant: " + timeUnit.getCalendarConstant()); // just to make the warning go away
			
			final long alarmTime = cal.getTimeInMillis(); // final so it can be accessed by anonymous class

			// notification id from the meds table to use as request identifier
			int requestCode = med.getInt(med.getColumnIndexOrThrow(Meds.NOTIF_ID));
			mAlarmMgr = (AlarmManager) this.getSystemService(Context.ALARM_SERVICE);

			// remove any other alarms that are registered for this requestcode
			cancelPendingAlarm(mAlarmMgr, new Intent(this, AlarmReceiver.class), medUri, requestCode);

			if (alarmTime > currentTime) { // schedule alarm
				Intent intent = new Intent(this, AlarmReceiver.class); // must be an explicit intent
				intent.setData(medUri); // this persists across reboots, extras do not
				intent.setAction(AppUtils.NOTIFICATION_FUTURE); // lets the alarm receiver know what time to use for notification

				mPendIntent = PendingIntent.getBroadcast(getApplicationContext(), requestCode, 
						intent, PendingIntent.FLAG_UPDATE_CURRENT);
				mAlarmMgr.set(AlarmManager.RTC_WAKEUP, alarmTime, mPendIntent);
				
				if (mShowToast) {
					mHandler.post(new Runnable() {
						@Override
						public void run() { // use a handler so that the toast doesn't get orphaned on a dead thread
							AppUtils.makeToast(AlarmSetService.this, "Notification set for " + 
									AppUtils.formatDateTime(alarmTime, AlarmSetService.this), Toast.LENGTH_LONG);
						}
					});
				}
			}
			else { // notification time is already past
				Intent intent = new Intent(this, AlarmReceiver.class);
				intent.setData(medUri);
				intent.setAction(AppUtils.NOTIFICATION_PAST_DUE);

				mPendIntent = PendingIntent.getBroadcast(getApplicationContext(), requestCode, 
						intent, PendingIntent.FLAG_UPDATE_CURRENT);
				mAlarmMgr.set(AlarmManager.RTC_WAKEUP, alarmTime, mPendIntent);
			}

			if (!mRunningAtStartup) {
				ContentValues values = new ContentValues();
				// no need to put all the existing values back in
				values.put(Meds.NOTIF_DATE_TIME, alarmTime);

				// update database
				getContentResolver().update(medUri, values, null, null);
			}

			// close cursor
			freq.close();
		}
		med.close();
	}

	private void handleVaccs(Uri vacUri) {
		Cursor vacc = getContentResolver().query(vacUri, Vaccinations.PROJECTION_ALL, null, null, null);
		if (vacc == null || !vacc.moveToFirst()) // prevents CursorIndexOutOfBoundsException on next line
			return;

		if (vacc.getInt(vacc.getColumnIndexOrThrow(Vaccinations.NOTIF_IS_SET)) != 0) {
			long expirationDateMillis = vacc.getLong(vacc.getColumnIndexOrThrow(Vaccinations.EXPIRATION));

			Calendar cal = Calendar.getInstance();
			long currentTime = cal.getTimeInMillis();

			cal.setTimeInMillis(expirationDateMillis);
			// alert one month prior?
			cal.add(Calendar.MONTH, -1);

			final long alarmTime = cal.getTimeInMillis();

			mAlarmMgr = (AlarmManager) this.getSystemService(Context.ALARM_SERVICE);
			Intent alarmIntent = new Intent(this, AlarmReceiver.class);
			alarmIntent.setData(vacUri);
			if (alarmTime > currentTime) {
				alarmIntent.setAction(AppUtils.NOTIFICATION_FUTURE);
			}
			else {
				alarmIntent.setAction(AppUtils.NOTIFICATION_PAST_DUE);
			}

			int requestCode = vacc.getInt(vacc.getColumnIndexOrThrow(Vaccinations.NOTIF_ID));

			mPendIntent = PendingIntent.getBroadcast(getApplicationContext(), requestCode, 
					alarmIntent, PendingIntent.FLAG_UPDATE_CURRENT);
			mAlarmMgr.set(AlarmManager.RTC_WAKEUP, alarmTime, mPendIntent);

			if (!mRunningAtStartup) {
				mHandler.post(new Runnable() {
					@Override
					public void run() {
						AppUtils.makeToast(AlarmSetService.this, "Notification set for " + 
								AppUtils.formatDateTime(alarmTime, AlarmSetService.this), Toast.LENGTH_LONG);
					}
				});

				ContentValues values = new ContentValues(); // if running at startup, no need to update anything
				values.put(Vaccinations.NOTIF_DATE_TIME, alarmTime);
				getContentResolver().update(vacUri, values, null, null);
			}
		}

		vacc.close();
	}

	private void handleVisits(Uri vetUri) { // TODO calendar entry instead?
		Cursor visit = getContentResolver().query(vetUri, VetVisits.PROJECTION_ALL, null, null, null);
		if (visit == null || !visit.moveToFirst())
			return;

		if (visit.getInt(visit.getColumnIndexOrThrow(VetVisits.NOTIF_IS_SET)) != 0) {
			long nextVisitDateMillis = visit.getLong(visit.getColumnIndexOrThrow(VetVisits.NEXT_APPOINTMENT_DATE));

			Calendar cal = Calendar.getInstance();
			long currentTime = cal.getTimeInMillis();

			cal.setTimeInMillis(nextVisitDateMillis);
			// alert one day prior?
			cal.add(Calendar.DAY_OF_YEAR, -1);

			final long alarmTime = cal.getTimeInMillis();

			mAlarmMgr = (AlarmManager) this.getSystemService(Context.ALARM_SERVICE);
			Intent alarmIntent = new Intent(this, AlarmReceiver.class);
			alarmIntent.setData(vetUri);
			if (alarmTime > currentTime) {
				alarmIntent.setAction(AppUtils.NOTIFICATION_FUTURE);
			}
			else {
				alarmIntent.setAction(AppUtils.NOTIFICATION_PAST_DUE);
			}

			int requestCode = visit.getInt(visit.getColumnIndexOrThrow(VetVisits.NOTIF_ID));

			mPendIntent = PendingIntent.getBroadcast(getApplicationContext(), requestCode, alarmIntent, 
					PendingIntent.FLAG_UPDATE_CURRENT);
			mAlarmMgr.set(AlarmManager.RTC_WAKEUP, alarmTime, mPendIntent);

			if (!mRunningAtStartup) {
				mHandler.post(new Runnable() {
					@Override
					public void run() { // use application context?
						AppUtils.makeToast(getApplicationContext(),"Notification set for " + 
								AppUtils.formatDateTime(alarmTime, AlarmSetService.this), Toast.LENGTH_LONG);
					}
				});

				ContentValues values = new ContentValues();
				values.put(VetVisits.NOTIF_DATE_TIME, alarmTime);
				getContentResolver().update(vetUri, values, null, null);
			}
		}

		visit.close();
	}

	private void cancelPendingAlarm(AlarmManager manager, Intent intent, Uri uri, int requestCode) {
		intent.setData(uri);
		intent.setAction(AppUtils.NOTIFICATION_FUTURE); // needs to be the same action as was originally set with

		PendingIntent pendIntent = PendingIntent.getBroadcast(getApplicationContext(), 
				requestCode, intent, PendingIntent.FLAG_UPDATE_CURRENT);
		manager.cancel(pendIntent); // remove alarms set by NotifAndNextAlarmService
	}

	private void setAlarmsAtBoot() {
		Cursor meds = getContentResolver().query(Meds.CONTENT_URI, new String[] { Meds._ID, Meds.NOTIF_IS_SET }, 
				Meds.NOTIF_IS_SET + " > 0", null, null);
		if (meds.moveToFirst()) { // returns false if the cursor is empty
			while (!meds.isAfterLast()) {
				long id = meds.getLong(meds.getColumnIndexOrThrow(Meds._ID));
				handleMeds(Uri.withAppendedPath(Meds.CONTENT_URI, String.valueOf(id)));
				meds.moveToNext();
			}
		}
		meds.close();

		Cursor vaccs = getContentResolver().query(Vaccinations.CONTENT_URI, 
				new String[] {Vaccinations._ID, Vaccinations.NOTIF_IS_SET }, Vaccinations.NOTIF_IS_SET + " > 0", null, null);
		if (vaccs.moveToFirst()) {
			while (!vaccs.isAfterLast()) {
				long id = vaccs.getLong(vaccs.getColumnIndexOrThrow(Vaccinations._ID));
				handleVaccs(Uri.withAppendedPath(Vaccinations.CONTENT_URI, String.valueOf(id)));
				vaccs.moveToNext();
			}
		}
		vaccs.close();

		Cursor visits = getContentResolver().query(VetVisits.CONTENT_URI, new String[] { VetVisits._ID, VetVisits.NOTIF_IS_SET }, 
				VetVisits.NOTIF_IS_SET + " > 0", null, null);
		if (visits.moveToFirst()) {
			while (!visits.isAfterLast()) {
				long id = visits.getLong(visits.getColumnIndexOrThrow(VetVisits._ID));
				handleVisits(Uri.withAppendedPath(VetVisits.CONTENT_URI, String.valueOf(id)));
				visits.moveToNext();
			}
		}
		visits.close();
	}
}
