package com.paulro.petkeeper;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class AlarmBootReceiver extends BroadcastReceiver {

	@Override
	public void onReceive(Context context, Intent intent) {
		// TODO wake lock?
		if (intent.getAction().equals(Intent.ACTION_BOOT_COMPLETED)) {
			// start service to set all alarms
			Intent serviceIntent = new Intent(context, AlarmSetService.class);
			serviceIntent.setAction(Intent.ACTION_BOOT_COMPLETED);
			context.startService(serviceIntent);
		}
	}
}
