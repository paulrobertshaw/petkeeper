package com.paulro.petkeeper.utilities;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.ParcelFileDescriptor;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.widget.Toast;

import java.io.FileOutputStream;
import java.lang.ref.WeakReference;

public class SaveImageFromFileTask extends AsyncTask<Uri, Void, Void> {

	private final WeakReference<ImageSaverFragment> fragmentReference;
	private final int maxDimen;
	private final String petID;
	private final boolean kitKatPlus;

	public SaveImageFromFileTask(ImageSaverFragment fragment, int maxDimen, String petID, boolean kitKatPlus) {
		fragmentReference = new WeakReference<>(fragment);
		this.maxDimen = maxDimen;
		this.petID = petID;
		this.kitKatPlus = kitKatPlus;
	}

	@Override
	protected Void doInBackground(Uri... params) {
		Uri imgUri = params[0];

		Bitmap bmp = null;

		String fileName = AppUtils.imageFileName(petID);
		FileOutputStream fos = null;
		try {
			Fragment fragment = (Fragment) fragmentReference.get();
			int startIndex = "content://".length();
			if (kitKatPlus && !imgUri.toString().startsWith("media/external/images/", startIndex)) {
				ParcelFileDescriptor pfd =
						fragment.getActivity().getContentResolver().openFileDescriptor(imgUri, "r"); // r for read
				// document provider doesn't find orientation
				bmp = AppUtils.decodeSampledBitmapFromFileDescriptor(pfd.getFileDescriptor(), maxDimen);
				pfd.close();
			}
			else {
				String filePath = getPath(imgUri, fragment.getActivity());
				if (filePath == null) {
					// could be a Picasa image
					String authority = imgUri.getAuthority();
					Log.v("authority", authority);
					if (!authority.startsWith("com.android.providers.media")) {
						// TODO phrasing!
						AppUtils.makeToast(fragment.getActivity(), "Sorry, only local files can be loaded", Toast.LENGTH_LONG);
						return null;
					}
				}
				// rotate image before saving it.
				bmp = AppUtils.rotateBitmap(filePath, AppUtils.decodeSampledBitmapFromFile(filePath, maxDimen));
			}

			fos = fragment.getActivity().openFileOutput(fileName, Context.MODE_PRIVATE);
			bmp.compress(Bitmap.CompressFormat.JPEG, 75, fos); // quality between 0 and 100
			fos.flush();
		} catch (Exception e) {
			e.printStackTrace();
		}
		finally {
			try {
				fos.close();
				bmp.recycle(); // release the memory this image was using
			} catch (Exception e) {
				e.printStackTrace(); // mostly ignore exception
			}
		}

		return null;
	}

	private String getPath(Uri imgUri, final Context context) {
		Cursor imgs = context.getContentResolver().query(imgUri,
				new String[] { MediaStore.Images.Media.DATA }, null, null, null);
		if (imgs != null) {
			imgs.moveToFirst();
			int dataIndex = imgs.getColumnIndex(MediaStore.Images.Media.DATA);
			String path = null;
			if (dataIndex > -1) {
				path = imgs.getString(dataIndex);
			}
			imgs.close();
			return path;
		}
		return null; // if the file is not in the gallery, or as a fallback
	}

	@Override
	protected void onPostExecute(Void result) {
		if (fragmentReference != null) {
			final ImageSaverFragment fragment = fragmentReference.get();
			if (fragment != null) { // check to make sure the referenced object still exists
				// restart loader rather than doing the same scaling computation in this task
				// allows the previous image to be recycled too
				fragment.setNewImage();
			}
		}
	}
}
