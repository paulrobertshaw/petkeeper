package com.paulro.petkeeper.utilities;

public interface NewPetSelected {
	public void onNewPetSelected();
}
