package com.paulro.petkeeper.provider;

import java.util.Calendar;

import android.content.ContentResolver;
import android.net.Uri;
import android.provider.BaseColumns;

public final class PetsContract {

	public static final String AUTHORITY = "com.paulro.petkeeper.provider";
	
	public static final Uri CONTENT_URI = Uri.parse("content://" + AUTHORITY);
	
	public static final String SELECTION_BY_ID = BaseColumns._ID + " = ? ";
	
	/**
	 * Constants for the pets table
	 */
	public static final class Pets implements BaseColumns {
		public static final Uri CONTENT_URI = Uri.withAppendedPath(PetsContract.CONTENT_URI, "pets");
		
		public static final String CONTENT_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE + "/vnd.com.paulro.provider_pets";
		
		public static final String CONTENT_ITEM_TYPE = ContentResolver.CURSOR_ITEM_BASE_TYPE + "/vnd.com.paulro.provider_pets";
		
		public static final String NAME = "pet_name";
		public static final String HEIGHT = "height";
		public static final String WEIGHT = "weight";
		public static final String DOB = "date_of_birth";
		public static final String BREED = "breed";
		public static final String MICROCHIP = "microchip";
		public static final String GENDER = "gender";
		public static final String PHOTO = "photo";
		
		public static final String[] PROJECTION_ALL = { _ID, NAME, HEIGHT, WEIGHT, DOB, BREED, MICROCHIP, GENDER, PHOTO };
		
		public static final String SORT_ORDER_DEFAULT = NAME + " COLLATE NOCASE";
	}
	
	/**
	 * Constants for the meds table
	 */
	public static final class Meds implements BaseColumns {
		public static final Uri CONTENT_URI = Uri.withAppendedPath(PetsContract.CONTENT_URI, "meds");
		
		public static final String CONTENT_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE + "/vnd.com.paulro.provider_meds";
		
		public static final String CONTENT_ITEM_TYPE = ContentResolver.CURSOR_ITEM_BASE_TYPE + "/vnd.com.paulro.provider_meds";
		
		public static final String NAME = "med_name";
		public static final String FREQUENCY = "med_frequency";
		public static final String DATE_BEGUN = "date_begun";
		public static final String DATE_MOST_RECENT = "date_most_recent";
		public static final String END_DATE = "end_date";
		public static final String DOSE = "dose";
		public static final String NOTIF_DATE_TIME = "notif_date_time";
		public static final String NOTIF_ID = "notif_id";
		public static final String NOTIF_IS_SET = "notif_is_set";
		public static final String PET_ID = "pet_id";
		
		public static final String[] PROJECTION_ALL = 
			{ _ID, NAME, FREQUENCY, DATE_BEGUN, DATE_MOST_RECENT, END_DATE, DOSE, NOTIF_DATE_TIME, NOTIF_ID, NOTIF_IS_SET, PET_ID };
		
		public static final String SORT_ORDER_DEFAULT = DATE_MOST_RECENT + " DESC";
		
		public static final String SELECTION_BY_FOREIGN_KEY = PET_ID + " = ? ";
	}
	
	/**
	 * Constants for the vet_visits table
	 */
	public static final class VetVisits implements BaseColumns {
		public static final Uri CONTENT_URI = Uri.withAppendedPath(PetsContract.CONTENT_URI, "vet_visits");
		
		public static final String CONTENT_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE + "/vnd.com.paulro.provider_vet_visits";
		
		public static final String CONTENT_ITEM_TYPE = ContentResolver.CURSOR_ITEM_BASE_TYPE + "/vnd.com.paulro.provider_vet_visits";
		
		// the names of the columns
		public static final String DOCTOR = "doctor";
		public static final String DATE = "visit_date";
		public static final String NEXT_APPOINTMENT_DATE = "next_appointment";
		public static final String SUMMARY = "summary";
		public static final String NOTIF_DATE_TIME = "notif_date_time";
		public static final String NOTIF_ID = "notif_id";
		public static final String NOTIF_IS_SET = "notif_is_set";
		public static final String PET_ID = "pet_id";
		
		public static final String[] PROJECTION_ALL = 
			{ _ID, DOCTOR, DATE, NEXT_APPOINTMENT_DATE, SUMMARY, NOTIF_DATE_TIME, NOTIF_ID, NOTIF_IS_SET, PET_ID };
		
		public static final String SORT_ORDER_DEFAULT = DATE + " DESC";
		
		public static final String SELECTION_BY_FOREIGN_KEY = PET_ID + " = ? ";
	}
	
	/**
	 * Constants for the vaccinations table
	 */
	public static final class Vaccinations implements BaseColumns {
		public static final Uri CONTENT_URI = Uri.withAppendedPath(PetsContract.CONTENT_URI, "vaccinations");
		
		public static final String CONTENT_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE + "/vnd.com.paulro.provider_vaccinations";
		
		public static final String CONTENT_ITEM_TYPE = ContentResolver.CURSOR_ITEM_BASE_TYPE + "/vnd.com.paulro.provider_vaccinations";

		public static final String NAME = "vac_name";
		public static final String DATE = "vac_date";
		public static final String EXPIRATION = "expiration_date";
		public static final String NUMBER = "vac_number";
		public static final String DOCTOR = "doctor";
		public static final String NOTIF_DATE_TIME = "notif_date_time";
		public static final String NOTIF_ID = "notif_id";
		public static final String NOTIF_IS_SET = "notif_is_set";
		public static final String PET_ID = "pet_id";
		
		public static final String[] PROJECTION_ALL = 
			{ _ID, NAME, DATE, EXPIRATION, NUMBER, DOCTOR, NOTIF_DATE_TIME, NOTIF_ID, NOTIF_IS_SET, PET_ID };
		
		public static final String SORT_ORDER_DEFAULT = DATE + " DESC";
		
		public static final String SELECTION_BY_FOREIGN_KEY = PET_ID + " = ? ";
	}
	
	/**
	 * Constants for the notifications table
	 */
	public static final class NotificationFrequency implements BaseColumns {
		public static final Uri CONTENT_URI = Uri.withAppendedPath(PetsContract.CONTENT_URI, "notif_freq");
		
		public static final String CONTENT_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE + "/vnd.com.paulro.provider_notif_freq";
		
		public static final String CONTENT_ITEM_TYPE = ContentResolver.CURSOR_ITEM_BASE_TYPE + "/vnd.com.paulro.provider_notif_freq";
		
		public static final String DESCRIPTION = "description";
		public static final String TIME_VALUE = "time_value";
		public static final String TIME_UNIT = "time_unit";
		
		public static final String[] PROJECTION_ALL = { _ID, DESCRIPTION, TIME_VALUE, TIME_UNIT };
		
		public static final String SORT_ORDER_DEFAULT = _ID + " ASC";
		
		public static enum TimeUnits { // for the custom interval menu, if I ever use it
			HOUR (0, "hour", Calendar.HOUR_OF_DAY),
			DAY (1, "day", Calendar.DAY_OF_MONTH), // check if this wraps around
			WEEK (2, "week", Calendar.WEEK_OF_YEAR),
			MONTH (3, "month", Calendar.MONTH);
			
			private final int index;
			private final String name;
			private final int calendarConstant;
			TimeUnits(int index, String name, int calendarConstant) {
				this.index = index;
				this.name = name;
				this.calendarConstant = calendarConstant;
			}
			public int getIndex() {
				return index;
			}
			public String getName() {
				return name;
			}
			public int getCalendarConstant() {
				return calendarConstant;
			}
		}
	}
	
	public static final class NotificationId implements BaseColumns {
		public static final String TBL_NAME = "notif_id";
	}
}
