package com.paulro.petkeeper.utilities;

import android.net.Uri;

public interface ImageSaverFragment {
	public void newImageSelected(Uri imgUri);
	public void newImageSelectedKitKat(Uri imgUri);
	public void setNewImage();
}
