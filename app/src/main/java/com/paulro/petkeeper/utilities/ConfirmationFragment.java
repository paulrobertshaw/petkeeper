package com.paulro.petkeeper.utilities;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;

import com.paulro.petkeeper.NavigationDrawerFragment;

public class ConfirmationFragment extends DialogFragment {

	public static final String TAG = ConfirmationFragment.class.getSimpleName();

	private Context mContext;
	private NavigationDrawerFragment.NavigationDrawerCallbacks mCallbacks;

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		mContext = activity;
		if (activity instanceof NavigationDrawerFragment.NavigationDrawerCallbacks) {
			mCallbacks = (NavigationDrawerFragment.NavigationDrawerCallbacks) activity;
		}
	}

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		// TODO localize
		AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
		builder.setMessage("Are you sure you want to delete this pet?");
		builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				if (mCallbacks != null) {
					mCallbacks.onDeletePetSelected(true);
				}
			}
		});
		builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
			}
		});

		return builder.create();
	}

	@Override
	public void onDetach() {
		super.onDetach();
		mCallbacks = null;
	}
}
