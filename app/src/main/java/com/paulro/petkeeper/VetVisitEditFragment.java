package com.paulro.petkeeper;

import java.util.Calendar;

import com.paulro.petkeeper.utilities.AppUtils;
import com.paulro.petkeeper.provider.PetsContract;
import com.paulro.petkeeper.provider.PetsContract.VetVisits;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.app.ActionBarActivity;
import android.text.TextUtils;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TimePicker;
import android.widget.Toast;

public class VetVisitEditFragment extends Fragment implements LoaderManager.LoaderCallbacks<Cursor> {

	private static final int LOADER_ID = 51;

	private EditText mDoctor;

	private EditText mDate;
	private long mDateUnixTime = 0;
	private boolean mDateHasFocus = false;
	private EditText mDateNextVisit;
	private long mDateNextVisitUnixTime = 0;
	private boolean mDateNextVisitHasFocus = false;

	private EditText mSummary;

	private CheckBox mSetNotification;
	private boolean mUserClearNotification = false;

	private Uri petUri;
	private Uri vetUri;

	private int mDatePickerBugfix = 0;

	private DatePickerDialog.OnDateSetListener mDatePickerListner = new DatePickerDialog.OnDateSetListener() {

		@Override
		public void onDateSet(DatePicker view, int year, int monthOfYear,
				int dayOfMonth) {
			if (mDatePickerBugfix > 0) { // annoyingly, onDateSet is called twice in JellyBean and higher
				return;
			}
			mDatePickerBugfix++;

			if (mDateHasFocus) {
				if (mDateUnixTime > 0) {
					// preserves the user's input time
					mDateUnixTime = AppUtils.getUnixTime(view, mDateUnixTime);
				}
				else {
					// no time has already been entered, start with current time
					mDateUnixTime = AppUtils.getUnixTime(view);
				}
				showTimePickerDialog(mDateUnixTime);
			}
			else if (mDateNextVisitHasFocus) {
				if (mDateNextVisitUnixTime > 0) {
					mDateNextVisitUnixTime = AppUtils.getUnixTime(view, mDateNextVisitUnixTime);
				}
				else {
					mDateNextVisitUnixTime = AppUtils.getUnixTime(view);
				}
				showTimePickerDialog(mDateNextVisitUnixTime);
			}
		}
	};
	private TimePickerDialog.OnTimeSetListener mTimePickerListener = new TimePickerDialog.OnTimeSetListener() {

		@Override
		public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
			if (mDateHasFocus) {
				mDateUnixTime = AppUtils.getUnixTimeOfDay(view, mDateUnixTime);
				mDate.setText(AppUtils.formatDateTime(mDateUnixTime, getActivity()));
			}
			else if (mDateNextVisitHasFocus) {
				mDateNextVisitUnixTime = AppUtils.getUnixTimeOfDay(view, mDateNextVisitUnixTime);
				mDateNextVisit.setText(AppUtils.formatDateTime(mDateNextVisitUnixTime, getActivity()));
				// if user has not already unchecked the box, automatically set a notification if next visit date provided
				if (!mUserClearNotification) {
					mSetNotification.setChecked(true);
				}
			}
		}
	};

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		petUri = (savedInstanceState == null) ? null : (Uri) savedInstanceState.getParcelable(PetsContract.Pets.CONTENT_ITEM_TYPE);
		vetUri = (savedInstanceState == null) ? null : 
			(Uri) savedInstanceState.getParcelable(PetsContract.VetVisits.CONTENT_ITEM_TYPE);

		mDateUnixTime = (savedInstanceState == null) ? 0 : savedInstanceState.getLong(PetsContract.VetVisits.DATE);
		mDateNextVisitUnixTime = (savedInstanceState == null) ? 0 : savedInstanceState.getLong(PetsContract.VetVisits.NEXT_APPOINTMENT_DATE);

		Bundle extras = getActivity().getIntent().getExtras();
		if (extras != null) {
			petUri = extras.getParcelable(PetsContract.Pets.CONTENT_ITEM_TYPE);
			vetUri = extras.getParcelable(PetsContract.VetVisits.CONTENT_ITEM_TYPE);
		}
		if (vetUri != null) { // only need loader if there is an existing record
			if (getLoaderManager().getLoader(LOADER_ID) == null) {
				getLoaderManager().initLoader(LOADER_ID, null, this);
			}
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_vet_visit_edit, container, false);

		mDoctor = (EditText) view.findViewById(R.id.field_doctor);
		mDate = (EditText) view.findViewById(R.id.field_date);
		mDateNextVisit = (EditText) view.findViewById(R.id.field_date_next_visit);
		mSummary = (EditText) view.findViewById(R.id.field_summary);
		mSetNotification = (CheckBox) view.findViewById(R.id.check_set_notification);
		mSetNotification.setChecked(false); // checked only when next visit date set
		// if user has not already unchecked the box, automatically set a notification if next visit date provided
		mSetNotification.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				mUserClearNotification = true;
			}
		});

		if (vetUri == null) { // Uri is not passed in, therefore is for a new record
			// title should be "add", not "edit
			if (getActivity() instanceof ActionBarActivity) {
				((ActionBarActivity) getActivity()).getSupportActionBar()
				.setTitle(getString(R.string.title_activity_vet_visit_edit_add));
			}
		}
		mDate.setOnFocusChangeListener(new View.OnFocusChangeListener() {

			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				if (hasFocus) {
					mDatePickerBugfix = 0;
					mDateHasFocus = true;
					showDatePickerDialog(mDateUnixTime);
				}
				else {
					mDateHasFocus = false;
				}
			}
		});
		mDateNextVisit.setOnFocusChangeListener(new View.OnFocusChangeListener() {

			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				if (hasFocus) {
					mDatePickerBugfix = 0;
					mDateNextVisitHasFocus = true;
					showDatePickerDialog(mDateNextVisitUnixTime);
				}
				else {
					mDateNextVisitHasFocus = false;
				}
			}
		});

		setHasOptionsMenu(true);
		return view;
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putParcelable(PetsContract.Pets.CONTENT_ITEM_TYPE, petUri);
		outState.putParcelable(PetsContract.VetVisits.CONTENT_ITEM_TYPE, vetUri);

		outState.putString(VetVisits.DOCTOR, mDoctor.getText().toString());
		outState.putLong(VetVisits.DATE, mDateUnixTime);
		outState.putLong(VetVisits.NEXT_APPOINTMENT_DATE, mDateNextVisitUnixTime);
		outState.putString(VetVisits.SUMMARY, mSummary.getText().toString());
		outState.putBoolean(VetVisits.NOTIF_IS_SET, mSetNotification.isChecked());
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.action_save:
			if (saveData()) { // save was successful
				getActivity().setResult(Activity.RESULT_OK);
				getActivity().finish();
			}
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onDetach() {
		super.onDetach();
		mDatePickerListner = null;
		mTimePickerListener = null;
	}

	protected void showDatePickerDialog(long startTime) {
		int[] startDate = (startTime > 0) ? AppUtils.getFriendlyDate(startTime) : 
			AppUtils.getFriendlyDate(Calendar.getInstance().getTimeInMillis());
		DatePickerDialog dpd = new DatePickerDialog(getActivity(), mDatePickerListner, startDate[0], startDate[1], startDate[2]);
		dpd.show();
	}

	protected void showTimePickerDialog(long startTime) {
		int[] initialTime = (startTime > 0) ? AppUtils.getFriendlyTime(startTime): 
			AppUtils.getFriendlyTime(Calendar.getInstance().getTimeInMillis());
		TimePickerDialog tpd = new TimePickerDialog(getActivity(), mTimePickerListener, 
				initialTime[0], initialTime[1], DateFormat.is24HourFormat(getActivity()));
		tpd.show();
	}

	private boolean saveData() {
		String doctor = mDoctor.getText().toString();
		boolean setNotification = mSetNotification.isChecked();
		if (TextUtils.isEmpty(doctor)) {
			AppUtils.makeToast(getActivity(), "Doctor name can not be empty", Toast.LENGTH_LONG);
			return false;
		}
		if (mDateUnixTime == 0) {
			AppUtils.makeToast(getActivity(), "Please enter date of visit", Toast.LENGTH_LONG);
			return false;
		}
		if (mDateNextVisitUnixTime > 0 && mDateUnixTime > mDateNextVisitUnixTime) {
			AppUtils.makeToast(getActivity(), "Next appointment date is before this appointment date", Toast.LENGTH_LONG);
			return false;
		}

		if (mDateNextVisitUnixTime == 0 && setNotification) {
			AppUtils.makeToast(getActivity(), "Cannot set notification without next visit date!", Toast.LENGTH_LONG);
			return false;
		}

		ContentValues values = new ContentValues();
		values.put(VetVisits.DOCTOR, doctor);
		values.put(VetVisits.DATE, (mDateUnixTime == 0) ? null : mDateUnixTime);
		values.put(VetVisits.NEXT_APPOINTMENT_DATE, (mDateNextVisitUnixTime == 0) ? null : mDateNextVisitUnixTime);
		values.put(VetVisits.SUMMARY, mSummary.getText().toString());
		values.put(VetVisits.NOTIF_IS_SET, setNotification);
		values.put(VetVisits.PET_ID, petUri.getLastPathSegment());

		if (vetUri == null) {
			vetUri = getActivity().getContentResolver().insert(PetsContract.VetVisits.CONTENT_URI, values);
		}
		else {
			getActivity().getContentResolver().update(vetUri, values, null, null);
		}

		if (setNotification) {
			Intent serviceIntent = new Intent(getActivity(), AlarmSetService.class);
			serviceIntent.setData(vetUri);
			getActivity().startService(serviceIntent);
		}
		
		return true;
	}

	@Override
	public Loader<Cursor> onCreateLoader(int id, Bundle args) {
		return new CursorLoader(getActivity(), vetUri, PetsContract.VetVisits.PROJECTION_ALL, null, null, null);
	}

	@Override
	public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
		if (data != null && data.getCount() > 0) {
			data.moveToFirst();

			mDoctor.setText(data.getString(data.getColumnIndexOrThrow(VetVisits.DOCTOR)));
			mDateUnixTime = data.getLong(data.getColumnIndexOrThrow(VetVisits.DATE));
			mDateNextVisitUnixTime = data.getLong(data.getColumnIndexOrThrow(VetVisits.NEXT_APPOINTMENT_DATE));
			mSummary.setText(data.getString(data.getColumnIndexOrThrow(VetVisits.SUMMARY)));
			mSetNotification.setChecked(data.getInt(data.getColumnIndexOrThrow(VetVisits.NOTIF_IS_SET)) != 0);

			if (mDateUnixTime > 0) {
				mDate.setText(AppUtils.formatDateTime(mDateUnixTime, getActivity()));
			}
			if (mDateNextVisitUnixTime > 0) {
				mDateNextVisit.setText(AppUtils.formatDateTime(mDateNextVisitUnixTime, getActivity()));
			}
		}
	}

	@Override
	public void onLoaderReset(Loader<Cursor> loader) {
	}
}
