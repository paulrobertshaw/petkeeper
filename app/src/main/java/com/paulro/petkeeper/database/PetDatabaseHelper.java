package com.paulro.petkeeper.database;

import com.paulro.petkeeper.provider.PetsContract;
import com.paulro.petkeeper.provider.PetsContract.NotificationId;
import com.paulro.petkeeper.provider.PetsContract.NotificationFrequency.TimeUnits;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class PetDatabaseHelper extends SQLiteOpenHelper {

	private static final String DB_NAME = "petcare.db";
	private static final int VERSION = 1;

	public PetDatabaseHelper(Context context) {
		super(context, DB_NAME, null, VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL(TblPets.DDL_CREATE_TBL_PETS);
		db.execSQL(TblMeds.DDL_CREATE_TBL_MEDS);
		db.execSQL(TblVisits.DDL_CREATE_TBL_VET_VISITS);
		db.execSQL(TblVaccs.DDL_CREATE_TBL_VACCINATIONS);
		db.execSQL(TblNotifFrequency.DDL_CREATE_TABLE_NOTIFICATIONS);
		String notificationCountTable = "CREATE TABLE " + NotificationId.TBL_NAME + " (" +
				NotificationId._ID + " INTEGER PRIMARY KEY " + ")"; //AUTOINCREMENT
		db.execSQL(notificationCountTable);

        String DDL_CREATE_TRIGGER_DEL_MEDS = "CREATE TRIGGER delete_meds DELETE ON " + TblMeds.TBL_NAME + "\n" +
                " BEGIN \n" +
                " DELETE FROM " + NotificationId.TBL_NAME + " WHERE " + NotificationId._ID + " = old." + TblMeds.COL_NOTIF_ID + ";\n" +
                " END \n";
        String DDL_CREATE_TRIGGER_DEL_VACCS = "CREATE TRIGGER delete_vaccs DELETE ON " + TblVaccs.TBL_NAME + "\n" +
                " BEGIN \n" +
                " DELETE FROM " + NotificationId.TBL_NAME + " WHERE " + NotificationId._ID + " = old." + TblVaccs.COL_NOTIF_ID + ";\n" +
                " END \n";
        String DDL_CREATE_TRIGGER_DEL_VISITS = "CREATE TRIGGER delete_visits DELETE ON " + TblVisits.TBL_NAME + "\n" +
                " BEGIN \n" +
                " DELETE FROM " + NotificationId.TBL_NAME + " WHERE " + NotificationId._ID + " = old." + TblVisits.COL_NOTIF_ID + ";\n" +
                " END \n";

		db.execSQL(DDL_CREATE_TRIGGER_DEL_MEDS);
		db.execSQL(DDL_CREATE_TRIGGER_DEL_VACCS);
		db.execSQL(DDL_CREATE_TRIGGER_DEL_VISITS);
		initFreqTable(db);

		initialValues(db);
	}

	@Override
	public void onOpen(SQLiteDatabase db) {
		// foreign key integrity, only works on Froyo and later
		super.onOpen(db);
		if (!db.isReadOnly()) {
			db.execSQL("PRAGMA foreign_keys=ON;");
		}
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO drop tables for testing only; don't lose anyone's data!
		String[] tables = { TblPets.TBL_NAME, TblMeds.TBL_NAME, TblVisits.TBL_NAME, 
				TblVaccs.TBL_NAME, TblNotifFrequency.TBL_NAME, NotificationId.TBL_NAME };
		for (String s: tables) {
			db.execSQL("DROP TABLE IF EXISTS " + s);
		}
		onCreate(db);
	}

	private void initFreqTable(SQLiteDatabase db) {
		ContentValues values = new ContentValues();
		values.put(TblNotifFrequency.COL_ID, 0); // start table at 0 to line up with zero-indexed Spinner
		values.put(TblNotifFrequency.COL_DESCRIPTION, "built-in0"); // built-in value
		values.put(TblNotifFrequency.COL_TIME_VALUE, 6);
		values.put(TblNotifFrequency.COL_TIME_UNIT, TimeUnits.HOUR.name());
		db.insert(TblNotifFrequency.TBL_NAME, null, values);
		values.clear();
		values.put(TblNotifFrequency.COL_DESCRIPTION, "built-in1"); 
		values.put(TblNotifFrequency.COL_TIME_VALUE, 8);
		values.put(TblNotifFrequency.COL_TIME_UNIT, TimeUnits.HOUR.name());
		db.insert(TblNotifFrequency.TBL_NAME, null, values);
		values.clear();
		values.put(TblNotifFrequency.COL_DESCRIPTION, "built-in2"); 
		values.put(TblNotifFrequency.COL_TIME_VALUE, 12);
		values.put(TblNotifFrequency.COL_TIME_UNIT, TimeUnits.HOUR.name());
		db.insert(TblNotifFrequency.TBL_NAME, null, values);
		values.clear();
		values.put(TblNotifFrequency.COL_DESCRIPTION, "built-in3"); 
		values.put(TblNotifFrequency.COL_TIME_VALUE, 1);
		values.put(TblNotifFrequency.COL_TIME_UNIT, TimeUnits.DAY.name());
		db.insert(TblNotifFrequency.TBL_NAME, null, values);
		values.clear();
		values.put(TblNotifFrequency.COL_DESCRIPTION, "built-in4"); 
		values.put(TblNotifFrequency.COL_TIME_VALUE, 1);
		values.put(TblNotifFrequency.COL_TIME_UNIT, TimeUnits.WEEK.name());
		db.insert(TblNotifFrequency.TBL_NAME, null, values);
		values.clear();
		values.put(TblNotifFrequency.COL_DESCRIPTION, "built-in5"); 
		values.put(TblNotifFrequency.COL_TIME_VALUE, 14);
		values.put(TblNotifFrequency.COL_TIME_UNIT, TimeUnits.DAY.name());
		db.insert(TblNotifFrequency.TBL_NAME, null, values);
		values.clear();
		values.put(TblNotifFrequency.COL_DESCRIPTION, "built-in6"); 
		values.put(TblNotifFrequency.COL_TIME_VALUE, 1);
		values.put(TblNotifFrequency.COL_TIME_UNIT, TimeUnits.MONTH.name());
		db.insert(TblNotifFrequency.TBL_NAME, null, values);
	}
	
	private void initialValues(SQLiteDatabase db) {
		ContentValues values = new ContentValues();
		values.put(TblPets.COL_ID, 1);
		values.put(TblPets.COL_NAME, "doge");
		values.put(TblPets.COL_HEIGHT, 42);
		values.put(TblPets.COL_WEIGHT, 24);
		values.put(TblPets.COL_DOB, 1364870018251L);
		values.put(TblPets.COL_BREED, "shibe");
		values.put(TblPets.COL_MICROCHIP, 10010110);
		values.put(TblPets.COL_GENDER, "Female");
		db.insert(TblPets.TBL_NAME, null, values);
		values.clear();
		
		values.put(TblMeds.COL_NAME, "flea stuff");
		values.put(TblMeds.COL_FREQUENCY, 5);
		values.put(TblMeds.COL_DATE_BEGUN, 1365829786809L);
		values.put(TblMeds.COL_DATE_MOST_RECENT, 1399845716830L);
		values.put(TblMeds.COL_END_DATE, 1526188214934L);
		values.put(TblMeds.COL_DOSE, "one bottle");
		values.put(TblMeds.COL_NOTIF_DATE_TIME, 1401055316830L);
		values.put(TblMeds.COL_NOTIF_ID, db.insert(PetsContract.NotificationId.TBL_NAME, PetsContract.NotificationId._ID, null));
		values.put(TblMeds.COL_NOTIF_SET, true);
		values.put(TblMeds.COL_PET_ID, 1);
		db.insert(TblMeds.TBL_NAME, null, values);
		values.clear();
		
		values.put(TblMeds.COL_NAME, "wash ears");
		values.put(TblMeds.COL_FREQUENCY, 1);
		values.put(TblMeds.COL_DATE_BEGUN, 1336874918147L);
		values.put(TblMeds.COL_DATE_MOST_RECENT, 1399932706981L);
		values.put(TblMeds.COL_END_DATE, 1778638319977L);
		values.put(TblMeds.COL_DOSE, "one earful");
		values.put(TblMeds.COL_NOTIF_ID, db.insert(PetsContract.NotificationId.TBL_NAME, PetsContract.NotificationId._ID, null));
		values.put(TblMeds.COL_NOTIF_SET, false);
		values.put(TblMeds.COL_PET_ID, 1);
		db.insert(TblMeds.TBL_NAME, null, values);
		values.clear();
		
		values.put(TblVaccs.COL_NAME, "raybeez");
		values.put(TblVaccs.COL_DATE, 1399512600027L);
		values.put(TblVaccs.COL_EXPIRATION, 1494207014494L);
		values.put(TblVaccs.COL_NUMBER, 234573249);
		values.put(TblVaccs.COL_DOCTOR, "Dr Manhattan");
		values.put(TblVaccs.COL_NOTIF_ID, db.insert(PetsContract.NotificationId.TBL_NAME, PetsContract.NotificationId._ID, null));
		values.put(TblVaccs.COL_PET_ID, 1);
		db.insert(TblVaccs.TBL_NAME, null, values);
		values.clear();
		
		values.put(TblVisits.COL_DOCTOR, "Dr Manhattan");
		values.put(TblVisits.COL_DATE, 1399397444141L);
		values.put(TblVisits.COL_NEXT_APPOINTMENT_DATE, 1410120008154L);
		values.put(TblVisits.COL_SUMMARY, "got ears checked, anus probed, temperature and weight measured...");
		values.put(TblVisits.COL_NOTIF_ID, db.insert(PetsContract.NotificationId.TBL_NAME, PetsContract.NotificationId._ID, null));
		values.put(TblVisits.COL_NOTIF_SET, true);
		values.put(TblVisits.COL_PET_ID, 1);
		db.insert(TblVisits.TBL_NAME, null, values);
		values.clear();
		
		values.put(TblVisits.COL_DOCTOR, "craay z.");
		values.put(TblVisits.COL_DATE, 1396375865049L);
		values.put(TblVisits.COL_SUMMARY, "neutered :(");
		values.put(TblVisits.COL_NOTIF_ID, db.insert(PetsContract.NotificationId.TBL_NAME, PetsContract.NotificationId._ID, null));
		values.put(TblVisits.COL_NOTIF_SET, true);
		values.put(TblVisits.COL_PET_ID, 1);
		db.insert(TblVisits.TBL_NAME, null, values);
		values.clear();
		
		values.put(TblVisits.COL_DOCTOR, "Dr Manhattan");
		values.put(TblVisits.COL_DATE, 1205860801357L);
		values.put(TblVisits.COL_NEXT_APPOINTMENT_DATE, 1207696927010L);
		values.put(TblVisits.COL_SUMMARY, "dog was sick from eating too much cat poop, needed stomach pumped and breath freshened.");
		values.put(TblVisits.COL_NOTIF_ID, db.insert(PetsContract.NotificationId.TBL_NAME, PetsContract.NotificationId._ID, null));
		values.put(TblVisits.COL_NOTIF_SET, false);
		values.put(TblVisits.COL_PET_ID, 1);
		db.insert(TblVisits.TBL_NAME, null, values);
		values.clear();
		
		values.put(TblVisits.COL_DOCTOR, "Jones");
		values.put(TblVisits.COL_DATE, 1399930289664L);
		values.put(TblVisits.COL_NEXT_APPOINTMENT_DATE, 1402876065386L);
		values.put(TblVisits.COL_SUMMARY, "everyting cool");
		values.put(TblVisits.COL_NOTIF_ID, db.insert(PetsContract.NotificationId.TBL_NAME, PetsContract.NotificationId._ID, null));
		values.put(TblVisits.COL_NOTIF_SET, false);
		values.put(TblVisits.COL_PET_ID, 1);
		db.insert(TblVisits.TBL_NAME, null, values);
	}
}
